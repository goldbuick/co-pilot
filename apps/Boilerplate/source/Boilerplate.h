
#pragma once

#include <co+pilot/bundles/common/Scene.h>

namespace Boilerplate
{
    
    class Boilerplate : public copilot::SceneManager
    {
    public:
        Boilerplate(copilot::CoreLogicManager* manager, const char* name)
        : copilot::SceneManager(manager, name) { }
        
        // get next scene
        copilot::Scene* next(uint8_t result);
    };
    
}
