
#pragma once

#include <co+pilot/bundles/common/Scene.h>

namespace Boilerplate
{
    
    class BoilerplateScene : public copilot::Scene
    {
    public:
        BoilerplateScene(copilot::CoreLogicManager* manager)
        : copilot::Scene(manager) { }
        
        void init();
    };
    
}
