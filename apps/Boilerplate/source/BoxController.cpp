
#include "BoxController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/sdl/InputMap.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/bundles/physics/Collide2D.h>

using namespace copilot;

namespace Boilerplate
{
    
    BoxController::BoxController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        LibSDL* sdl = manager->request<LibSDL>();
        GLRenderPipe* render = manager->request<GLRenderPipe>();
        EventedCollection* entities = manager->request<EventedCollection>();

        // get window
        size_t windowHandle = sdl->getWindowHandle(0);
        
        // add camera
        size_t cameraHandle = render->addCamera2D(0);
        render->addCamera2DToWindow(windowHandle, cameraHandle);
        
        GLRenderScreenUnits unit;
        unit.autosize = false;
        unit.target = glm::vec2(640.f, 480.f);
        render->setUnitForWindow(windowHandle, unit);
        
        render->addLayerToCamera2D(GLClearLayer::addLayer(manager, glm::makeColor(0,0,0)), cameraHandle);
        
        size_t layer = render->addLayer(BoxController::selBoxes, GLDebugLayer::render);
        render->addLayerToCamera2D(layer, cameraHandle);
        
        // wire up events
        Props(BoxController);
        entities->addEvent(Props.evtBoxProps(), BoxController::selBox, EventedCollection::opCopyAllFromMeta);
        entities->addTimer(60.f, BoxController::selBoxes, BoxController::opUpdateBoxes);
    }
    
    // selectors
    EventedSelector(BoxController::selBox)
    {
        Props(BoxController);
        size_t collider = Props.with(meta)->collider();
        ForEachEventedSelectorByProp(box)
            if (Props.with(*i)->collider() == collider) result.push_back(*i);
        return result;
    }
    EventedSelector(BoxController::selBoxes)
    {
        Props(BoxController);
        return collection->selectByProp(Props.strbox());
    }
    
    // operations
    EventedOperation(BoxController::opUpdateBoxes)
    {
        opDelta;
        Props(BoxController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        
        Props.with(target);
        
        size_t collider = Props.collider();
        glm::vec2 position = Props.position();
        glm::vec2 velocity = Props.velocity();
        
        if (Props.flagMove().x || Props.flagMove().y)
        {
            velocity = Props.flagMove() * 256.f;
        }
        
        glm::vec2 dist, step = velocity * delta;
        while (step.x != 0.f || step.y != 0.f)
        {
            if (collide2D->sweptCollide(collider, position, 0.f, step, dist).size() > 0)
            {
                if (velocity.x && step.x == 0.f) velocity.x = 0.f;
                if (velocity.y && step.y == 0.f) velocity.y = 0.f;
            }
            position += dist;
        }
        
        Props.position(position);
        Props.velocity(velocity);
        collide2D->update(collider, position, 0.f);
        
    }
    
    // api
    
    void BoxController::addBox(bool useController, glm::vec2 position)
    {
        Props(BoxController);
        OtherProps(GLDebugLayer);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        EventedCollection* entities = manager->request<EventedCollection>();

        PropertyCollection props;
        Props.with(props);
        Props.box(true);
        Props.size(glm::vec2(64.f));
        Props.position(position);
        
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(Props.size());
        GLDebugLayerProps.position(Props.position());
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, useController ? glm::makeColor(255,255,0) : glm::makeColor(255,0,0)));
        
        cpShape* shape = cpBoxShapeNew(NULL, Props.size().x, Props.size().y);
        size_t collider = collide2D->addShape(shape);
        collide2D->update(collider, position, 0.f);
        Props.collider(collider);

        size_t entity = entities->add("init", props);
        
        if (useController)
        {
            InputMap* input = manager->request<InputMap>();
            
            uint8_t joystick = 0;
            InputMapVector playerMove;
            
            playerMove.left.keys.push_back({SDLK_LEFT});
            playerMove.right.keys.push_back({SDLK_RIGHT});
            playerMove.up.keys.push_back({SDLK_UP});
            playerMove.down.keys.push_back({SDLK_DOWN});
            
            playerMove.threshold = AxisMax;
            playerMove.left.joystickAxises.push_back({joystick, 0, AxisMin});
            playerMove.right.joystickAxises.push_back({joystick, 0, AxisMax});
            playerMove.up.joystickAxises.push_back({joystick, 1, AxisMin});
            playerMove.down.joystickAxises.push_back({joystick, 1, AxisMax});
            size_t move = input->addVector(playerMove);
            
            // common data for input events
            PropertyCollection meta;
            Props.with(meta);
            Props.collider(collider);
            
            // bind input map events to triggers
            entities->addInput(move, Props.evtBoxProps(), meta, Props.strflagMove());
        }
    }
    
}


