
#include "Boilerplate.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/files/AssetManager.h>

using namespace copilot;

int main(int argc, char * argv[])
{
    CoreLogicManager manager;
    
    // config pub dir
    AssetManager* assetManager = manager.request<AssetManager>();
    assetManager->config(argc, argv);
    
    // add window
    LibSDL* sdl = manager.request<LibSDL>();
    sdl->addWindow("SubSpaceEmbrace", glm::vec2(1024.f, 768.f));
    
    // main game loop
    Boilerplate::Boilerplate* boiler = manager.request<Boilerplate::Boilerplate>();
    boiler->run(0, 60.f);
    
    return 0;
}

