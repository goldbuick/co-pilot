//
//  FudgeController.cpp
//  FudgeCatcher
//
//  Created by Steven Velozo on 11/26/13.
//  Copyright (c) 2013 Steven Velozo. All rights reserved.
//

#include "FudgeController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/files/FileAccess.h>
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/files/images/Images.h>
using namespace copilot;

FudgeController::FudgeController(CoreLogicManager* manager, const char* name) :
    CoreLogic(manager, name)
{
    LibSDL* sdl = manager->request<copilot::LibSDL>();
    GLDraw* draw = manager->request<copilot::GLDraw>();
    Images* images = manager->request<copilot::Images>();
    FileAccess* fileAccess = manager->request<copilot::FileAccess>();
    EventedCollection* entities = manager->request<copilot::EventedCollection>();
    
    // load default shader
    copilot::FileBuffer* vert = fileAccess->bufferFromAppOrPub("default", "vert", true);
    copilot::FileBuffer* frag = fileAccess->bufferFromAppOrPub("default", "frag", true);
    if (vert && frag) fudgetechshadez = draw->compileShader(vert->getText(), frag->getText());
    if (vert) delete vert;
    if (frag) delete frag;
    
    FileBuffer *candyBuffer = fileAccess->bufferFromAppOrPub("FudgeGun/FoodCatcher/Candy1", "png");
    Image candyimage = images->loadImageFromBuffer(candyBuffer->getData(), candyBuffer->getLength());
    
    fudgetechs[0] = draw->compileTexture(candyimage.size, candyimage.pixels.data(), candyimage.pixels.size());
    
    quadindices = draw->compileQuadIndices();
    
    entities->addEvent(sdl->addTimer(60), FudgeController::SelectFudge, FudgeController::UpdateFudge);
}

void FudgeController::addFudge(int Weight)
{
    EventedCollection* entities = manager->request<EventedCollection>();
    
    PropertyCollection fudge;
    
    fudge.set("weight", Weight);
    // This is the top left corner of the screen, and the x,y center of my fudge.
    fudge.set("position", glm::vec2(0, 0));
    fudge.set("velocity", glm::vec2(50,0));
    fudge.set("fudge", true);
    
    entities->add(0, fudge);
}

void FudgeController::draw(glm::vec2 windowSize)
{
    copilot::GLDraw* draw = manager->request<copilot::GLDraw>();
    copilot::EventedCollection* entities = manager->request<copilot::EventedCollection>();
    
    copilot::PropertyCollection meta;
    std::list<copilot::ReadEventedEntity> selection = entities->select(FudgeController::SelectFudge, meta);
    
    glm::mat4 transform;
    glm::mat4 projection = glm::ortho(0.f, windowSize.x, windowSize.y, 0.f, -512.f, 512.f);
    
    quadpool.index = 0;
    for (std::list<copilot::ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
    {
        glm::vec2 position = i->get<glm::vec2>("position");
        quadpool.quads[quadpool.index++] = draw->compileQuad(fudgetechs[0]/* L2Vec */, position, fudgetechs[0].size);
    }
    
    draw->render(fudgetechshadez, projection, transform, &fudgetechs[0], 1, quadpool.quads, &quadindices, quadpool.index);
}


EventedSelector(FudgeController::SelectFudge)
{
    ForEachEventedSelector if (i->has<bool>("fudge")) result.push_back(*i);
    return result;
}

EventedOperation(FudgeController::UpdateFudge)
{
    PropertyCollection *props = target->getProperties();
    
    float delta = meta.get<float>("delta");
    
    glm::vec2 position = props->get<glm::vec2>("position");
    glm::vec2 velocity = props->get<glm::vec2>("velocity");
    
    position += velocity * delta;
    
    velocity -= velocity * delta * .2f;
//
    if (velocity.x < 0.001)
        velocity.x = 0.0;
    if (velocity.y < 0.001)
        velocity.y = 0.0;
    
    props->set("velocity", velocity);
    props->set("position", position);
}