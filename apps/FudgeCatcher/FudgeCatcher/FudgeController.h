
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>
#include <co+pilot/bundles/render/GLDraw.h>

class FudgeController : public copilot::CoreLogic
{
    public:
        FudgeController(copilot::CoreLogicManager* manager, const char* name);
        
        void addFudge(int Weight);
        void draw(glm::vec2 windowSize);
    
        static EventedSelector(SelectFudge);    
        static EventedOperation(UpdateFudge);
    private:
        copilot::GLTexture              fudgetechs[4];
        copilot::GLShader               fudgetechshadez;
        copilot::GLRenderQuadPool       quadpool;
        copilot::GLRenderQuadIndices    quadindices;
};
