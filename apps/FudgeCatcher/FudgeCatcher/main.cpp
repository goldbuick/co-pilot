//
//  main.m
//  FudgeCatcher
//
//  Created by Steven Velozo on 11/26/13.
//  Copyright (c) 2013 Steven Velozo. All rights reserved.
//

#include "FudgeController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/render/GLDraw.h>
#include <co+pilot/bundles/common/Settings.h>
#include <co+pilot/bundles/files/FileAccess.h>

int main(int argc, char * argv[])
{
    copilot::CoreLogicManager manager;
    
    copilot::SettingsArgs args = manager.request<copilot::Settings>()->parse(argc, argv);
    copilot::GLDraw* draw = manager.request<copilot::GLDraw>();
    copilot::FileAccess* fileAccess = manager.request<copilot::FileAccess>();
    copilot::EventedCollection* entities = manager.request<copilot::EventedCollection>();
    
    args.pubPath.length() ?
    fileAccess->setDirAbsolute(args.pubPath.c_str()) :
    fileAccess->setDirRelative();
    
    copilot::LibSDL* sdl = manager.request<copilot::LibSDL>();
    
    glm::vec2 windowSize(1200,600);
    sdl->addWindow("FUDGE TITLE", windowSize);
    
    FudgeController* FUDGEMASTER1 = manager.request<FudgeController>();
    
    FUDGEMASTER1->addFudge(1200);

    // standard update timer
    uint32_t updateId = sdl->addTimer(60);

    while (sdl->active())
    {
        SDL_Event event;
        
        while (sdl->getEvents(0, event))
        {
            if (event.type == updateId)
            {
                float delta = (float)event.user.code / 1000.f;
                
                copilot::PropertyCollection meta;
                meta.set("delta", delta);
                entities->trigger(updateId, meta);
            }
            
            switch (event.type)
            {
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE:
                            exit(0);
                            break;
                            
                        default:
                            break;
                    }
            }
        }
        
        entities->dispatch();
        
        // render stuff now ??
        size_t window = sdl->getWindowHandle(0);
        sdl->makeCurrentWindow(window);
        
        draw->config(windowSize);
        draw->clear(glm::vec3(0.2f, 0.5f, 1.0f));
        FUDGEMASTER1->draw(windowSize);
        
        sdl->swapBuffersWindow(window);
    }
    
    return 0;
}
