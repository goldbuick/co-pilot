
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    
    class AttackController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyEvent(Attack)
            PropertyEvent(Impact)
            PropertyEvent(Death)
            // common
            PropertyAccessor(position, glm::vec2)
            // attack event
            PropertyAccessor(team, int)
            PropertyAccessor(slot, int)
            PropertyAccessor(handle, size_t)
            PropertyAccessor(direction, glm::vec2)
            // impact event
            PropertyAccessor(impact, glm::vec2)
            PropertyAccessor(damage, int)
        )
        
    public:
        AttackController(copilot::CoreLogicManager* manager, const char* name);
        
    };
    
}