
#include "HotShotsCoda.h"
#include "AudioController.h"
#include "AttackController.h"
#include "PlayerController.h"

//#include <co+pilot/bundles/audio/Plaid.h>
#include <co+pilot/bundles/files/AssetManager.h>
//#include <co+pilot/bundles/audio/plaid/audio/effects.h>

using namespace copilot;

namespace coda
{
    
    AudioController::AudioController(copilot::CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        OtherProps(AttackController);
        OtherProps(PlayerController);
        
        //manager->request<PlaidAudio>();
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        entities->addTimer(settings->get().FPS(), AudioController::selAudioUpdate, AudioController::opAudioUpdate);
        //entities->addEvent(AttackControllerProps.evtAttack(), AudioController::selAudioUpdate, AudioController::opAudioAttack);
        entities->addEvent(PlayerControllerProps.evtPlayerLadderGrab(), AudioController::selAudioUpdate, AudioController::opAudioLadder);
        
        PropertyCollection audioUpdate;
        audioUpdate.set("audioUpdate", true);
        entities->add(settings->get().evtInit(), audioUpdate);
    }
    
    // selectors
    EventedSelector(AudioController::selAudioUpdate)
    {
        return collection->selectByProp("audioUpdate");
    }
    
    // operations
    EventedOperation(AudioController::opAudioUpdate)
    {
        //manager->request<PlaidAudio>()->update();
    }
    
    EventedOperation(AudioController::opAudioAttack)
    {
        manager->request<AudioController>()->randPlay("coda/sfx/gun-machine-fire", 0.8f, 1.f);
    }
    
    EventedOperation(AudioController::opAudioLadder)
    {
        manager->request<AudioController>()->randPlay("coda/sfx/ladder-grab", 1.f, 2.f);
    }
    
    // api    
    void AudioController::randPlay(const char* resource, float min, float max)
    {
//        PlaidAudio* plaid = manager->request<PlaidAudio>();
//        AssetManager* assets = manager->request<AssetManager>();
//        PlaidSoundRef* sound = assets->request<PlaidSoundRef>(resource);
//        
//        float delta = max - min;
//        float ratio = (float)(rand() % 1000) / 1000.f;
//        float pitch = min + delta * ratio;
//        plaid->getAudio()->play(new plaid::Pitch(sound->ref->player(), pitch));
    }
    
}
