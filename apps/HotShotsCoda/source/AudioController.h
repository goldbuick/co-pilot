
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    
    class AudioController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
        )
        
    public:
        AudioController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selAudioUpdate);
        
        // operations
        static EventedOperation(opAudioUpdate);
        static EventedOperation(opAudioAttack);
        static EventedOperation(opAudioLadder);
        
        // api
        void randPlay(const char* resource, float min, float max);
    };
    
}