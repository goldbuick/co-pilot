
#include "HotShotsCoda.h"
#include "PlayerController.h"
#include "CameraController.h"
#include "AttackController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    
    CameraController::CameraController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Props(CameraController);
        OtherProps(AttackController);
        
        Settings* settings = manager->request<Settings>();
        LibSDL* sdl = manager->request<LibSDL>();
        GLRenderPipe* render = manager->request<GLRenderPipe>();;
        EventedCollection* entities = manager->request<EventedCollection>();
        
        // get window
        size_t windowHandle = sdl->getWindowHandle(0);
        
        // add camera
        size_t cameraHandle = render->addCamera2D(0);
        render->addCamera2DToWindow(windowHandle, cameraHandle);
        
        GLRenderScreenUnits unit;
        unit.autosize = false;
        unit.target = settings->get().Unit();
        render->setUnitForWindow(windowHandle, unit);
        
        // add layers
        render->addLayerToCamera2D(GLClearLayer::addLayer(manager, glm::vec3(167/255.f, 200/255.f, 255/255.f)), cameraHandle);
        
        // create camera entity
        PropertyCollection meta;;
        Props.with(meta);
        Props.cameraHandle(cameraHandle);
        Props.shake(0.f);
        Props.offset(0.f);
        Props.overZoom(PlayerMatchStart);
        
        entities->add(settings->get().evtInit(), meta);
        entities->addTimer(settings->get().FPS(), CameraController::selCamera, CameraController::opUpdateCamera);
        entities->addEvent(AttackControllerProps.evtImpact(), CameraController::selCamera, CameraController::opShakeCamera);
        entities->addEvent(Props.evtCameraProps(), CameraController::selCamera, EventedCollection::opCopyAllFromMeta);
    }
    
    // selectors
    EventedSelector(CameraController::selCamera)
    {
        Props(CameraController);
        
        return collection->selectByProp(Props.strcameraHandle());
    }
    
    // operations
    EventedOperation(CameraController::opUpdateCamera)
    {
        opDelta;
        Props(CameraController);
        
        LibSDL* sdl = manager->request<LibSDL>();
        GLRenderPipe* render = manager->request<GLRenderPipe>();;
        
        Props.with(target);
        
        glm::vec2 unit = render->getUnitForWindow(sdl->getWindowHandle(0));
        glm::vec2 halfUnit = unit * 0.5f;
        
        GLRenderCamera2D* camera = render->getCamera2D(Props.cameraHandle());
        
        // drop overzoom
        float overZoomValue = Props.overZoom(Props.overZoom() - delta);
        if (overZoomValue < 0.f) overZoomValue = 0.f;
        float overZoomRatio = overZoomValue / (float)PlayerMatchStart;
        float overZoom = 1.f + 0.8f * (overZoomRatio * overZoomRatio);
        if (overZoom < 1.f) overZoom = 1.f;
        
        // rock it
        Props.offset(Props.offset() + delta);
        //camera->angle = glm::sin(Props.offset()) * 0.01f;
        
        // find players
        std::list<ReadEventedEntity> players = collection->select(PlayerController::selPlayers);
        
        // get clipping
        glm::rect clipping = Props.clipping();
        
        // get bounding box
        glm::vec2 position;
        float margin = 256.f;
        glm::vec2 minPos(100000.f), maxPos(-100000.f);
        for (std::list<ReadEventedEntity>::iterator i=players.begin(); i!=players.end(); ++i)
        {
            position = clipping.clip(Props.with(*i)->position() - margin);
            if (position.x < minPos.x) minPos.x = position.x;
            if (position.y < minPos.y) minPos.y = position.y;
            position = clipping.clip(Props.with(*i)->position() + margin);
            if (position.x > maxPos.x) maxPos.x = position.x;
            if (position.y > maxPos.y) maxPos.y = position.y;
        }
        Props.with(target);
        
        // box size
        glm::vec2 boxSize = maxPos - minPos;
        
        // center camera
        camera->position = minPos + (boxSize * 0.5f);
        
        // pick the biggest diff
        glm::vec2 scale = (unit / boxSize) / overZoom;
        if (scale.x > 1.f) scale.x = 1.f;
        if (scale.y > 1.f) scale.y = 1.f;
        
        //*/
        camera->scale = (scale.x < scale.y) ? glm::vec2(scale.x) : glm::vec2(scale.y);
        /*/
        camera->scale = glm::vec2(0.5f);
        //*/
        
        // clip camera
        if (clipping.getWidth() > 0 && overZoom == 1.f)
        {
            halfUnit *= 1.f / camera->scale;
            clipping = glm::rect::withLTRB(
                                           clipping.getLeft() + halfUnit.x,
                                           clipping.getTop() + halfUnit.y,
                                           clipping.getRight() - halfUnit.x,
                                           clipping.getBottom() - halfUnit.y);
            camera->position = clipping.clip(camera->position);
        }
        
        // supra fx
        float shake = Props.shake();
        if (shake >= 0.f)
        {
            float range = 32.f * shake;
            glm::vec2 minPos = camera->position - range;
            glm::vec2 maxPos = camera->position + range;
            camera->position = glm::linearRand(minPos, maxPos);
            Props.shake(shake - delta);
        }
        
    }
    
    EventedOperation(CameraController::opShakeCamera)
    {
        Props(CameraController);
        OtherProps(AttackController);
        
        Props.with(target)->shake((float)AttackControllerProps.with(meta)->damage() * 0.1f);
    }
    
    // api
    void CameraController::setClipping(glm::rect clipping)
    {
        Props(CameraController);
        
        // do a set props thing
        PropertyCollection meta;
        Props.with(meta);
        Props.clipping(clipping);
        manager->request<EventedCollection>()->trigger(Props.evtCameraProps(), meta);
    }
    
    void CameraController::addLayerToCamera2D(size_t layerHandle)
    {
        GLRenderPipe* render = manager->request<GLRenderPipe>();;
        render->addLayerToCamera2D(layerHandle, render->camera2DHandle(0));
    }
        
}
