
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

#include <map>

namespace coda
{
    
    class CameraController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyEvent(CameraProps)
            PropertyAccessor(cameraHandle, size_t)
            PropertyAccessor(offset, float)
            PropertyAccessor(shake, float)
            PropertyAccessor(overZoom, float)
            PropertyAccessor(position, glm::vec2)
            PropertyAccessor(clipping, glm::rect))
    public:
        CameraController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selCamera);
        
        // operations
        static EventedOperation(opUpdateCamera);
        static EventedOperation(opShakeCamera);
        
        // api
        void setClipping(glm::rect clipping);
        void addLayerToCamera2D(size_t layerHandle);
    };
    
}