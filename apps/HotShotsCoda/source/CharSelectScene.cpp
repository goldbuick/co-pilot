
#include "HotShotsCoda.h"
#include "CharSelector.h"
#include "AudioController.h"
#include "CharSelectScene.h"
#include "TerrainController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/files/AssetManager.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    
    void CharSelectScene::init()
    {
        manager->request<AudioController>();
        LibSDL* sdl = manager->request<LibSDL>();
        Settings* settings = manager->request<Settings>();
        GLRenderPipe* render = manager->request<GLRenderPipe>();
        CharSelector* selector = manager->request<CharSelector>();
        TerrainController* terrain = manager->request<TerrainController>();
        
        
        // play music
        settings->playMusic("coda/music/menu-loop");
        
        // get window
        size_t windowHandle = sdl->getWindowHandle(0);
        
        // add camera
        size_t cameraHandle = render->addCamera2D(0);
        render->addCamera2DToWindow(windowHandle, cameraHandle);
        
        GLRenderScreenUnits unit;
        unit.autosize = false;
        unit.target = settings->get().Unit();
        render->setUnitForWindow(windowHandle, unit);
        
        // add layers
        render->addLayerToCamera2D(GLClearLayer::addLayer(manager, glm::vec3(84/255.f,36/255.f,55/255.f)), cameraHandle);
        render->addLayerToCamera2D(terrain->addDecoLayer(glm::vec2(1.f)), cameraHandle);
        render->addLayerToCamera2D(selector->addGfxLayer(), cameraHandle);
        render->addLayerToCamera2D(selector->addCharLayer(), cameraHandle);
        render->addLayerToCamera2D(selector->addUILayer(), cameraHandle);
        
        // add deco
        glm::vec3 decoColor(192/255.f,41/255.f,66/255.f);
        glm::vec3 decoColor2(84/255.f,36/255.f,55/255.f);
        for (int i=0; i<64; ++i)
            terrain->addDeco(
                glm::linearRand(unit.target * -0.5f, unit.target * 0.5f),
                glm::linearRand(glm::vec2(64.f), glm::vec2(256.f)),
                glm::linearRand(decoColor, decoColor2));
        
        // add ui
        glm::vec2 inset(116.5f);
        selector->addCharSelect(1, glm::vec2(-inset.x, -inset.y));
        selector->addCharSelect(2, glm::vec2( inset.x, -inset.y));
        selector->addCharSelect(3, glm::vec2(-inset.x,  inset.y));
        selector->addCharSelect(4, glm::vec2( inset.x,  inset.y));
        
    }
    
}