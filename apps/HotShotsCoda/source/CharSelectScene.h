
#pragma once

#include <co+pilot/bundles/common/Scene.h>

namespace coda
{
    
    class CharSelectScene : public copilot::Scene
    {
    public:
        CharSelectScene(copilot::CoreLogicManager* manager) : copilot::Scene(manager) { }
        void init();
    };
    
}
