
#include "HotShotsCoda.h"
#include "CharSelector.h"
#include "AudioController.h"
#include "PlayerController.h"
#include <co+pilot/bundles/sdl/InputMap.h>
#include <co+pilot/bundles/render/GLFont.h>

using namespace copilot;

namespace coda
{
    
    CharSelector::CharSelector(copilot::CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        NamedProps(CharSelector, ChrUI);
        NamedProps(CharSelector, ChrSelect);
        
        Settings* settings = manager->request<Settings>();
        AssetManager* assetManager = manager->request<AssetManager>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        entities->addTimer(settings->get().FPS(), CharSelector::selGfx, CharSelector::opUpdateGfx);
        entities->addTimer(settings->get().FPS(), CharSelector::selChars, CharSelector::opUpdateChars);
        entities->addTimer(settings->get().FPS(), CharSelector::selUI, CharSelector::opUpdateUI);
        entities->addEvent(ChrSelect.evtChrSelectProps(), CharSelector::selChar, EventedCollection::opCopyAllFromMeta);
        
        PropertyCollection props;
        ChrUI.with(props);
        ChrUI.chrUI(true);
        ChrUI.uiReadyTimer(3.f);
        ChrUI.uiTimerActive(false);
        
        entities->add(settings->get().evtInit(), props);
        
        assetManager->request<GLFontTextureRef>("default,24");
    }
    
    // selectors
    EventedSelector(CharSelector::selGfx)
    {
        NamedProps(CharSelector, ChrGfx);
        
        return collection->selectByProp(ChrGfx.strchrGfx());
    }
    EventedSelector(CharSelector::selChar)
    {
        NamedProps(CharSelector, ChrSelect);
        OtherNamedProps(CharSelector, ChrSelect);
        
        CharSelectorChrSelect.with(meta);
        ForEachEventedSelectorByProp(chrSelect)
            if (ChrSelect.with(*i)->chrSelect() == CharSelectorChrSelect.chrSelect()) result.push_back(*i);
        return result;
    }
    EventedSelector(CharSelector::selChars)
    {
        NamedProps(CharSelector, ChrSelect);
        
        return collection->selectByProp(ChrSelect.strchrSelect());
    }
    EventedSelector(CharSelector::selUI)
    {
        NamedProps(CharSelector, ChrUI);
        
        return collection->selectByProp(ChrUI.strchrUI());
    }
    
    // operations
    EventedOperation(CharSelector::opUpdateGfx)
    {
        NamedProps(CharSelector, ChrGfx);
        NamedProps(CharSelector, ChrSelect);
        
        ChrGfx.with(target);
        ReadEventedEntity entity = collection->get(ChrGfx.chrPlayer());
        if (!entity.valid()) return;

        ChrSelect.with(entity);
        
        if (ChrGfx.hasthumb())
        {
            if (ChrSelect.chrClass() == PlayerClassNone || ChrSelect.chrLock())
            {
                ChrGfx.position(glm::vec2(10000.f));
            }
            else
            {
                ChrGfx.position(ChrSelect.position() + ChrSelect.chrFlagMove() * 100.f);
            }
        }
    }
    EventedOperation(CharSelector::opUpdateChars)
    {
        NamedProps(CharSelector, ChrSelect);
        
        ChrSelect.with(target);
        
        if (ChrSelect.chrClass() == PlayerClassNone)
        {
            if (ChrSelect.chrFlagJump())
            {
                ChrSelect.chrClass(PlayerClassMax);
                manager->request<AudioController>()->randPlay("coda/sfx/menu-char-join", 1.f, 1.1f);
            }
        }
        else
        {
            if (ChrSelect.chrLock())
            {
                if (ChrSelect.chrFlagJump())
                {
                    ChrSelect.chrLock(false);
                    manager->request<AudioController>()->randPlay("coda/sfx/menu-char-join", 1.f, 1.1f);
                }
            }
            else
            {
                float pi = glm::pi<float>();
                float twoPi = pi * 2.f;
                float quarterPi = pi * 0.25f;
                float eigthPi = quarterPi * 0.5f;
                
                glm::vec2 flagMove = ChrSelect.chrFlagMove();
                float thumbAngle = atan2f(flagMove.y, flagMove.x) - eigthPi;
                thumbAngle = ceilf(thumbAngle / quarterPi) * quarterPi;
                
                if (thumbAngle < 0) thumbAngle += twoPi;
                if (thumbAngle >= twoPi) thumbAngle -= twoPi;
                
                ChrSelect.chrClass(1 + (int)(thumbAngle / quarterPi));
                
                if (ChrSelect.chrFlagAttack1() || ChrSelect.chrFlagAttack2())
                {
                    ChrSelect.chrLock(true);
                    manager->request<AudioController>()->randPlay("coda/sfx/menu-char-selected", 0.6f, 1.6f);
                }
                
                if (ChrSelect.chrLastClass() != ChrSelect.chrClass())
                {
                    if (ChrSelect.chrLastClass() != 0)
                    {
                        manager->request<AudioController>()->randPlay("coda/sfx/menu-char-change", 0.6f, 1.6f);
                    }
                    ChrSelect.chrLastClass(ChrSelect.chrClass());
                }
            }
        }
    }
    EventedOperation(CharSelector::opUpdateUI)
    {
        opDelta;
        NamedProps(CharSelector, ChrUI);
        NamedProps(CharSelector, ChrSelect);
        
        std::list<ReadEventedEntity> chrs = collection->select(CharSelector::selChars);
        
        std::list<int> playerIds;
        std::list<int> playerClasses;
        std::list<ReadEventedEntity>::iterator i;
        
        int lockCount = 0;
        int selectingCount = 0;
        for (i=chrs.begin(); i!=chrs.end(); ++i)
        {
            ChrSelect.with(*i);
            if (ChrSelect.chrLock())
            {
                ++lockCount;
                playerIds.push_back(ChrSelect.chrSelect());
                playerClasses.push_back(ChrSelect.chrClass());
            }
            if (ChrSelect.chrClass() != PlayerClassNone)
            {
                ++selectingCount;
            }
        }
        
        ChrUI.with(target);
        if (lockCount == 0 || lockCount != selectingCount)
        {
            ChrUI.uiReadyTimer(3.f);
            ChrUI.uiTimerActive(false);
        }
        else
        {
            ChrUI.uiTimerActive(true);
        }
        
        if (ChrUI.uiReadyTimer(ChrUI.uiReadyTimer() - delta) < 0.f)
        {
            Settings* settings = manager->request<Settings>();
            settings->get().PlayerIds(playerIds);
            settings->get().PlayerClasses(playerClasses);
            Scene::setResult(manager, SceneFight);
        }
    }
    
    // render
    GLRenderPipeLayer(CharSelector::renderChars)
    {
        NamedProps(CharSelector, ChrSelect);
        
        AssetManager* assetManager = manager->request<AssetManager>();
        PlayerController* player = manager->request<PlayerController>();
        GLShaderRef* shader = assetManager->request<GLShaderRef>("default,default");
        
        GLFont* font = manager->request<GLFont>();
        GLFontTextureRef* fontTex = assetManager->request<GLFontTextureRef>("default,24");
        
        glm::vec2 size;
        glm::vec2 position;
        std::string chrClassName;
        glm::mat4 projection = render->unitProjectionOrtho(units);
        glm::mat4 transform = render->camera2DTransform(units, camera, glm::vec2(1.f));
        
        GLRenderQuadPool* quadPool = batch->getQuadPool();
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            ChrSelect.with(*i);
            position = ChrSelect.position();
            chrClassName = player->getClassName(ChrSelect.chrClass());
            if (chrClassName.length() == 0) chrClassName = "Jump to Join";
            
            size = font->getTextSize(fontTex->ref, chrClassName.c_str());
            font->drawText(quadPool, fontTex->ref, position.x - size.x * 0.5f, position.y + size.y * 0.5f, chrClassName.c_str());
            
            chrClassName = "";
            if (ChrSelect.chrLock())
            {
                chrClassName = "Jump to Clear";
            }
            else if (ChrSelect.chrClass() != PlayerClassNone)
            {
                chrClassName = "Attack to Select";
            }
            
            if (chrClassName.length())
            {
                size = font->getTextSize(fontTex->ref, chrClassName.c_str());
                font->drawText(quadPool, fontTex->ref, position.x - size.x * 0.5f, position.y + size.y * 2.f, chrClassName.c_str());
            }
            
            chrClassName = strPrint("Player %i", ChrSelect.chrSelect());
            size = font->getTextSize(fontTex->ref, chrClassName.c_str());
            font->drawText(quadPool, fontTex->ref, position.x - size.x * 0.5f, position.y - size.y * 2.f, chrClassName.c_str());
        }
        
        batch->render(*shader->ref, projection, transform, &fontTex->ref->texture, 1, quadPool);
    }
    GLRenderPipeLayer(CharSelector::renderUI)
    {
        NamedProps(CharSelector, ChrUI);
        
        AssetManager* assetManager = manager->request<AssetManager>();
        GLShaderRef* shader = assetManager->request<GLShaderRef>("default,default");
        
        GLFont* font = manager->request<GLFont>();
        GLFontTextureRef* fontTex = assetManager->request<GLFontTextureRef>("default,24");
        
        glm::vec2 size;
        glm::vec2 position;
        std::string strTimer;
        glm::mat4 projection = render->unitProjectionOrtho(units);
        glm::mat4 transform = render->camera2DTransform(units, camera, glm::vec2(1.f));
        
        GLRenderQuadPool* quadPool = batch->getQuadPool();
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            ChrUI.with(*i);
            if (ChrUI.uiTimerActive())
            {
                strTimer = strPrint("Starting %i ...", (int)ChrUI.uiReadyTimer());
                size = font->getTextSize(fontTex->ref, strTimer.c_str());
                font->drawText(quadPool, fontTex->ref, -size.x * 0.5f, size.y * 0.5f, strTimer.c_str());
            }
        }
        
        batch->render(*shader->ref, projection, transform, &fontTex->ref->texture, 1, quadPool);
    }
    
    // api
    size_t CharSelector::addGfxLayer()
    {
        return GLDebugLayer::addLayer(manager, CharSelector::selGfx);
    }
    size_t CharSelector::addCharLayer()
    {
        return manager->request<GLRenderPipe>()->addLayer(CharSelector::selChars, CharSelector::renderChars);
    }
    size_t CharSelector::addUILayer()
    {
        return manager->request<GLRenderPipe>()->addLayer(CharSelector::selUI, CharSelector::renderUI);
    }
    
    size_t CharSelector::addGfx(PropertyCollection props, glm::vec2 position, glm::vec2 size, glm::vec3 color)
    {
        OtherProps(GLDebugLayer);
        NamedProps(CharSelector, ChrGfx);
        NamedProps(CharSelector, ChrSelect);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        ChrGfx.with(props);
        ChrGfx.chrGfx(true);
        ChrGfx.position(position);
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(size);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        return entities->add(settings->get().evtInit(), props);
    }
    
    void CharSelector::addCharSelect(int playerId, glm::vec2 position)
    {
        NamedProps(CharSelector, ChrGfx);
        NamedProps(CharSelector, ChrSelect);
        
        Settings* settings = manager->request<Settings>();
        PlayerController* player = manager->request<PlayerController>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        ChrSelect.with(props);
        ChrSelect.chrLock(false);
        ChrSelect.chrSelect(playerId);
        ChrSelect.chrClass(PlayerClassNone);
        ChrSelect.position(position);
        
        size_t entity = entities->add(settings->get().evtInit(), props);
        
        size_t move, aim, dash, jump, attack1, attack2;
        player->addInput(playerId, move, aim, dash, jump, attack1, attack2);
        
        // common data for input events
        PropertyCollection meta;
        ChrSelect.with(meta)->chrSelect(playerId);
        
        // bind input map events to triggers
        entities->addInput(move, ChrSelect.evtChrSelectProps(), meta, ChrSelect.strchrFlagMove());
        entities->addInput(jump, ChrSelect.evtChrSelectProps(), meta, ChrSelect.strchrFlagJump());
        entities->addInput(attack1, ChrSelect.evtChrSelectProps(), meta, ChrSelect.strchrFlagAttack1());
        entities->addInput(attack2, ChrSelect.evtChrSelectProps(), meta, ChrSelect.strchrFlagAttack2());
        
        // build gfx
        {
            PropertyCollection gfx;
            ChrGfx.with(gfx);
            ChrGfx.thumb(true);
            ChrGfx.chrPlayer(entity);
            position = glm::vec2(10000.f);
            addGfx(gfx, position, glm::vec2(32.f), glm::vec3(0.5f));
            addGfx(gfx, position, glm::vec2(16.f), glm::vec3(1.0f));
        }
    }
    
}
