
#pragma once

#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{

    class CharSelector : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(ChrUI,
            PropertyEvent(UIProps)
            PropertyAccessor(chrUI, bool)
            PropertyAccessor(uiTimerActive, bool)
            PropertyAccessor(uiReadyTimer, float))
        
        PropertyInterfaceEntity(ChrSelect,
            PropertyEvent(ChrSelectProps)
            PropertyAccessor(chrLock, bool)
            PropertyAccessor(chrClass, int)
            PropertyAccessor(chrLastClass, int)
            PropertyAccessor(chrSelect, int)
            PropertyAccessor(chrFlagJump, float)
            PropertyAccessor(chrFlagAttack1, float)
            PropertyAccessor(chrFlagAttack2, float)
            PropertyAccessor(chrFlagMove, glm::vec2)
            PropertyAccessor(position, glm::vec2))
        
        PropertyInterfaceEntity(ChrGfx,
            PropertyAccessor(chrGfx, bool)
            PropertyAccessor(chrPlayer, size_t)
            PropertyAccessor(thumb, bool)
            PropertyAccessor(position, glm::vec2))
        
        
    public:
        CharSelector(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selGfx);
        static EventedSelector(selChar);
        static EventedSelector(selChars);
        static EventedSelector(selUI);
        
        // operations
        static EventedOperation(opUpdateGfx);
        static EventedOperation(opUpdateChars);
        static EventedOperation(opUpdateUI);
        
        // render
        static GLRenderPipeLayer(renderChars);
        static GLRenderPipeLayer(renderUI);
        
        // api
        size_t addGfxLayer();
        size_t addCharLayer();
        size_t addUILayer();
        
        size_t addGfx(copilot::PropertyCollection props, glm::vec2 position, glm::vec2 size, glm::vec3 color);
        void addCharSelect(int playerId, glm::vec2 position);
    };
    
}