
#include "HotShotsCoda.h"
#include "CollisionController.h"
#include <co+pilot/bundles/physics/Collide2D.h>

using namespace copilot;

namespace coda
{
    
    CollisionController::CollisionController(copilot::CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        entities->addTimer(settings->get().FPS(), CollisionController::selColliders, CollisionController::opUpdateColliders);
    }
    
    // selectors
    EventedSelector(CollisionController::selColliders)
    {
        Props(CollisionController);
        
        return collection->selectByProp(Props.strcollider());
    }
    
    // operations
    EventedOperation(CollisionController::opUpdateColliders)
    {
        opDelta;
        Props(CollisionController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        
        size_t collider = Props.with(target)->collider();
        glm::vec2 position = Props.position();
        glm::vec2 velocity = Props.velocity();
        
        glm::vec2 dist, collide, step = velocity * delta;
        while (step.x != 0.f || step.y != 0.f)
        {
            if (collide2D->platformerCollide(collider, position, 0.f, step, dist, collide).size() > 0)
            {
                if (collide.x != 0.f) velocity.x = 0.f;
                if (collide.y != 0.f) velocity.y = 0.f;
            }
            position += dist;
        }
        
        Props.position(position);
        Props.velocity(velocity);
        collide2D->update(collider, position, 0.f);
    }
    
    // api
    void CollisionController::setEntity(size_t collider, size_t entity)
    {
        shapeToEntity[collider] = entity;
    }
    size_t CollisionController::getEntity(size_t collider)
    {
        std::map<size_t, size_t>::iterator i=shapeToEntity.find(collider);
        if (i == shapeToEntity.end()) return 0;
        return i->second;
    }
    
    size_t CollisionController::getCircle(int radius)
    {
        std::map<int, size_t>::iterator circle = circleShapes.find(radius);
        if (circle != circleShapes.end()) return circle->second;
        
        Collide2D* collide2D = manager->request<Collide2D>();
        circleShapes[radius] = collide2D->addShape(cpCircleShapeNew(NULL, radius, cpvzero));
        collide2D->update(circleShapes[radius], glm::vec2(-100000.f), 0.f);
        
        return circleShapes[radius];
    }
    
}
