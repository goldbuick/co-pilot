
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

#include <map>

namespace coda
{    
    class CollisionController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyAccessor(collider, size_t)
            PropertyAccessor(position, glm::vec2)
            PropertyAccessor(velocity, glm::vec2))
    public:
        CollisionController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selColliders);
        static EventedSelector(selSecondaryColliders);
        
        // operations
        static EventedOperation(opUpdateColliders);
        static EventedOperation(opUpdateSecondaryColliders);
        
        // api
        void setEntity(size_t collider, size_t entity);
        size_t getEntity(size_t collider);
        
        size_t getCircle(int radius);
        
    private:
        std::map<int, size_t> circleShapes;
        std::map<size_t, size_t> shapeToEntity;
    };
    
}