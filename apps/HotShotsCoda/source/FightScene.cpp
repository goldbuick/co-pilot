
#include "FightScene.h"
#include "HotShotsCoda.h"
#include "AudioController.h"
#include "ItemsController.h"
#include "MeleeController.h"
#include "CameraController.h"
#include "PlayerController.h"
#include "MonsterController.h"
#include "TerrainController.h"
#include "ProjectileController.h"
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    
    void FightScene::init()
    {
        srand((unsigned int)time(NULL));
        
        manager->request<AudioController>();
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        ItemsController* items = manager->request<ItemsController>();
        MeleeController* melee = manager->request<MeleeController>();
        CameraController* camera = manager->request<CameraController>();
        PlayerController* player = manager->request<PlayerController>();
        TerrainController* terrain = manager->request<TerrainController>();
        MonsterController* monster = manager->request<MonsterController>();
        ProjectileController* projectile = manager->request<ProjectileController>();
        
        // play music
        settings->playMusic("coda/music/fight-loop");

        // setup draw layers
        camera->addLayerToCamera2D(terrain->addDecoLayer(glm::vec2(terrain->layerScale(TerrainBackgroundMax))));
        for (int i=TerrainBackgroundMax-1; i>TerrainBlock; --i)
            camera->addLayerToCamera2D(terrain->addBackgroundLayer(i));
        camera->addLayerToCamera2D(terrain->addBlockLayer());
        camera->addLayerToCamera2D(monster->addSpawnLayer());
        camera->addLayerToCamera2D(items->addLayer());
        camera->addLayerToCamera2D(monster->addMonsterLayer());
        camera->addLayerToCamera2D(player->addLayer());
        camera->addLayerToCamera2D(melee->addLayer());
        camera->addLayerToCamera2D(projectile->addLayer());
        camera->addLayerToCamera2D(player->addGuiLayer());
        camera->addLayerToCamera2D(player->addStateLayer());
        
        // arena box
        glm::vec2 arenaSize = arenaContainer();
        
        // deco clouds
        glm::vec3 decoColor[2] = {
            glm::makeColor(167, 200, 255),
            glm::makeColor(105, 160, 255)
        };
        glm::vec2 range[2] = {
            glm::vec2(-arenaSize.x, -arenaSize.y) * terrain->layerScale(TerrainBackgroundMax), glm::vec2(arenaSize.x, arenaSize.y)
        };
        for (int i=0; i<128; ++i)
            terrain->addDeco(
                glm::linearRand(range[0], range[1]),
                glm::linearRand(glm::vec2(64.f), glm::vec2(256.f)),
                glm::linearRand(decoColor[0], decoColor[1]));
        
        // generate model
        FightSceneNode level;
        
        level.cols = level.rows = 3;
        glm::vec2 spawn = glm::vec2(1.f, (int)glm::linearRand(0.f,level.rows-1.f));
        
        char ladderShift;
        for (float y=0; y<level.rows; ++y)
        {
            ladderShift = rand() % 100 < 50 ? -1 : 1;
            for (float x=0; x<level.cols; ++x)
            {
                // tier1 node
                FightSceneNode tier1;
                
                // ladder direction pref
                tier1.ladderShift = ladderShift;
                
                genTier1Area(level, tier1, glm::vec2(x,y), spawn);
                
                // add to level
                level.tiles.push_back(tier1);
            }
        }
        
        bool done;
        do {
            // assume the best
            done = true;
            
            // fill out neighboors for tier1 & tier2
            mapBorders(level);
            
            // scan for invalid states
            for (float y=0; y<level.rows && done; ++y)
                for (float x=0; x<level.cols && done; ++x)
                {
                    FightSceneNode* sub = &level.tiles[x + y * level.cols];
                    if (sub->type == FSN_Box &&
                        (sub->topLeft == FSN_Box || sub->top == FSN_Box || sub->topRight == FSN_Box ||
                         sub->bottomLeft == FSN_Box || sub->bottom == FSN_Box || sub->bottomRight == FSN_Box))
                    {
                        done = false;
                        genTier1Area(level, *sub, glm::vec2(x,y), spawn);
                    }
                }
        } while (!done);
        
        // tier1
        float header = 100.f;
        glm::vec2 tier1Size = glm::vec2(arenaSize.x, arenaSize.y - header);
        tier1Size.x /= level.cols;
        tier1Size.y /= level.rows;
        
        for (float y=0; y<level.rows; ++y)
            for (float x=0; x<level.cols; ++x)
                buildTier1Area(glm::rect::withXYWH(x * tier1Size.x, header + y * tier1Size.y, tier1Size.x, tier1Size.y), level.tiles[x + y * level.cols]);
        
        // used to grow ladders
        cpShape* shape = cpBoxShapeNew(NULL, 1.f, 1.f);
        terrain->set(shape, TerrainBase);
        
        size_t ladderCollider = collide2D->addShape(shape);
        
        // grow ladders
        glm::vec3 ladderColor = glm::makeColor(150,109,47);
        for (std::list<FightSceneLadder>::iterator i=growLadders.begin(); i!=growLadders.end(); ++i)
        {
            glm::vec2 dist;
            glm::vec2 velocity(0.f, 1000.f);
            collide2D->sweptCollide(ladderCollider, i->anchor + glm::vec2(0.f, i->scanOffset), 0.f, velocity, dist);
            dist.y -= 26.f;
            if (dist.y < 0.f) dist.y = 0.f;
            terrain->addLadder(i->anchor, glm::vec2(4.f, i->scanOffset + dist.y), ladderColor);
        }
        
        // cleanup
        collide2D->removeShape(ladderCollider);
        
        // add collectables
        for (int i=0; i<PlayerTotalMasks; ++i)
            items->addItem(ItemMask, terrain->getLocation(TerrainMaskLocation));
        
        // add low-tier spawns
        int lowTierCount = (int)terrain->locationTotal(TerrainLowTierLocation);
        if (lowTierCount > 5) lowTierCount = 5;
        for (int i=0; i<lowTierCount; ++i)
            monster->addMonsterSpawn(terrain->getLocation(TerrainLowTierLocation));

        // selected players
        std::list<int> playerIds = settings->get().PlayerIds();
        std::list<int> playerClasses = settings->get().PlayerClasses();
        
        // add player controller
        std::list<int>::iterator c=playerClasses.begin();
        for (std::list<int>::iterator i=playerIds.begin(); i!=playerIds.end(); ++i, ++c)
        {
            player->addPlayerInput(PlayerInputPS4, *i);
            //
            if (*c >= PlayerClassMax)
            {
                player->addPlayer(1 + (rand() % (PlayerClassMax-1)), *i, terrain->getLocation(TerrainSpawnLocation));
            }
            else
            {
                player->addPlayer(*c, *i, terrain->getLocation(TerrainSpawnLocation));
            }
            /*/
            player->addPlayer(*c, *i, terrain->getLocation(TerrainSpawnLocation));
            //*/
        }
    }
    
    void FightScene::mapBorders(FightSceneNode& node)
    {
        // bail if we have no children
        if (node.tiles.size() == 0) return;
        
        int stride = node.cols;
        float right = node.cols-1;
        float bottom = node.rows-1;
        
        // otherwise fill out bordering info for children
        for (float y=0; y<node.rows; ++y)
            for (float x=0; x<node.cols; ++x)
            {
                int offset = x + y * node.cols;
                FightSceneNode* sub = &node.tiles[offset];
                
                if (x == 0)
                {
                    sub->left = -1;
                }
                else
                {
                    sub->left = node.tiles[offset-1].type;
                }
                if (x == right)
                {
                    sub->right = -1;
                }
                else
                {
                    sub->right = node.tiles[offset+1].type;
                }
                
                if (y == 0)
                {
                    sub->top = -1;
                }
                else
                {
                    sub->top = node.tiles[offset-stride].type;
                    if (x == 0)
                    {
                        sub->topLeft = -1;
                    }
                    else
                    {
                        sub->topLeft = node.tiles[offset-stride-1].type;
                    }
                    if (x == right)
                    {
                        sub->topRight = -1;
                    }
                    else
                    {
                        sub->topRight = node.tiles[offset-stride+1].type;
                    }
                }
                if (y == bottom)
                {
                    sub->bottom = -1;
                }
                else
                {
                    sub->bottom = node.tiles[offset+stride].type;
                    if (x == 0)
                    {
                        sub->bottomLeft = -1;
                    }
                    else
                    {
                        sub->bottomLeft = node.tiles[offset+stride-1].type;
                    }
                    if (x == right)
                    {
                        sub->bottomRight = -1;
                    }
                    else
                    {
                        sub->bottomRight = node.tiles[offset+stride+1].type;
                    }
                }
                
                mapBorders(node.tiles[offset]);
            }
    }
    
    glm::vec2 FightScene::arenaContainer()
    {
        Settings* settings = manager->request<Settings>();
        CameraController* camera = manager->request<CameraController>();
        TerrainController* terrain = manager->request<TerrainController>();
        
        // size of level
        glm::vec2 arenaSize = settings->get().Unit() * glm::vec2(2.8f, 2.0f);
        camera->setClipping(glm::rect::withMinMax(glm::vec2(0.f), arenaSize));
        
        // frame edges
        float edgeWidth = 8.f;
        float edgeSides = arenaSize.y * 5.f;
        glm::vec3 edgeColor = glm::makeColor(62,63,45);
        
        terrain->addBlock(glm::vec2(-edgeWidth, arenaSize.y * 0.5f), glm::vec2(edgeWidth * 2.f, edgeSides), edgeColor, false);
        terrain->addBlock(glm::vec2(arenaSize.x + edgeWidth, arenaSize.y * 0.5f), glm::vec2(edgeWidth * 2.f, edgeSides), edgeColor, false);
        
        float edgeHeight = 256.f;
        terrain->addBlock(glm::vec2(arenaSize.x * 0.5f, arenaSize.y), glm::vec2(arenaSize.x, edgeHeight), edgeColor, false);
        
        arenaSize.y -= edgeHeight * 0.5f;
        
        return arenaSize;
    }
    
    void FightScene::genTier1Area(FightSceneNode& parentNode, FightSceneNode& node, glm::vec2 coords, glm::vec2 spawnCoords)
    {
        // open, slab, one-sided, box, spawn
        if (coords != spawnCoords)
        {
            int weighted[10] = {
                FSN_Open, FSN_Open,
                FSN_Slab, FSN_Slab, FSN_Slab,
                FSN_OneSided, FSN_OneSided, FSN_OneSided,
                FSN_Box, FSN_Box
            };
            if (coords.y == parentNode.rows-1)
            {
                weighted[8] = weighted[9] = FSN_Open;
            }
            node.type = weighted[rand() % 10];
        }
        else
        {
            node.type = FSN_Spawn;
        }
        
        // containers tier2
        char ladderShift;
        if (node.type == FSN_Open || node.type == FSN_Box)
        {
            node.cols = node.rows = 2 + rand() % 2;
            for (float y2=0; y2<node.rows; ++y2)
            {
                ladderShift = rand() % 100 < 50 ? -1 : 1;
                for (float x2=0; x2<node.cols; ++x2)
                {
                    // tier2 node
                    FightSceneNode tier2;
                    
                    // ladder direction pref
                    tier2.ladderShift = ladderShift;
                    
                    genTier2Area(node, tier2);
                    
                    // add to tier1
                    node.tiles.push_back(tier2);
                }
            }
        }
        
    }
    
    void FightScene::genTier2Area(FightSceneNode& parentNode, FightSceneNode& node)
    {
        // open, one-sided
        int weighted[2] = {
            FSN_Open, FSN_OneSided
        };
        node.type = weighted[rand() % 2];
    }
    
    void FightScene::buildTier1Area(glm::rect rect, FightSceneNode node)
    {
        TerrainController* terrain = manager->request<TerrainController>();
        
        glm::vec3 color[2] = {
            glm::makeColor(221,181,111),
            glm::makeColor(150,109,47)
        };
        glm::vec3 edgeColor = glm::makeColor(62,63,45);
        glm::vec2 position = rect.getMin() + rect.getSize() * 0.5f;
        
        switch (node.type)
        {
            // open
            case FSN_Open:
            {
                glm::vec2 min = rect.getMin();
                glm::vec2 tier2Size = rect.getSize();
                tier2Size.x /= node.cols;
                tier2Size.y /= node.rows;
                for (float y=0; y<node.rows; ++y)
                    for (float x=0; x<node.cols; ++x)
                        buildTier2Area(glm::rect::withXYWH(min.x + x * tier2Size.x, min.y + y * tier2Size.y, tier2Size.x, tier2Size.y), node.tiles[x + y * node.cols]);
                break;
            }
                
            // slab
            case FSN_Slab:
            {
                glm::vec2 size = rect.getSize();
                size.y *= 0.5f;
                
                if (node.left != node.type && node.right != node.type)
                {
                    glm::vec2 range(32.f);
                    size = glm::linearRand(size * 0.5f, size);
                    range.x = (rect.getSize().x - size.x) * 0.5f;
                    position = glm::linearRand(position - range, position + range);
                }
                
                terrain->addBlock(position, size, glm::linearRand(color[0], color[1]), false);
                terrain->addBlock(position - glm::vec2(0.f, size.y * 0.5f - 2.f), glm::vec2(size.x, 4.f), edgeColor, false);
                
                if ((node.ladderShift == -1 && node.left != node.type) ||
                    (node.ladderShift == 1 && node.right != node.type))
                    seedLadder(node, position - glm::vec2(0.f, size.y * 0.5f), size);
                
                terrain->addLocation(TerrainMaskLocation, position - glm::vec2(0.f, size.y * 0.5f + 10.f));
                if (rand() % 100 < 20) terrain->addLocation(TerrainLowTierLocation, position - glm::vec2(0.f, size.y * 0.5f + 10.f));
                break;
            }
            
            // one-side
            case FSN_OneSided:
            {
                glm::vec2 size = rect.getSize();
                size.y = 4.f;
                
                if (node.left != node.type && node.right != node.type)
                {
                    glm::vec2 range(32.f);
                    size.x = glm::linearRand(size.x * 0.5f, size.x);
                    range.x = (rect.getSize().x - size.x) * 0.5f;
                    position = glm::linearRand(position - range, position + range);
                }

                position += glm::vec2(0.f, 2.f);
                terrain->addBackground(TerrainBackground1, position + glm::vec2(0.f, 5.f), glm::vec2(size.x, 10.f), glm::linearRand(color[0], color[1]));
                terrain->addBlock(position, size, edgeColor, true);
                
                if ((node.ladderShift == -1 && node.left != node.type) ||
                    (node.ladderShift == 1 && node.right != node.type))
                    seedLadder(node, position - glm::vec2(0.f, size.y * 0.5f), size);
                
                terrain->addLocation(TerrainMaskLocation, position - glm::vec2(0.f, size.y * 0.5f + 10.f));
                if (rand() % 100 < 20) terrain->addLocation(TerrainLowTierLocation, position - glm::vec2(0.f, size.y * 0.5f + 10.f));
                break;
            }
                
            // box
            case FSN_Box:
            {
                glm::vec3 _color = glm::linearRand(color[0], color[1]);
                glm::vec2 size = rect.getSize();
                glm::vec2 border = glm::vec2(32.f);
                glm::vec2 min = rect.getMin() + border;
                glm::vec2 max = rect.getMax() - border;
                
                terrain->addBlock(glm::vec2(min.x - border.x * 0.5f, position.y), glm::vec2(border.x, size.y), _color, false);
                terrain->addBlock(glm::vec2(max.x + border.x * 0.5f, position.y), glm::vec2(border.x, size.y), _color, false);
                
                terrain->addBlock(glm::vec2(position.x, min.y - border.y * 0.5f), glm::vec2(size.x - border.x * 2.f, border.y), _color, false);
                terrain->addBlock(glm::vec2(position.x, max.y + border.y * 0.5f), glm::vec2(size.x - border.x * 2.f, border.y), _color, false);
                
                terrain->addBlock(glm::vec2(position.x, min.y - border.y + 2.f), glm::vec2(size.x, 4.f), edgeColor, false);
                terrain->addBlock(glm::vec2(position.x, max.y + 2.f), glm::vec2(size.x - border.x * 2.f, 4.f), edgeColor, false);
                
                glm::vec2 tier2Size = (rect.getSize() - border * 2.f);
                tier2Size.x /= node.cols;
                tier2Size.y /= node.rows;
                for (float y=0; y<node.rows; ++y)
                    for (float x=0; x<node.cols; ++x)
                        buildTier2Area(glm::rect::withXYWH(min.x + x * tier2Size.x, min.y + y * tier2Size.y, tier2Size.x, tier2Size.y), node.tiles[x + y * node.cols]);
                
                seedLadder(node, glm::vec2(position.x, max.y), glm::vec2(size.x - border.x * 2.f, border.y));
                break;
            }
        
            // spawn area
            case FSN_Spawn:
            {
                terrain->addLocation(TerrainSpawnLocation, position - glm::vec2(200.f, 25.f));
                terrain->addLocation(TerrainSpawnLocation, position + glm::vec2(200.f, -25.f));
                terrain->addLocation(TerrainSpawnLocation, position - glm::vec2(65.f, 0.f));
                terrain->addLocation(TerrainSpawnLocation, position + glm::vec2(65.f, 0.f));
                break;
            }
        }
    }
    
    void FightScene::buildTier2Area(glm::rect rect, FightSceneNode node)
    {
        TerrainController* terrain = manager->request<TerrainController>();
        
        glm::vec3 color[2] = {
            glm::makeColor(221,181,111),
            glm::makeColor(150,109,47)
        };
        glm::vec3 edgeColor = glm::makeColor(62,63,45);
        glm::vec2 position = rect.getMin() + rect.getSize() * 0.5f;
        switch(node.type)
        {
            // open
            case FSN_Open:
            {
                terrain->addLocation(TerrainMaskLocation, position);
                if (rand() % 100 < 20) terrain->addLocation(TerrainLowTierLocation, position);
                break;
            }
                
            // one-side
            case FSN_OneSided:
            {
                glm::vec2 size = rect.getSize();
                size.y = 4.f;
                
                if (node.left != node.type && node.right != node.type)
                {
                    glm::vec2 range(8.f);
                    size.x = glm::linearRand(size.x * 0.5f, size.x);
                    range.x = (rect.getSize().x - size.x) * 0.5f;
                    position = glm::linearRand(position - range, position + range);
                }
                
                position += glm::vec2(0.f, 2.f);
                terrain->addBackground(TerrainBackground1, position + glm::vec2(0.f, 5.f), glm::vec2(size.x, 10.f), glm::linearRand(color[0], color[1]));
                terrain->addBlock(position, size, edgeColor, true);
                
                if ((node.ladderShift == -1 && node.left != node.type) ||
                    (node.ladderShift == 1 && node.right != node.type))
                    seedLadder(node, position - glm::vec2(0.f, size.y * 0.5f), size);
                
                terrain->addLocation(TerrainMaskLocation, position - glm::vec2(0.f, size.y * 0.5f + 10.f));
                if (rand() % 100 < 20) terrain->addLocation(TerrainLowTierLocation, position - glm::vec2(0.f, size.y * 0.5f + 10.f));
                break;
            }
        }
        
    }
    
    void FightScene::seedLadder(FightSceneNode origin, glm::vec2 anchor, glm::vec2 size)
    {
        FightSceneLadder ladder;
        
        float range = (size.x * 0.5f) - (size.x * 0.1f);
        ladder.anchor = glm::vec2(glm::linearRand(anchor.x - range, anchor.x + range), anchor.y);
        ladder.scanOffset = size.y;
        ladder.origin = origin;
        
        growLadders.push_back(ladder);
    }

}