
#pragma once

#include <list>
#include <vector>
#include <co+pilot/core/common/Maths.h>
#include <co+pilot/bundles/common/Scene.h>

namespace coda
{
    enum FightSceneNodeTypes
    {
        FSN_Open,
        FSN_Slab,
        FSN_OneSided,
        FSN_Box,
        FSN_Spawn
    };
    
    struct FightSceneNode
    {
        char type;
        float cols, rows;
        std::vector<FightSceneNode> tiles;
        char left, right;
        char top, bottom;
        char topLeft, topRight;
        char bottomLeft, bottomRight;
        char ladderShift;
    };
    
    struct FightSceneLadder
    {
        glm::vec2 anchor;
        float scanOffset;
        FightSceneNode origin;
    };
    
    class FightScene : public copilot::Scene
    {
    public:
        FightScene(copilot::CoreLogicManager* manager) : copilot::Scene(manager) { }
        void init();
        
    protected:
        void mapBorders(FightSceneNode& node);
        glm::vec2 arenaContainer();
        void genTier1Area(FightSceneNode& parentNode, FightSceneNode& node, glm::vec2 coords, glm::vec2 spawnCoords);
        void genTier2Area(FightSceneNode& parentNode, FightSceneNode& node);
        void buildTier1Area(glm::rect rect, FightSceneNode node);
        void buildTier2Area(glm::rect rect, FightSceneNode node);
        void seedLadder(FightSceneNode origin, glm::vec2 anchor, glm::vec2 size);
      
    protected:
        std::list<FightSceneLadder> growLadders;
        
    };
    
}
