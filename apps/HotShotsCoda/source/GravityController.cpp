
#include "HotShotsCoda.h"
#include "GravityController.h"

using namespace copilot;

namespace coda
{
    GravityController::GravityController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        entities->addTimer(settings->get().FPS(), GravityController::selGravity, GravityController::opUpdateGravity);
    }
    
    // selectors
    EventedSelector(GravityController::selGravity)
    {
        Props(GravityController);
        return collection->selectByProp(Props.strgravity());
    }
    
    // operations
    EventedOperation(GravityController::opUpdateGravity)
    {
        opDelta;
        Props(GravityController);
        
        Props.with(target)->velocity(Props.velocity() + Props.gravity() * delta);
    }

}

