
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{

    class GravityController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyAccessor(gravity, glm::vec2)
            PropertyAccessor(velocity, glm::vec2))
    public:
        GravityController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selGravity);
        
        // operations
        static EventedOperation(opUpdateGravity);
        
        // api
        void addZone(glm::rect area, glm::vec2 gravity);
    };
    
}