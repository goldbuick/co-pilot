
#include "HotShotsCoda.h"
#include "GunsController.h"
#include "AudioController.h"
#include "AttackController.h"
#include "TerrainController.h"
#include "ProjectileController.h"
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    GunsController::GunsController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        OtherProps(AttackController);
        
        EventedCollection* entities = manager->request<EventedCollection>();
        entities->addEvent(AttackControllerProps.evtAttack(), GunsController::selGun, GunsController::opFireGun);
    }
    
    // selectors
    EventedSelector(GunsController::selGun)
    {
        Props(GunsController);
        
        return collection->selectByProp(Props.strgun());
    }
    
    // operations
    EventedOperation(GunsController::opFireGun)
    {
        Props(GunsController);
        OtherProps(AttackController);
        
        GunsController* guns = manager->request<GunsController>();

        Props.with(target);
        AttackControllerProps.with(meta);

        // validate this attack event is for us
        if (Props.handle() != AttackControllerProps.handle() ||
            Props.slot() != AttackControllerProps.slot()) return;
        
        // generate starting position
        glm::vec2 position = AttackControllerProps.position();
        position.y += Props.offset().y;
        position += AttackControllerProps.direction() * Props.offset().x;
        
        guns->shootGun(Props.primary(), Props.team(), Props.gun(), position, AttackControllerProps.direction(), Props.color());
    }
    
    // api
    void GunsController::shootGun(bool primary, int team, int type, glm::vec2 position, glm::vec2 direction, glm::vec3 color)
    {
        Props(GunsController);
        OtherProps(GLDebugLayer);
        OtherProps(ProjectileController);
        
        ProjectileController* projectile = manager->request<ProjectileController>();
        
        PropertyCollection props;
        ProjectileControllerProps.with(props);
        ProjectileControllerProps.team(team);
        ProjectileControllerProps.color(color);
        ProjectileControllerProps.primary(primary);
        
        // get angle from direction vector
        float angle = glm::atan(direction.y, direction.x);
        
        // all weapons have random spray
        float range = 0.f;
        switch (type)
        {
            default: range = 0.04f; break;
            case GunGrenade: range = 0.01f; break;
        }
        angle += glm::linearRand(-range, range);
        
        // generate vector normal from angle
        glm::vec2 normal = glm::vec2(glm::cos(angle), glm::sin(angle));
        
        // customize based on type
        switch (type)
        {
            case GunMachine:
                ProjectileControllerProps.decay(0.2f);
                ProjectileControllerProps.playerDamage(1);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 1000.f);
                ProjectileControllerProps.size(glm::vec2(16.f, 2.f));
                manager->request<AudioController>()->randPlay("coda/sfx/gun-machine-fire", 0.8f, 1.f);
                break;
            
            case GunGnat:
                ProjectileControllerProps.decay(0.15f);
                ProjectileControllerProps.playerDamage(1);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 700.f);
                ProjectileControllerProps.size(glm::vec2(8.f, 5.f));
                break;
                
            case GunGrenade:
                ProjectileControllerProps.decay(1.5f);
                ProjectileControllerProps.playerDamage(5);
                ProjectileControllerProps.playerImpact(3.f);
                ProjectileControllerProps.velocity(normal * 400.f);
                ProjectileControllerProps.gravity(glm::vec2(0.f, 1024.f));
                ProjectileControllerProps.size(glm::vec2(8.f));
                ProjectileControllerProps.terrainBounce(0.6f);
                ProjectileControllerProps.gunType(GunGrenadeFlair);
                ProjectileControllerProps.gunCount(15);
                break;
                
            case GunGrenadeFlair:
                ProjectileControllerProps.decay(0.3f);
                ProjectileControllerProps.playerDamage(1);
                ProjectileControllerProps.playerImpact(1.0f);
                ProjectileControllerProps.velocity(normal * 500.f);
                ProjectileControllerProps.gravity(glm::vec2(0.f, 1024.f));
                ProjectileControllerProps.size(glm::vec2(6.f));
                ProjectileControllerProps.terrainBounce(0.6f);
                ProjectileControllerProps.gunType(GunGrenadeFragment);
                ProjectileControllerProps.gunCount(3);
                break;
                
            case GunGrenadeFragment:
                ProjectileControllerProps.decay(0.1f);
                ProjectileControllerProps.playerDamage(1);
                ProjectileControllerProps.playerImpact(1.0f);
                ProjectileControllerProps.velocity(normal * 500.f);
                ProjectileControllerProps.gravity(glm::vec2(0.f, 1024.f));
                ProjectileControllerProps.size(glm::vec2(3.f));
                ProjectileControllerProps.terrainBounce(0.6f);
                break;

            case GunRocket:
                ProjectileControllerProps.decay(1.0f);
                ProjectileControllerProps.playerDamage(5);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 1000.f);
                ProjectileControllerProps.slowing(1.8f);
                ProjectileControllerProps.size(glm::vec2(16.f, 3.f));
                ProjectileControllerProps.gunType(GunGrenadeFlair);
                ProjectileControllerProps.gunCount(9);
                ProjectileControllerProps.chainRange(0.25f);
                ProjectileControllerProps.chainTimeout(0.03f);
                ProjectileControllerProps.chainType(GunRocketFlair);
                break;

            case GunRocketFlair:
                ProjectileControllerProps.decay(0.3f);
                ProjectileControllerProps.playerDamage(1);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 1000.f);
                ProjectileControllerProps.size(glm::vec2(8.f, 1.f));
                break;
                
            case GunRocketHoming:
                ProjectileControllerProps.decay(1.0f);
                ProjectileControllerProps.playerDamage(5);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 1000.f);
                ProjectileControllerProps.slowing(1.8f);
                ProjectileControllerProps.size(glm::vec2(16.f, 3.f));
                ProjectileControllerProps.gunType(GunRocketHomingSecondary);
                ProjectileControllerProps.gunCount(5);
                ProjectileControllerProps.chainRange(0.35f);
                ProjectileControllerProps.chainTimeout(0.01f);
                ProjectileControllerProps.chainType(GunRocketFlair);
                break;
                
            case GunRocketHomingSecondary:
                ProjectileControllerProps.decay(1.0f);
                ProjectileControllerProps.playerDamage(5);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 1000.f);
                ProjectileControllerProps.slowing(1.8f);
                ProjectileControllerProps.size(glm::vec2(16.f, 3.f));
                ProjectileControllerProps.gunType(GunRocketHomingFragment);
                ProjectileControllerProps.gunCount(5);
                ProjectileControllerProps.chainRange(0.35f);
                ProjectileControllerProps.chainTimeout(0.01f);
                ProjectileControllerProps.chainType(GunRocketFlair);
                break;
                
            case GunRocketHomingFragment:
                ProjectileControllerProps.decay(3.0f);
                ProjectileControllerProps.playerDamage(1);
                ProjectileControllerProps.playerImpact(0.1f);
                ProjectileControllerProps.velocity(normal * 300.f);
                ProjectileControllerProps.size(glm::vec2(5.f));
                ProjectileControllerProps.homing(8.f);
                break;
                
            case GunSpiderMine:
                ProjectileControllerProps.prox(true);
                ProjectileControllerProps.decay(10.0f);
                ProjectileControllerProps.playerDamage(7);
                ProjectileControllerProps.playerImpact(0.3f);
                ProjectileControllerProps.velocity(glm::vec2(0.f));
                ProjectileControllerProps.size(glm::vec2(12.f));
                break;
        }
        
        // setup visual
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(ProjectileControllerProps.size());
        GLDebugLayerProps.glAngle(angle);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        // offset on projectile size
        position += normal * ProjectileControllerProps.size().x * 0.5f;
        ProjectileControllerProps.position(position);
        
        // add it
        projectile->addProjectile(props, primary ? TerrainProjectilePrimary : TerrainProjectileSecondary);
    }
    
    void GunsController::addGun(size_t handle, bool primary, int team, int slot, int type, glm::vec2 offset, glm::vec3 color)
    {
        Props(GunsController);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        Props.with(props);
        Props.gun(type);
        Props.team(team);
        Props.slot(slot);
        Props.color(color);
        Props.offset(offset);
        Props.handle(handle);
        Props.primary(primary);
        entities->add(settings->get().evtInit(), props);
    }
    
}
