
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    enum GunTypes
    {
        GunMachine = 1,
        
        GunGnat,
        
        GunGrenade,
        GunGrenadeFlair,
        GunGrenadeFragment,
        
        GunRocket,
        GunRocketFlair,
        
        GunRocketHoming,
        GunRocketHomingSecondary,
        GunRocketHomingFragment,
        
        GunSpiderMine,
        
        GunMax
    };
    
    class GunsController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyAccessor(gun, int)
            PropertyAccessor(team, int)
            PropertyAccessor(slot, int)
            PropertyAccessor(primary, bool)
            PropertyAccessor(color, glm::vec3)
            PropertyAccessor(handle, size_t)
            PropertyAccessor(offset, glm::vec2));
        
    public:
        GunsController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selGun);
        
        // operations
        static EventedOperation(opFireGun);
        
        // api
        void shootGun(bool primary, int team, int type, glm::vec2 position, glm::vec2 direction, glm::vec3 color);
        void addGun(size_t handle, bool primary, int team, int slot, int type, glm::vec2 offset, glm::vec3 color);
    };
    
}