
#include "FightScene.h"
#include "WinnerScene.h"
#include "HotShotsCoda.h"
#include "CharSelectScene.h"
#include "PlayerController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>

using namespace copilot;

namespace coda
{
    Settings::Settings(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Props(Settings);
        
        Props.with(settings);
        Props.FPS(60.f);
        Props.Unit(glm::vec2(640.f, 480.f));
        
        std::list<int> playerIds;
        playerIds.push_back(1);
        playerIds.push_back(2);
        playerIds.push_back(3);
        Props.PlayerIds(playerIds);
        
        std::list<int> playerClasses;
        playerClasses.push_back(PlayerClassBlerkgt);
        playerClasses.push_back(PlayerClassBlerkgt);
        playerClasses.push_back(PlayerClassBlerkgt);
        Props.PlayerClasses(playerClasses);
    }
    
    // api
    Settings::_Props Settings::get()
    {
        Props(Settings);
        
        Props.with(settings);
        return Props;
    }
    
    void Settings::playMusic(const char* resource)
    {
        AssetManager* assets = manager->request<AssetManager>();
//        PlaidAudio* plaid = manager->request<PlaidAudio>();
//        
//        if (!music.null()) plaid->getAudio()->stop(music);
//        if (resource)
//        {
//            music = assets->request<PlaidSoundRef>(resource)->ref->player(true);
//            plaid->getAudio()->play(music);
//        }
    }
    
    // get next scene
    Scene* HotShotsCoda::next(uint8_t result)
    {
        switch (result)
        {
            case SceneCharSelect: return new CharSelectScene(manager);
            case SceneFight: return new FightScene(manager);
            case SceneWinner: return new WinnerScene(manager);
        }
        
        return NULL;
    }
}