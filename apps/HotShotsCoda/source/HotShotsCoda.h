
#pragma once

#include <co+pilot/core/common/Maths.h>
//#include <co+pilot/bundles/audio/Plaid.h>
#include <co+pilot/bundles/common/Scene.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    class Settings : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyEvent(Init)
            PropertyAccessor(FPS, float)
            PropertyAccessor(Unit, glm::vec2)
            PropertyAccessor(PlayerIds, std::list<int>)
            PropertyAccessor(PlayerClasses, std::list<int>)
            PropertyAccessor(Winner, int))
        
    public:
        Settings(copilot::CoreLogicManager* manager, const char* name);
        
        // api
        Settings::_Props get();
        
        void playMusic(const char* resource);
        
    private:
        copilot::PropertyCollection settings;
        //plaid::Ref<plaid::AudioClip::Player> music;
    };
    
    enum HotShotCodaScenes
    {
        SceneQuit = 0,
        SceneCharSelect,
        SceneFight,
        SceneWinner,
        SceneMax
    };
    
    class HotShotsCoda : public copilot::SceneManager
    {
    public:
        HotShotsCoda(copilot::CoreLogicManager* manager, const char* name)
        : copilot::SceneManager(manager, name) { }
        
        // get next scene
        copilot::Scene* next(uint8_t result);
    };
    
}
