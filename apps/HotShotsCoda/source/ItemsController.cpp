
#include "HotShotsCoda.h"
#include "ItemsController.h"
#include "PlayerController.h"
#include "TerrainController.h"
#include "CollisionController.h"
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    
    ItemsController::ItemsController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        GLDebugLayer::texture(manager, glm::makeColor(255,50,50));
        GLDebugLayer::texture(manager, glm::makeColor(255,225,50));
        
        entities->addTimer(settings->get().FPS(), ItemsController::selItems, ItemsController::opUpdateItems);
    }
    
    // selectors
    EventedSelector(ItemsController::selItems)
    {
        Props(ItemsController);
        
        return collection->selectByProp(Props.stritem());
    }
    
    // operations
    EventedOperation(ItemsController::opUpdateItems)
    {
        Props(ItemsController);
        OtherProps(PlayerController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        CollisionController* collision = manager->request<CollisionController>();

        // check for overlap
        Props.with(target);
        
        std::list<Collision2D> collisions = collide2D->collide(Props.handle(), Props.position(), 0.f, glm::vec2(0.f,1.f));
        if (collisions.size())
        {
            ReadEventedEntity entity = collection->get(collision->getEntity(collisions.front().shape));

            // only players can collect
            if (entity.valid() && PlayerControllerProps.with(entity)->hasplayer())
            {
                glm::vec2 position = Props.position();
                int collectable = Props.collectable();
                
                PropertyCollection meta;
                Props.with(meta);
                Props.position(position);
                Props.collectable(collectable);
                Props.handle(entity.getHandle());
                
                collection->trigger(Props.evtCollected(), meta);
                target->setDead(true);
                
                Props.with(target);
            }
        }
    }
    
    // api
    size_t ItemsController::addLayer()
    {
        return GLDebugLayer::addLayer(manager, ItemsController::selItems);
    }
    
    void ItemsController::addItem(int type, glm::vec2 position)
    {
        Props(ItemsController);
        OtherProps(GLDebugLayer);
        
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        TerrainController* terrain = manager->request<TerrainController>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        glm::vec2 size;
        glm::vec3 color;
        switch (type)
        {
            case ItemHealth:
                size = glm::vec2(10.f);
                color = glm::makeColor(255,50,50);
                break;
                
            case ItemMask:
                size = glm::vec2(10.f);
                color = glm::makeColor(255,225,50);
                break;
        }
        
        PropertyCollection props;
        Props.with(props);
        Props.item(true);
        Props.position(position);
        Props.collectable(type);
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(size);
        GLDebugLayerProps.glAngle(glm::pi<float>() * 0.25f);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));

        cpShape* shape = cpBoxShapeNew(NULL, size.x, size.y);
        terrain->set(shape, TerrainPlayer);

        size_t collider = collide2D->addShape(shape);
        collide2D->update(collider, glm::vec2(-10000.f), 0.f);
        Props.handle(collider);
        
        entities->add(settings->get().evtInit(), props);
    }

}


