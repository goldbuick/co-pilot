
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    enum ItemTypes
    {
        ItemMask = 1,
        ItemHealth,
        ItemMax
    };
    
    class ItemsController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyEvent(Collected)
            PropertyAccessor(item, bool)
            PropertyAccessor(handle, size_t)
            PropertyAccessor(collectable, int)
            PropertyAccessor(position, glm::vec2)
        )
        
    public:
        ItemsController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selItems);
        
        // operations
        static EventedOperation(opUpdateItems);
        
        // api
        size_t addLayer();
        
        void addItem(int type, glm::vec2 position);
    };
    
}