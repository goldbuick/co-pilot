
#include "HotShotsCoda.h"
#include "MeleeController.h"
#include "AttackController.h"
#include "TerrainController.h"
#include "CollisionController.h"
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    MeleeController::MeleeController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        OtherProps(AttackController);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        entities->addEvent(AttackControllerProps.evtAttack(), MeleeController::selMelee, MeleeController::opSwingMelee);
        entities->addTimer(settings->get().FPS(), MeleeController::selActiveMelee, MeleeController::opUpdateMelee);
    }
    
    // selectors
    EventedSelector(MeleeController::selMelee)
    {
        Props(MeleeController);
        
        return collection->selectByProp(Props.strmelee());
    }
    EventedSelector(MeleeController::selActiveMelee)
    {
        Props(MeleeController);
        
        ForEachEventedSelectorByProp(melee)
            if (Props.with(*i)->meleeSweepDist() > 0.f) result.push_back(*i);
        return result;
    }
    
    // operations
    EventedOperation(MeleeController::opSwingMelee)
    {
        Props(MeleeController);
        OtherProps(AttackController);
        
        Props.with(target);
        AttackControllerProps.with(meta);
        
        // validate this attack event is for us
        if (Props.handle() != AttackControllerProps.handle() || Props.slot() != AttackControllerProps.slot()) return;

        // setup swing
        Props.meleeSweepDist(Props.attackDist());
        
        float start = glm::atan(AttackControllerProps.direction().y, AttackControllerProps.direction().x);
        float sweep = Props.attackDist();
        
        Props.meleeDirection((AttackControllerProps.direction().x < 0.f) ? -1.f : 1.f);
        Props.meleeAngle(start - (sweep * 0.5f * Props.meleeDirection()));
    }
    EventedOperation(MeleeController::opUpdateMelee)
    {
        opDelta;
        Props(MeleeController);
        OtherProps(AttackController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        CollisionController* collision = manager->request<CollisionController>();

        Props.with(target);
        
        ReadEventedEntity entity = collection->get(Props.handle());
        if (entity.valid())
        {
            glm::vec2 position = AttackControllerProps.with(entity)->position();
            position.y += Props.offset().y;
            
            AttackControllerProps.with(meta);
            
            glm::vec2 direction = glm::vec2(glm::cos(Props.meleeAngle()), glm::sin(Props.meleeAngle()));
            glm::vec2 end = position + (direction * Props.attackRadius());
            
            std::map<size_t, bool> ignores;
            std::list<Collision2D> collisions = collide2D->segmentCollide(ignores, position, end, TerrainProjectilePrimary, Props.team());
            
            for (std::list<Collision2D>::iterator i=collisions.begin(); i!=collisions.end(); ++i)
            {
                entity = collection->get(collision->getEntity(i->shape));
                if (entity.valid())
                {
                    glm::vec2 point(i->contacts.points[0].point.x, i->contacts.points[0].point.y);
                    
                    PropertyCollection props;
                    AttackControllerProps.with(props);
                    AttackControllerProps.team(Props.team());
                    AttackControllerProps.position(point);
                    AttackControllerProps.handle(entity.getHandle());
                    AttackControllerProps.damage(Props.attackDamage());
                    AttackControllerProps.impact(direction * Props.attackImpact());
                    collection->trigger(AttackControllerProps.evtImpact(), props);
                }
            }
            
            Props.meleeSweepVelocity(Props.attackSweepVelocity() * delta);
            float sweepDirection = Props.meleeSweepVelocity() * Props.meleeDirection();
            
            Props.meleeAngle(Props.meleeAngle() + sweepDirection);
            Props.meleeSweepDist(Props.meleeSweepDist() - Props.meleeSweepVelocity());
        }
        else
        {
            Props.meleeSweepDist(0.f);
        }

    }
    
    // renders
    GLRenderPipeLayer(MeleeController::renderMelee)
    {
        Props(MeleeController);
        OtherProps(AttackController);
        
        AssetManager* assetManager = manager->request<AssetManager>();
        EventedCollection* entities = manager->request<EventedCollection>();
        GLShaderRef* shader = assetManager->request<GLShaderRef>("default,default");
        
        glm::mat4 projection = render->unitProjectionOrtho(units);
        glm::mat4 transform = render->camera2DTransform(units, camera, glm::vec2(1.f));

        int count;
        float angle, step;
        glm::mat4 _transform;
        glm::vec2 size, position;
        glm::vec2 anchor = glm::vec2(0.f, 0.5f);
        
        glm::vec4 shades[5] = {
            glm::vec4(1.f, 1.f, 1.f, 1.f),
            glm::vec4(1.f, 1.f, 1.f, 0.75),
            glm::vec4(1.f, 1.f, 1.f, 0.5f),
            glm::vec4(1.f, 1.f, 1.f, 0.25f),
            glm::vec4(1.f, 1.f, 1.f, 0.12f),
        };
        glm::vec4* colors[4] = {
            NULL, NULL, NULL, NULL
        };
        
        GLTextureRef* tex = NULL;
        GLTextureRef* lastTex = NULL;
        
        GLRenderQuadPool* quadPool = batch->getQuadPool();
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            Props.with(*i);
            
            tex = assetManager->request<GLTextureRef>(GLTextureColorStr(Props.color()).c_str());
            if (tex != lastTex)
            {
                lastTex = tex;
                batch->render(*shader->ref, projection, transform, tex->ref, 1, quadPool);
            }
            
            ReadEventedEntity entity = entities->get(Props.handle());
            if (entity.valid())
            {
                position = AttackControllerProps.with(entity)->position();
                position.y += Props.offset().y;
                
                size = glm::vec2(Props.attackRadius(), 4.f);
                angle = Props.meleeAngle();
                
                count = 5;
                step = Props.meleeSweepVelocity() / (float)(count-1);
                for (int b=0; b<count; ++b)
                {
                    for (int c=0; c<4; ++c) colors[c] = &shades[b];
                    _transform = glm::rotate(angle, 0.f, 0.f, 1.f);
                    quadPool->quads[quadPool->index++] = draw->compileQuad(*tex->ref, position, size, &anchor, colors, &_transform);
                    angle -= step * Props.meleeDirection();
                }
            }
        }
        
        if (tex) batch->render(*shader->ref, projection, transform, tex->ref, 1, quadPool);
    }
    
    // api
    size_t MeleeController::addLayer()
    {
        return manager->request<GLRenderPipe>()->addLayer(MeleeController::selActiveMelee, MeleeController::renderMelee);
    }
    
    void MeleeController::addMelee(size_t handle, int team, int slot, int type, glm::vec2 offset, glm::vec3 color)
    {
        Props(MeleeController);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        Props.with(props);
        Props.melee(type);
        Props.team(team);
        Props.slot(slot);
        Props.color(color);
        Props.offset(offset);
        Props.handle(handle);
        
        switch (Props.melee())
        {
            case MeleeClub:
                Props.attackDist(glm::pi<float>() * 0.55f);
                Props.attackDamage(1);
                Props.attackImpact(512.f);
                Props.attackRadius(64.f);
                Props.attackSweepVelocity(Props.attackDist() * 12.f);
                break;
            case MeleeBossChop:
                Props.attackDist(glm::pi<float>() * 0.55f);
                Props.attackDamage(10);
                Props.attackImpact(512.f);
                Props.attackRadius(350.f);
                Props.attackSweepVelocity(Props.attackDist() * 6.f);
                break;
        }
        
        entities->add(settings->get().evtInit(), props);
    }
    
}
