
#pragma once

#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    enum MeleeTypes
    {
        MeleeClub = 1,
        MeleeBossChop,
        MeleeMax
    };
    
    class MeleeController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            // collision & type info
            PropertyAccessor(melee, int)
            PropertyAccessor(team, int)
            PropertyAccessor(slot, int)
            PropertyAccessor(color, glm::vec3)
            PropertyAccessor(handle, size_t)
            PropertyAccessor(offset, glm::vec2)
            
            // action props
            PropertyAccessor(attackDist, float)
            PropertyAccessor(attackDamage, int)
            PropertyAccessor(attackImpact, float)
            PropertyAccessor(attackRadius, float)
            PropertyAccessor(attackSweepVelocity, float)
            
            // active melee values
            PropertyAccessor(meleeAngle, float)
            PropertyAccessor(meleeDirection, float)
            PropertyAccessor(meleeSweepDist, float)
            PropertyAccessor(meleeSweepVelocity, float)
        );
        
    public:
        MeleeController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selMelee);
        static EventedSelector(selActiveMelee);
        
        // operations
        static EventedOperation(opSwingMelee);
        static EventedOperation(opUpdateMelee);
        
        // renders
        static GLRenderPipeLayer(renderMelee);
        
        // api
        size_t addLayer();
        void addMelee(size_t handle, int team, int slot, int type, glm::vec2 offset, glm::vec3 color);
    };
    
}