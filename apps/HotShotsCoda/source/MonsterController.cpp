
#include "HotShotsCoda.h"
#include "ItemsController.h"
#include "AttackController.h"
#include "MonsterController.h"
#include "TerrainController.h"
#include "CollisionController.h"
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    
    MonsterController::MonsterController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        OtherProps(AttackController);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        GLDebugLayer::texture(manager, glm::makeColor(200, 64, 20));
        
        entities->addEvent(AttackControllerProps.evtImpact(), MonsterController::selMonsterByHandle, MonsterController::opHitMonster);
        entities->addTimer(settings->get().FPS(), MonsterController::selMonsters, MonsterController::opUpdateMonsters);
        entities->addTimer(settings->get().FPS(), MonsterController::selMonsterSpawns, MonsterController::opUpdateMonsterSpawns);
    }
    
    // selectors
    EventedSelector(MonsterController::selMonsterSpawns)
    {
        NamedProps(MonsterController, Spawn);
        
        return collection->selectByProp(Spawn.strmonsterSpawn());
    }
    EventedSelector(MonsterController::selMonsters)
    {
        Props(MonsterController);
        
        return collection->selectByProp(Props.strmonster());
    }
    EventedSelector(MonsterController::selMonsterByHandle)
    {
        Props(MonsterController);
        
        std::list<copilot::ReadEventedEntity> result = EventedCollection::selByHandle(manager, collection, meta, current);
        if (!result.size() || !Props.with(result.front())->hasmonster()) result.clear();
        return result;
    }
    
    // operations
    EventedOperation(MonsterController::opUpdateMonsterSpawns)
    {
        opDelta;
        OtherProps(GLDebugLayer);
        NamedProps(MonsterController, Spawn);
        
        MonsterController* monster = manager->request<MonsterController>();
        
        Spawn.with(target);
        if (Spawn.countdown(Spawn.countdown() - delta) < 0.f)
        {
            Spawn.countdown(Spawn.monsterSpawn());
            monster->addMonster(Spawn.position());
        }
        
        float rate = delta * (Spawn.countdown() < 1.f ? 6.f : 1.f);
        if (Spawn.spin(Spawn.spin() + rate) < glm::pi<float>() * 2.f)
            Spawn.spin(Spawn.spin() - glm::pi<float>() * 2.f);
        
        GLDebugLayerProps.with(target)->glAngle(Spawn.spin());
    }
    EventedOperation(MonsterController::opUpdateMonsters)
    {
        Props(MonsterController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        
        Props.with(target);
        glm::vec2 velocity = Props.velocity();
        velocity.x = Props.walk() * 64.f;
        
        Props.velocity(velocity);
        
        // look ahead for turn around
        bool shouldTurn = false;
        glm::vec2 direction(Props.walk(), 0.f);
        
        glm::vec2 start, end;
        std::map<size_t, bool> ignores;
        
        start = Props.position() + glm::vec2((Props.size().x * 0.5 + 2.f) * Props.walk(), Props.size().y * -0.5);
        end = start + glm::vec2(0.f, Props.size().y);
        if (collide2D->collide(Props.collider(), Props.position() + direction, 0.f, direction).size() > 0)
            shouldTurn = true;
        
        start = Props.position() + glm::vec2(Props.size().x * 0.5 * Props.walk(), Props.size().y * -0.5) + glm::vec2(0.f, 1.f);
        end = start + glm::vec2(0.f, Props.size().y);
        if (collide2D->segmentCollide(ignores, start, end, TerrainPlayerBase, 1111).size() == 0)
            shouldTurn = true;
        
        if (shouldTurn) Props.walk(Props.walk() * -1);
    }
    EventedOperation(MonsterController::opHitMonster)
    {
        Props(MonsterController);
        OtherProps(AttackController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        ItemsController* items = manager->request<ItemsController>();
        
        Props.with(target);
        AttackControllerProps.with(meta);
        
        if (Props.health(Props.health() - AttackControllerProps.damage()) <= 0.f)
        {
            target->setDead(true);
            collide2D->removeShape(Props.collider());
            
            // drop buff
            items->addItem(ItemHealth, Props.position());
        }
    }
    
    // api
    size_t MonsterController::addSpawnLayer()
    {
        return GLDebugLayer::addLayer(manager, MonsterController::selMonsterSpawns);
    }
    size_t MonsterController::addMonsterLayer()
    {
        return GLDebugLayer::addLayer(manager, MonsterController::selMonsters);
    }
    
    void MonsterController::addMonsterSpawn(glm::vec2 position)
    {
        OtherProps(GLDebugLayer);
        NamedProps(MonsterController, Spawn);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        Spawn.with(props);
        Spawn.monsterSpawn(30 + rand() % 60);
        Spawn.position(position - glm::vec2(0.f, 12.f));
        Spawn.countdown(Spawn.monsterSpawn());
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(glm::vec2(32.f));
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, glm::makeColor(32, 64, 200)));
        
        entities->add(settings->get().evtInit(), props);
    }
    
    void MonsterController::addMonster(glm::vec2 position)
    {
        OtherProps(GLDebugLayer);
        Props(MonsterController);
        
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        TerrainController* terrain = manager->request<TerrainController>();
        EventedCollection* entities = manager->request<EventedCollection>();
        CollisionController* collision = manager->request<CollisionController>();
        
        PropertyCollection props;
        Props.with(props);
        Props.size(glm::vec2(16.f));
        Props.monster(true);
        Props.position(position);
        Props.health(20);
        Props.gravity(glm::vec2(0.f, 2048.f));
        Props.walk(rand() % 100 < 50 ? -1.f : 1.f);
        
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(Props.size());
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, glm::makeColor(200, 64, 20)));
        
        cpShape* shape = cpBoxShapeNew(NULL, Props.size().x, Props.size().y);
        terrain->set(shape, TerrainPlayerBase, 1111);
        
        Props.collider(collide2D->addShape(shape));
        collide2D->update(Props.collider(), position, 0.f);
        
        size_t entity = entities->add(settings->get().evtInit(), props);
        collision->setEntity(Props.collider(), entity);
    }
    
}


