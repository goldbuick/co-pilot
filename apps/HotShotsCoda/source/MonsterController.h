
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    
    class MonsterController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Spawn,
            PropertyAccessor(spin, float)
            PropertyAccessor(monsterSpawn, float)
            PropertyAccessor(countdown, float)
            PropertyAccessor(position, glm::vec2))
        
        PropertyInterfaceEntity(Props,
            PropertyAccessor(monster, bool)
            PropertyAccessor(size, glm::vec2)
            PropertyAccessor(walk, float)
            PropertyAccessor(health, float)
            PropertyAccessor(position, glm::vec2)
            PropertyAccessor(velocity, glm::vec2)
            PropertyAccessor(gravity, glm::vec2)
            PropertyAccessor(collider, size_t))
        
    public:
        MonsterController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selMonsterSpawns);
        static EventedSelector(selMonsters);
        static EventedSelector(selMonsterByHandle);
        
        // operations
        static EventedOperation(opUpdateMonsterSpawns);
        static EventedOperation(opUpdateMonsters);
        static EventedOperation(opHitMonster);
        
        // api
        size_t addSpawnLayer();
        size_t addMonsterLayer();
        void addMonsterSpawn(glm::vec2 position);
        void addMonster(glm::vec2 position);
    };
    
}