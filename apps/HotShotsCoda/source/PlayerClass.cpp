
#include "HotShotsCoda.h"
#include "PlayerClass.h"

using namespace copilot;

namespace coda
{
    
    PlayerClass::PlayerClass(copilot::CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        EventedCollection* entities = manager->request<EventedCollection>();
        entities->addTimer(settings::GameFPS, PlayerClass::selClass, PlayerClass::opUpdateClass);
    }
    
    // selectors
    EventedSelector(PlayerClass::selClass)
    {
        ForEachEventedSelector if (i->has<bool>("className")) result.push_back(*i);
        return result;
    }
    
    // operations
    EventedOperation(PlayerClass::opUpdateClass)
    {
        float delta = meta.get<float>("delta");
        
    }
    
    // api
    void PlayerClass::setClass(copilot::PropertyCollection& props)
    {
        
    }
    
}
