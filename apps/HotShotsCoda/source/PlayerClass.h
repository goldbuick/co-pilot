
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    
    class PlayerClass : public copilot::CoreLogic
    {
    public:
        PlayerClass(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selClass);
        
        // operations
        static EventedOperation(opUpdateClass);
        
        // api
        void setClass(copilot::PropertyCollection& props);
    };
    
}