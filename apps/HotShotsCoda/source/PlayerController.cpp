
#include "HotShotsCoda.h"
#include "GunsController.h"
#include "ItemsController.h"
#include "MeleeController.h"
#include "AttackController.h"
#include "PlayerController.h"
#include "GravityController.h"
#include "TerrainController.h"
#include "CollisionController.h"
#include "ProjectileController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/sdl/InputMap.h>
#include <co+pilot/bundles/render/GLFont.h>
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/property/PropMods.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>

using namespace copilot;

namespace coda
{
    
    PlayerController::PlayerController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Props(PlayerController);
        OtherProps(ItemsController);
        OtherProps(AttackController);
        NamedProps(PlayerController, State);
        
        Settings* settings = manager->request<Settings>();
        AssetManager* assetManager = manager->request<AssetManager>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        // spool up dependent controllers
        manager->request<GunsController>();
        manager->request<MeleeController>();
        manager->request<GravityController>();
        manager->request<CollisionController>();
        
        // add events
        entities->addTimer(settings->get().FPS(), PlayerController::selState, PlayerController::opUpdateState);
        entities->addEvent(Props.evtPlayerProps(), PlayerController::selPlayer, EventedCollection::opCopyAllFromMeta);
        entities->addEvent(AttackControllerProps.evtImpact(), PlayerController::selPlayerByHandle, PlayerController::opHitPlayer);
        entities->addEvent(ItemsControllerProps.evtCollected(), PlayerController::selPlayerByHandle, PlayerController::opCollectedPlayer);
        
        PropertyCollection props;
        State.with(props);
        State.state(true);
        State.countdown(PlayerMatchStart);
        entities->add(settings->get().evtInit(), props);
        
        assetManager->request<GLTextureRef>("0.4,0.1,0.1");
        assetManager->request<GLTextureRef>("1.0,0.1,0.1");
        assetManager->request<GLTextureRef>("0.1,0.1,0.1");
        assetManager->request<GLTextureRef>("1.0,1.0,1.0");
        assetManager->request<GLTextureRef>("1.0,0.98,0.2");
        assetManager->request<GLFontTextureRef>("default,64");
    }
    
    // selectors
    EventedSelector(PlayerController::selState)
    {
        NamedProps(PlayerController, State);
        
        return collection->selectByProp(State.strstate());
    }
    EventedSelector(PlayerController::selPlayer)
    {
        Props(PlayerController);
        
        int player = Props.with(meta)->player();
        ForEachEventedSelectorByProp(player)
            if (Props.with(*i)->player() == player) result.push_back(*i);
        return result;
    }
    EventedSelector(PlayerController::selPlayers)
    {
        Props(PlayerController);
        
        return collection->selectByProp(Props.strplayer());
    }
    EventedSelector(PlayerController::selValidPlayers)
    {
        Props(PlayerController);
        
        ForEachEventedSelectorByProp(player)
            if (!Props.with(*i)->flagMonster()) result.push_back(*i);
        return result;
    }
    EventedSelector(PlayerController::selPlayerByHandle)
    {
        Props(PlayerController);
        
        std::list<copilot::ReadEventedEntity> result = EventedCollection::selByHandle(manager, collection, meta, current);
        if (!result.size() || !Props.with(result.front())->hasplayer()) result.clear();
        return result;
    }
    
    // operations
    EventedOperation(PlayerController::opUpdateState)
    {
        opDelta;
        Props(PlayerController);
        NamedProps(PlayerController, State);
        
        Settings* settings = manager->request<Settings>();
        TerrainController* terrain = manager->request<TerrainController>();
        
        std::list<copilot::ReadEventedEntity> players = collection->select(PlayerController::selValidPlayers);
        if (players.size() == 1)
        {
            // last player standing!
            Props.with(players.front());
            settings->get().Winner(Props.player());
            Scene::setResult(manager, SceneWinner);
        }
        
        State.with(target);
        if (!State.go() && State.countdown(State.countdown() - delta) < 0.f)
        {
            State.go(true);
            terrain->clearSpawns();
            collection->addTimer(settings->get().FPS(), PlayerController::selPlayers, PlayerController::opUpdatePlayer);
        }
    }
    EventedOperation(PlayerController::opUpdatePlayer)
    {
        opDelta;
        Props(PlayerController);
        OtherProps(AttackController);
        OtherProps(GravityController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        ItemsController* items = manager->request<ItemsController>();
        PlayerController* player = manager->request<PlayerController>();
        TerrainController* terrain = manager->request<TerrainController>();
        
        Props.with(target);
        
        // deco
        Props.masksOffset(Props.masksOffset() + delta * (float)(Props.masks() * 4.f));
        
        // updating velocity
        glm::vec2 velocity = Props.velocity();
        
        GravityControllerProps.with(target);
        
        bool gvReset = false;
        if (Props.flagBoss())
        {
            // running
            const float maxSpeed = 512.f;
            const glm::vec2 accSpeed = Props.flagMove() * maxSpeed * 6.f * delta;
            
            // move it
            velocity = glm::clamp(velocity + accSpeed, -glm::vec2(maxSpeed), glm::vec2(maxSpeed));
            
            // general friction
            velocity -= velocity * delta * 6.f;
            
            // boss health always draining
            if (Props.health(Props.health() - delta * 10.f) < 0.f)
            {
                for (int i=0; i<PlayerTotalMasks; ++i)
                    items->addItem(ItemMask, terrain->getLocation(TerrainMaskLocation));
                
                target->setDead(true);
                collide2D->removeShape(Props.collider());
                collide2D->removeShape(Props.footCollider());
                player->addPlayer(player->playerForm(Props.playerClass()), Props.player(), Props.position());
                return;
            }
        }
        else
        {
            // grab delay
            if (Props.flagGrabDelay() >= 0.f)
                Props.flagGrabDelay(Props.flagGrabDelay() - delta);
            
            // extra filtering for move flag
            glm::vec2 flagMove = Props.flagMove();
            if (flagMove.x > fAxisMin && flagMove.x < fAxisMax) flagMove.x = 0.f;
            if (flagMove.y > fAxisMin && flagMove.y < fAxisMax) flagMove.y = 0.f;
            
            // track facing direction
            if (flagMove.x != 0.f) Props.flagFacing(glm::sign(flagMove.x));
            
            // not-dashing
            if (Props.flagDashing() < 0.f)
            {
                // check for ladder overlap
                glm::vec2 ladderCenter;
                glm::vec2 ladderOffset(0.f, flagMove.y > 0.f ? 3.f : 0.f);
                bool onLadder = terrain->collideWithLadder(Props.collider(),
                                                           Props.position() + ladderOffset, 0.f, glm::vec2(0.f), ladderCenter);
                // grab ladder
                if (onLadder)
                {
                    // press up or down to grab ladder
                    if (!Props.flagClimbing() && flagMove.y != 0.f && Props.flagGrabDelay() < 0.f)
                    {
                        Props.flagClimbing(true);
                        
                        // snap to ladder
                        glm::vec2 position = Props.position();
                        position.x = ladderCenter.x;
                        Props.position(position);
                        
                        // change velocity
                        velocity = glm::vec2(0.f);
                        GravityControllerProps.gravity(glm::vec2(0.f));
                        
                        // change layers
                        cpShape* shape = collide2D->getShape(Props.collider());
                        if (shape) terrain->set(shape, TerrainPlayerLadder);
                        
                        // trigger grab
                        collection->trigger(Props.evtPlayerLadderGrab());
                    }
                    if (Props.flagClimbing())
                    {
                        velocity.y = flagMove.y * 256.f;
                    }
                }
                else if (Props.flagClimbing())
                {
                    gvReset = true;
                    // reset velocity
                    if (flagMove.y <= 0.f)
                    {
                        velocity = glm::vec2(0.f);
                    }
                }
                
                // running
                const float maxSpeed = Props.flagMonster() ? 128.f : 512.f;
                const float accRate = Props.flagMonster() ? 3.f : 6.f;
                const float accSpeed = flagMove.x * maxSpeed * accRate * delta;
                
                if (!Props.flagClimbing()) velocity.x = glm::clamp(velocity.x + accSpeed, -maxSpeed, maxSpeed);
                
                // running friction
                velocity.x -= velocity.x * delta * (flagMove.x ? 0.f : 32.f);
                
                // jumping
                if (Props.flagJump() && (Props.flagGrounded() || Props.flagClimbing()))
                {
                    velocity.y = Props.flagMonster() ? -500.f : -700.f;
                    Props.flagGrounded(false);
                    if (Props.flagClimbing()) gvReset = true;
                    Props.flagGrabDelay(0.1f);
                }
                
                // can we jump
                if (Props.velocity().y >= 0.f)
                {
                    Props.flagGrounded(collide2D->collide(Props.footCollider(),
                                                          Props.position() + Props.footOffset(), 0.f, glm::vec2(0.f, 1.f)).size() > 0);
                }
            }
            else
            {
                if (Props.flagDashing(Props.flagDashing() - delta) < 0.f || Props.flagDash())
                {
                    gvReset = true;
                    Props.flagDashing(-1.f);
                }
            }
            
            // dash
            if (Props.flagDashPressed() && !Props.flagDash())
            {
                PropMods::cooldownTrigger(target, Props.strflagDash(), Props.cooldownDash());
                
                Props.flagDashPressed(false);
                Props.flagDashing(Props.dashDuration());
                
                // change velocity
                velocity = glm::vec2(Props.flagFacing() * Props.dashVelocity(), 0.f);
                GravityControllerProps.gravity(glm::vec2(0.f));
            }
            if (PropMods::cooldownUpdate(target, Props.strflagDash(), delta) && Props.flagDashing() < 0.f && Props.flagDash())
            {
                Props.flagDashPressed(true);
            }
        }
        
        // track facing direction
        if (Props.flagAim().x != 0.f || Props.flagAim().y != 0.f)
        {
            Props.flagLockedAim(Props.flagAim());
        }
        
        // trigger attack events
        int team = Props.team();
        float readyIdle = -1000.f;
        
        if (!Props.flagClimbing() &&
            PropMods::cooldownUpdate(target, Props.strflagAttack1(), delta) &&
            (Props.flagAttack1()))// || Props.flagAim().x != 0.f || Props.flagAim().y != 0.f))
        {
            PropMods::cooldownTrigger(target, Props.strflagAttack1(), Props.cooldownAttack1());
    
            // attack1
            Props.flagAttack1Ready(Props.readyAttack1());
            velocity += Props.flagLockedAim() * Props.kickbackAttack1();
        }
        
        if (Props.flagAttack1Ready() > 0.f)
        {
            Props.flagAttack1Ready(Props.flagAttack1Ready() - delta);
        }
        else if (Props.flagAttack1Ready() <= 0.f && Props.flagAttack1Ready() != readyIdle)
        {
            Props.flagAttack1Ready(-1000.f);
            
            // attack1
            PropertyCollection meta;
            AttackControllerProps.with(meta);
            AttackControllerProps.slot(0);
            AttackControllerProps.team(team);
            AttackControllerProps.direction(Props.flagLockedAim() * Props.directionAttack1());
            AttackControllerProps.position(Props.position());
            AttackControllerProps.handle(target->getHandle());
            collection->trigger(AttackControllerProps.evtAttack(), meta);
        }
        
        if (!Props.flagClimbing() && PropMods::cooldownUpdate(target, Props.strflagAttack2(), delta) && Props.flagAttack2())
        {
            PropMods::cooldownTrigger(target, Props.strflagAttack2(), Props.cooldownAttack2());

            // attack2
            Props.flagAttack2Ready(Props.readyAttack2());
            velocity += Props.flagLockedAim() * Props.kickbackAttack2();
        }
        
        if (Props.flagAttack2Ready() > 0.f)
        {
            Props.flagAttack2Ready(Props.flagAttack2Ready() - delta);
        }
        else if (Props.flagAttack2Ready() <= 0.f && Props.flagAttack2Ready() != readyIdle)
        {
            Props.flagAttack2Ready(-1000.f);
            
            // attack2
            PropertyCollection meta;
            AttackControllerProps.with(meta);
            AttackControllerProps.slot(1);
            AttackControllerProps.team(team);
            AttackControllerProps.direction(Props.flagLockedAim() * Props.directionAttack2());
            AttackControllerProps.position(Props.position());
            AttackControllerProps.handle(target->getHandle());
            collection->trigger(AttackControllerProps.evtAttack(), meta);
        }
        
        if (gvReset)
        {
            Props.flagClimbing(false);
            
            // reset gravity
            GravityControllerProps.gravity(Props.gravityAmount());
            
            // reset layers
            cpShape* shape = collide2D->getShape(Props.collider());
            if (shape) terrain->set(shape, TerrainPlayerBase, Props.team());
        }
        
        // inertia tweaks velocity
        velocity += (Props.inertia() * glm::vec2(64.f, 32.f)) * delta;
        
        // update inertia & velocity
        Props.inertia(Props.inertia() - Props.inertia() * delta * 16.f);
        Props.velocity(velocity);
    }
    EventedOperation(PlayerController::opHitPlayer)
    {
        Props(PlayerController);
        OtherProps(AttackController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        ItemsController* items = manager->request<ItemsController>();
        PlayerController* player = manager->request<PlayerController>();
        TerrainController* terrain = manager->request<TerrainController>();
        
        Props.with(target);
        AttackControllerProps.with(meta);
        
        bool shouldImpact = true;
        if (Props.flagClimbing())
        {
            // set to off-ladder state
            cpShape* shape = collide2D->getShape(Props.collider());
            if (shape) terrain->set(shape, TerrainPlayerBase, Props.team());
            
            // check to see if we overlap anything
            shouldImpact = collide2D->collide(Props.collider(), Props.position(), 0.f, glm::vec2(0.f,1.f)).size() > 0;
            
            // set to on-ladder state
            shape = collide2D->getShape(Props.collider());
            if (shape) terrain->set(shape, TerrainPlayerLadder, Props.team());
        }
        if (shouldImpact) Props.inertia(AttackControllerProps.impact());
        
        if (Props.health(Props.health() - AttackControllerProps.damage()) <= 0.f)
        {
            target->setDead(true);
            collide2D->removeShape(Props.collider());
            collide2D->removeShape(Props.footCollider());
            if (Props.flagBoss())
            {
                player->addPlayer(player->playerForm(Props.playerClass()), Props.player(), Props.position());
            }
            else
            {
                for (int i=0; i<Props.masks(); ++i)
                    items->addItem(ItemMask, terrain->getLocation(TerrainMaskLocation));
                player->addPlayer(player->monsterForm(Props.playerClass()), Props.player(), terrain->getLocation(TerrainSpawnLocation));
            }
        }
        else if (rand() % 100 < 6 && Props.masks())
        {
            Props.masks(Props.masks()-1);
            items->addItem(ItemMask, terrain->getLocation(TerrainMaskLocation));
        }
    }
    EventedOperation(PlayerController::opCollectedPlayer)
    {
        Props(PlayerController);
        OtherProps(ItemsController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        PlayerController* player = manager->request<PlayerController>();
     
        Props.with(target);
        ItemsControllerProps.with(meta);
        
        switch (ItemsControllerProps.collectable())
        {
            case ItemHealth:
                if (Props.health(Props.health() + 5) > Props.maxHealth())
                    Props.health(Props.maxHealth());
                break;
                
            case ItemMask:
                if (Props.masks(Props.masks() + 1) == PlayerTotalMasks)
                {
                    target->setDead(true);
                    collide2D->removeShape(Props.collider());
                    collide2D->removeShape(Props.footCollider());
                    player->addPlayer(player->bossForm(Props.playerClass()), Props.player(), Props.position());
                }
                break;
        }
    }
    
    // render
    GLRenderPipeLayer(PlayerController::renderPlayers)
    {
        Props(PlayerController);
        OtherProps(ItemsController);
        
        AssetManager* assetManager = manager->request<AssetManager>();
        GLShaderRef* shader = assetManager->request<GLShaderRef>("default,default");
        
        glm::mat4 projection = render->unitProjectionOrtho(units);
        glm::mat4 transform = render->camera2DTransform(units, camera, glm::vec2(1.f));
        
        GLTextureRef* tex1 = assetManager->request<GLTextureRef>("0.4,0.1,0.1");
        GLTextureRef* tex2 = assetManager->request<GLTextureRef>("1.0,0.1,0.1");
        GLTextureRef* tex3a = assetManager->request<GLTextureRef>("0.1,0.1,0.1");
        GLTextureRef* tex3b = assetManager->request<GLTextureRef>("1.0,1.0,1.0");
        GLTextureRef* tex4 = assetManager->request<GLTextureRef>("1.0,0.98,0.2");
        
        GLRenderQuadPool* quadPool = batch->getQuadPool();
        
        // health container
        float gap = 8.f;
        float healthRatio = 2.f;
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            Props.with(*i);
            if (!Props.flagMonster())
            {
                float healthMax = Props.maxHealth() / healthRatio;
                glm::vec2 size(healthMax + 2.f, 6.f);
                glm::vec2 position = Props.position() - glm::vec2(0.f, Props.size().y * 0.5f + gap);
                
                quadPool->quads[quadPool->index++] = draw->compileQuad(*tex1->ref, position, size);
            }
        }
        batch->render(*shader->ref, projection, transform, tex1->ref, 1, quadPool);
        
        // health
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            Props.with(*i);
            if (!Props.flagMonster())
            {
                float health = Props.health() / healthRatio;
                float healthMax = Props.maxHealth() / healthRatio;
                glm::vec2 size(health, 4.f);
                glm::vec2 position = Props.position()
                - glm::vec2(healthMax * 0.5f, Props.size().y * 0.5f + gap)
                + glm::vec2(size.x * 0.5f, 0.f);
                
                quadPool->quads[quadPool->index++] = draw->compileQuad(*tex2->ref, position, size);
            }
        }
        batch->render(*shader->ref, projection, transform, tex2->ref, 1, quadPool);
        
        // lockedAim
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            Props.with(*i);
            glm::vec2 size(6.f);
            glm::vec2 position = Props.position();
            position.y += Props.mount().y;
            position += Props.flagLockedAim() * Props.mount().x;
            
            quadPool->quads[quadPool->index++] = draw->compileQuad(*tex3a->ref, position, size);
        }
        batch->render(*shader->ref, projection, transform, tex3a->ref, 1, quadPool);
        
        // aim
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            Props.with(*i);
            glm::vec2 size(3.f);
            glm::vec2 position = Props.position();
            position.y += Props.mount().y;
            position += Props.flagAim() * Props.mount().x;
            
            quadPool->quads[quadPool->index++] = draw->compileQuad(*tex3b->ref, position, size);
        }
        batch->render(*shader->ref, projection, transform, tex3b->ref, 1, quadPool);
        
        // masks
        for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
        {
            Props.with(*i);
            if (Props.masks())
            {
                glm::vec2 normal;
                glm::vec2 position;
                glm::vec2 size(8.f);
                float pi2 = glm::pi<float>() * 2.f;
                float step = pi2 / (float)Props.masks();
                float angle = glm::mod(Props.masksOffset(), pi2);
                for (int m=0; m<Props.masks(); ++m)
                {
                    normal = glm::vec2(glm::cos(angle), glm::sin(angle));
                    position = Props.position() + normal * 20.f;
                    quadPool->quads[quadPool->index++] = draw->compileQuad(*tex4->ref, position, size);
                    angle += step;
                }
            }
        }
        batch->render(*shader->ref, projection, transform, tex4->ref, 1, quadPool);
    }
    
    GLRenderPipeLayer(PlayerController::renderState)
    {
        NamedProps(PlayerController, State);
        
        AssetManager* assetManager = manager->request<AssetManager>();
        GLShaderRef* shader = assetManager->request<GLShaderRef>("default,default");
        
        glm::mat4 projection = render->unitProjectionOrtho(units);
        glm::mat4 transform;
        
        GLFont* font = manager->request<GLFont>();
        GLFontTextureRef* fontTex = assetManager->request<GLFontTextureRef>("default,64");
        
        if (selection.size())
        {
            State.with(selection.front());
            if (!State.go())
            {
                GLRenderQuadPool* quadPool = batch->getQuadPool();
                std::string count = strPrint("Starting in %i", (int)(State.countdown() + 1));
                glm::vec2 position = (units->calculated * 0.5f) - (font->getTextSize(fontTex->ref, count.c_str()) * 0.5f);
                font->drawText(quadPool, fontTex->ref, position.x, position.y, count.c_str());
                batch->render(*shader->ref, projection, transform, &fontTex->ref->texture, 1, quadPool);
            }
        }
    }

    // api
    size_t PlayerController::addLayer()
    {
        return GLDebugLayer::addLayer(manager, PlayerController::selPlayers);
    }
    
    size_t PlayerController::addGuiLayer()
    {
        return manager->request<GLRenderPipe>()->addLayer(PlayerController::selPlayers, PlayerController::renderPlayers);
    }
    
    size_t PlayerController::addStateLayer()
    {
        return manager->request<GLRenderPipe>()->addLayer(PlayerController::selState, PlayerController::renderState);
    }
    
    void PlayerController::addInput(int playerId, size_t& move, size_t& aim, size_t& dash, size_t& jump, size_t& attack1, size_t& attack2)
    {
        LibSDL* sdl = manager->request<LibSDL>();
        InputMap* input = manager->request<InputMap>();
        uint8_t joystick = playerId - 1;
        
        InputMapVector playerMove;
        InputMapVector playerAim;
        InputMapButton playerDash;
        InputMapButton playerJump;
        InputMapButton playerAttack1;
        InputMapButton playerAttack2;
        
        // keyboard for player1
        if (playerId == 1)
        {
            playerMove.left.keys.push_back({SDLK_LEFT});
            playerMove.right.keys.push_back({SDLK_RIGHT});
            playerMove.up.keys.push_back({SDLK_UP});
            playerMove.down.keys.push_back({SDLK_DOWN});
            
            playerAim.left.keys.push_back({SDLK_LEFT});
            playerAim.right.keys.push_back({SDLK_RIGHT});
            playerAim.up.keys.push_back({SDLK_UP});
            playerAim.down.keys.push_back({SDLK_DOWN});
            
            playerJump.triggers.keys.push_back({SDLK_z});
            playerJump.triggers.keys.push_back({SDLK_SPACE});
            
            playerDash.triggers.keys.push_back({SDLK_x});
            playerAttack2.triggers.keys.push_back({SDLK_c});
        }
        
        // joystick inputs
        printf("player %i, %s\n", playerId, sdl->getJoystickName(joystick));
        const char* joyType = strstr(sdl->getJoystickName(joystick), "360");
        int inputType = (joyType == NULL) ? PlayerInputPS4 : PlayerInputXbox360;
        switch (inputType)
        {
            case PlayerInputXbox360:
                playerMove.threshold = AxisMax;
                playerMove.left.joystickAxises.push_back({joystick, 0, AxisMin});
                playerMove.right.joystickAxises.push_back({joystick, 0, AxisMax});
                playerMove.up.joystickAxises.push_back({joystick, 1, AxisMin});
                playerMove.down.joystickAxises.push_back({joystick, 1, AxisMax});
                move = input->addVector(playerMove);
                
                playerAim.threshold = AxisMax;
                playerAim.left.joystickAxises.push_back({joystick, 3, AxisMin});
                playerAim.right.joystickAxises.push_back({joystick, 3, AxisMax});
                playerAim.up.joystickAxises.push_back({joystick, 4, AxisMin});
                playerAim.down.joystickAxises.push_back({joystick, 4, AxisMax});
                aim = input->addVector(playerAim);
                
                playerDash.triggers.joystickButtons.push_back({joystick, 1});
                playerDash.triggers.joystickButtons.push_back({joystick, 2});
                playerDash.triggers.joystickButtons.push_back({joystick, 3});
                dash = input->addButton(playerDash);
                
                playerJump.triggers.joystickButtons.push_back({joystick, 0});
                playerJump.triggers.joystickAxises.push_back({joystick, 2, AxisMax});
                jump = input->addButton(playerJump);
                
                playerAttack1.triggers.joystickAxises.push_back({joystick, 5, AxisMax});
                attack1 = input->addButton(playerAttack1);
                
                playerAttack2.triggers.joystickButtons.push_back({joystick, 4});
                playerAttack2.triggers.joystickButtons.push_back({joystick, 5});
                attack2 = input->addButton(playerAttack2);
                break;
                
            case PlayerInputPS4:
                playerMove.threshold = AxisMax;
                playerMove.left.joystickAxises.push_back({joystick, 0, AxisMin});
                playerMove.right.joystickAxises.push_back({joystick, 0, AxisMax});
                playerMove.up.joystickAxises.push_back({joystick, 1, AxisMin});
                playerMove.down.joystickAxises.push_back({joystick, 1, AxisMax});
                move = input->addVector(playerMove);
                
                playerAim.threshold = AxisMax;
                playerAim.left.joystickAxises.push_back({joystick, 2, AxisMin});
                playerAim.right.joystickAxises.push_back({joystick, 2, AxisMax});
                playerAim.up.joystickAxises.push_back({joystick, 5, AxisMin});
                playerAim.down.joystickAxises.push_back({joystick, 5, AxisMax});
                aim = input->addVector(playerAim);
                
                playerDash.triggers.joystickButtons.push_back({joystick, 0});
                playerDash.triggers.joystickButtons.push_back({joystick, 2});
                playerDash.triggers.joystickButtons.push_back({joystick, 3});
                dash = input->addButton(playerDash);
                
                playerJump.triggers.joystickButtons.push_back({joystick, 1});
                playerJump.triggers.joystickButtons.push_back({joystick, 4});
                playerJump.triggers.joystickButtons.push_back({joystick, 5});
                jump = input->addButton(playerJump);
                
                playerAttack1.triggers.joystickAxises.push_back({joystick, 3, AxisMax});
                attack1 = input->addButton(playerAttack1);
                
                playerAttack2.triggers.joystickAxises.push_back({joystick, 4, AxisMax});
                attack2 = input->addButton(playerAttack2);
                break;
        }
    }
    
    int PlayerController::bossForm(int playerClass)
    {
        playerClass = PlayerMonsterClassMax + playerForm(playerClass);
        return playerClass;
    }
    int PlayerController::playerForm(int playerClass)
    {
        playerClass %= PlayerClassMax;
        return playerClass;
    }
    int PlayerController::monsterForm(int playerClass)
    {
        playerClass = PlayerClassMax + playerForm(playerClass);
        return playerClass;
    }
    bool PlayerController::isBossForm(int playerClass)
    {
        return (playerClass > PlayerMonsterClassMax && playerClass < PlayerBossClassMax);
    }
    bool PlayerController::isPlayerForm(int playerClass)
    {
        return (playerClass > PlayerClassNone && playerClass < PlayerClassMax);
    }
    bool PlayerController::isMonsterForm(int playerClass)
    {
        return (playerClass > PlayerClassMax && playerClass < PlayerMonsterClassMax);
    }
    
    std::string PlayerController::getClassName(int playerClass)
    {
        std::string name;
        
        switch (playerClass)
        {
            case PlayerClassNone: name = ""; break;
            case PlayerClassBlerkgt: name = "Blerkgt"; break;
            case PlayerClassFiftyNine: name = "FiftyNine"; break;
            case PlayerClassNightless: name = "Nightless"; break;
            default: name = "Random"; break;
        }
        
        return name;
    }
    
    void PlayerController::addPlayerInput(int inputType, int playerId)
    {
        Props(PlayerController);
        
        EventedCollection* entities = manager->request<EventedCollection>();
        
        // add inputs
        size_t move, aim, dash, jump, attack1, attack2;
        addInput(playerId, move, aim, dash, jump, attack1, attack2);
        
        // common data for input events
        PropertyCollection meta;
        Props.with(meta);
        Props.player(playerId);
        
        // bind input map events to triggers
        entities->addInput(move, Props.evtPlayerProps(), meta, Props.strflagMove());
        entities->addInput(aim, Props.evtPlayerProps(), meta, Props.strflagAim());
        entities->addInput(dash, Props.evtPlayerProps(), meta, Props.strflagDash());
        entities->addInput(jump, Props.evtPlayerProps(), meta, Props.strflagJump());
        entities->addInput(attack1, Props.evtPlayerProps(), meta, Props.strflagAttack1());
        entities->addInput(attack2, Props.evtPlayerProps(), meta, Props.strflagAttack2());
    }
    
    void PlayerController::addPlayer(int playerClass, int playerId, glm::vec2 position)
    {
        if (playerId < 1 || playerId > 4) return;
        Props(PlayerController);
        OtherProps(GLDebugLayer);
        OtherProps(GravityController);
        
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        GunsController* guns = manager->request<GunsController>();
        MeleeController* melee = manager->request<MeleeController>();
        TerrainController* terrain = manager->request<TerrainController>();
        EventedCollection* entities = manager->request<EventedCollection>();
        CollisionController* collision = manager->request<CollisionController>();

        // handle of entity
        PropertyCollection props;
        Props.with(props);
        Props.team(playerId);
        Props.player(playerId);
        Props.playerClass(playerClass);
        Props.position(position);
        Props.flagGrounded(false);
        Props.velocity(glm::vec2(0.f));
        Props.flagAttack1Ready(-1000.f);
        Props.flagAttack2Ready(-1000.f);
        Props.jumpAmount(-700.f);
        
        Props.cooldownDash(2.f);
        Props.dashDuration(0.25f);
        Props.dashVelocity(1024.f);
        
        // set kind flags
        Props.flagBoss(isBossForm(Props.playerClass()));
        Props.flagMonster(isMonsterForm(Props.playerClass()));
        
        // customize !
        switch (Props.playerClass())
        {
            case PlayerClassBlerkgt:
                Props.size(glm::vec2(12.f, 24.f));
                
                Props.readyAttack1(0.f);
                Props.cooldownAttack1(0.1f);
                Props.kickbackAttack1(-16.f);
                Props.directionAttack1(glm::vec2(1.f));
                break;
                
            case PlayerClassFiftyNine:
                Props.size(glm::vec2(12.f, 20.f));
                
                Props.readyAttack1(0.f);
                Props.cooldownAttack1(0.1f);
                Props.kickbackAttack1(-6.f);
                Props.directionAttack1(glm::vec2(1.f));
                break;
                
            case PlayerClassNightless:
                Props.size(glm::vec2(14.f, 24.f));
                
                Props.readyAttack1(0.f);
                Props.cooldownAttack1(0.1f);
                Props.kickbackAttack1(-16.f);
                Props.directionAttack1(glm::vec2(1.f));
                break;
                
            case PlayerBossClassBlerkgt:
            case PlayerBossClassFiftyNine:
            case PlayerBossClassNightless:
                Props.size(glm::vec2(200.f));
                
                Props.readyAttack1(0.f);
                Props.cooldownAttack1(1.f);
                Props.kickbackAttack1(0.f);
                Props.directionAttack1(glm::vec2(1.f));
                break;
                
            case PlayerMonsterClassBlerkgt:
            case PlayerMonsterClassFiftyNine:
            case PlayerMonsterClassNightless:
                Props.size(glm::vec2(24.f, 24.f));
                
                Props.readyAttack1(0.f);
                Props.cooldownAttack1(0.3f);
                Props.kickbackAttack1(-12.f);
                Props.directionAttack1(glm::vec2(1.f));
                break;
        }

        switch (Props.playerClass())
        {
            case PlayerClassBlerkgt:
            case PlayerClassNightless:
                Props.readyAttack2(0.1f);
                Props.cooldownAttack2(1.0f);
                Props.kickbackAttack2(-32.f);
                Props.directionAttack2(glm::vec2(1.f));
                break;
                
            case PlayerClassFiftyNine:
                Props.readyAttack2(0.1f);
                Props.cooldownAttack2(1.5f);
                Props.kickbackAttack2(-32.f);
                Props.directionAttack2(glm::vec2(1.f));
                break;
                
            case PlayerBossClassBlerkgt:
            case PlayerBossClassFiftyNine:
            case PlayerBossClassNightless:
                Props.readyAttack2(0.1f);
                Props.cooldownAttack2(1.0f);
                Props.kickbackAttack2(-2.f);
                Props.directionAttack2(glm::vec2(1.f));
                break;
                
            case PlayerMonsterClassBlerkgt:
            case PlayerMonsterClassFiftyNine:
            case PlayerMonsterClassNightless:
                Props.size(glm::vec2(24.f, 24.f));
                
                Props.readyAttack2(0.f);
                Props.cooldownAttack2(0.1f);
                Props.kickbackAttack2(-12.f);
                Props.directionAttack2(glm::vec2(1.f));
                break;
        }
        
        switch (Props.playerClass())
        {
            default:
                Props.health(100);
                Props.gravityAmount(glm::vec2(0.f, 2048.f));
                break;
                
            case PlayerBossClassBlerkgt:
            case PlayerBossClassFiftyNine:
            case PlayerBossClassNightless:
                Props.health(500);
                Props.gravityAmount(glm::vec2(0.f));
                break;
                
            case PlayerMonsterClassBlerkgt:
            case PlayerMonsterClassFiftyNine:
            case PlayerMonsterClassNightless:
                Props.health(50);
                Props.gravityAmount(glm::vec2(0.f, 2048.f));
                break;
        }
        Props.maxHealth(Props.health());
        GravityControllerProps.with(props);
        GravityControllerProps.gravity(Props.gravityAmount());
        
        cpShape* shape = cpBoxShapeNew(NULL, Props.size().x, Props.size().y);
        terrain->set(shape, Props.flagBoss() ? TerrainPlayerBoss : TerrainPlayerBase, Props.team());
        
        Props.collider(collide2D->addShape(shape));
        collide2D->update(Props.collider(), position, 0.f);

        if (!Props.flagBoss())
        {
            float halfHeight = Props.size().y * 0.5f;
            float footHeight = 4.f;
            shape = cpBoxShapeNew(NULL, Props.size().x, footHeight);
            terrain->set(shape, TerrainBase, Props.team());
            
            Props.footCollider(collide2D->addShape(shape));
            
            Props.footOffset(glm::vec2(0.f, halfHeight - (footHeight * 0.5f) + 1.f));
            collide2D->update(Props.footCollider(), glm::vec2(-10000.f), 0.f);
        }
        
        // temp visuals for now
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(Props.size());
        glm::vec3 color;
        switch (playerId)
        {
            case 1: color = glm::makeColor(217,91,67); break;
            case 2: color = glm::makeColor(192,41,66); break;
            case 3: color = glm::makeColor(84,36,55); break;
            case 4: color = glm::makeColor(83,119,122); break;
        }
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        // setup cooldowns
        PropMods::cooldownInit(props, Props.strflagDash());
        PropMods::cooldownInit(props, Props.strflagAttack1());
        PropMods::cooldownInit(props, Props.strflagAttack2());
        
        // set gun
        Props.mount(glm::vec2(Props.size().x * 0.5f + 8.f, Props.flagBoss() ? 0.f : (Props.size().y * -0.5f + 8.f)));
        
        // create entity
        size_t entity = entities->add(settings->get().evtInit(), props);
        collision->setEntity(Props.collider(), entity);

        // add weapons
        switch (Props.playerClass())
        {
            case PlayerClassBlerkgt:
                guns->addGun(entity, true, Props.team(), 0, GunMachine, Props.mount(), color);
                guns->addGun(entity, false, Props.team(), 1, GunGrenade, Props.mount(), color);
                break;
                
            case PlayerClassFiftyNine:
                guns->addGun(entity, true, Props.team(), 0, GunMachine, Props.mount(), color);
                guns->addGun(entity, true, Props.team(), 1, GunSpiderMine, Props.mount(), color);
                break;
                
            case PlayerClassNightless:
                guns->addGun(entity, true, Props.team(), 0, GunMachine, Props.mount(), color);
                guns->addGun(entity, false, Props.team(), 1, GunRocket, Props.mount(), color);
                break;
                
            case PlayerBossClassFiftyNine:
            case PlayerBossClassBlerkgt:
            case PlayerBossClassNightless:
                guns->addGun(entity, true, Props.team(), 0, GunRocketHoming, Props.mount(), color);
                melee->addMelee(entity, Props.team(), 1, MeleeBossChop, Props.mount(), color);
                break;
                
            case PlayerMonsterClassBlerkgt:
            case PlayerMonsterClassFiftyNine:
            case PlayerMonsterClassNightless:
                melee->addMelee(entity, Props.team(), 1, MeleeClub, Props.mount(), color);
                break;
        }
    }
    
}

