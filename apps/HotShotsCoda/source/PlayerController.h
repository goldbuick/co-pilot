
#pragma once

#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    enum PlayerMisc
    {
        PlayerMatchStart = 1,
        PlayerTotalMasks = 5
    };
    
    enum PlayerControllers
    {
        PlayerInputXbox360,
        PlayerInputPS4,
        PlayerInputMax
    };
    
    enum PlayerClasses
    {
        PlayerClassNone = 0,
        
        PlayerClassBlerkgt,
        PlayerClassFiftyNine,
        PlayerClassNightless,
        PlayerClassMax,
        
        PlayerMonsterClassBlerkgt,
        PlayerMonsterClassFiftyNine,
        PlayerMonsterClassNightless,
        PlayerMonsterClassMax,
        
        PlayerBossClassBlerkgt,
        PlayerBossClassFiftyNine,
        PlayerBossClassNightless,
        PlayerBossClassMax
    };
    
    class PlayerController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(State,
            PropertyAccessor(go, bool)
            PropertyAccessor(countdown, float)
            PropertyAccessor(state, bool))
        
        PropertyInterfaceEntity(Props,
            PropertyEvent(PlayerProps)
            PropertyEvent(PlayerLadderGrab)
            // class & stats
            PropertyAccessor(player, int)
            PropertyAccessor(team, int)
            PropertyAccessor(playerClass, int)
            PropertyAccessor(health, float)
            PropertyAccessor(maxHealth, int)
            PropertyAccessor(masks, int)
            PropertyAccessor(masksOffset, float)
                                
            // weapon mount
            PropertyAccessor(mount, glm::vec2)
                                
            // primary attack
            PropertyAccessor(flagAttack1, float)
            PropertyAccessor(flagAttack1Ready, float)

            PropertyAccessor(readyAttack1, float)
            PropertyAccessor(cooldownAttack1, float)
            PropertyAccessor(kickbackAttack1, float)
            PropertyAccessor(directionAttack1, glm::vec2)
                  
            // secondary attack
            PropertyAccessor(flagAttack2, float)
            PropertyAccessor(flagAttack2Ready, float)
                                
            PropertyAccessor(readyAttack2, float)
            PropertyAccessor(cooldownAttack2, float)
            PropertyAccessor(kickbackAttack2, float)
            PropertyAccessor(directionAttack2, glm::vec2)
                                
            // dash
            PropertyAccessor(flagDash, float)
            PropertyAccessor(flagDashPressed, bool)
            PropertyAccessor(flagDashing, float)
            PropertyAccessor(cooldownDash, float)
            PropertyAccessor(dashVelocity, float)
            PropertyAccessor(dashDuration, float)
                                
            // input & state
            PropertyAccessor(flagBoss, bool)
            PropertyAccessor(flagMonster, bool)
            PropertyAccessor(flagJump, float)
            PropertyAccessor(flagAim, glm::vec2)
            PropertyAccessor(flagLockedAim, glm::vec2)
            PropertyAccessor(flagMove, glm::vec2)
            PropertyAccessor(flagFacing, float)
            PropertyAccessor(flagGrounded, bool)
            PropertyAccessor(flagClimbing, bool)
            PropertyAccessor(flagGrabDelay, float)
                                
            // jumpin
            PropertyAccessor(jumpAmount, float)
            PropertyAccessor(gravityAmount, glm::vec2)
            
            // collider
            PropertyAccessor(size, glm::vec2)
            PropertyAccessor(position, glm::vec2)
            PropertyAccessor(velocity, glm::vec2)
            PropertyAccessor(inertia, glm::vec2)
            PropertyAccessor(collider, size_t)
            PropertyAccessor(footOffset, glm::vec2)
            PropertyAccessor(footCollider, size_t))
        
    public:
        PlayerController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selState);
        static EventedSelector(selPlayer);
        static EventedSelector(selPlayers);
        static EventedSelector(selValidPlayers);
        static EventedSelector(selPlayerByHandle);
        
        // operations
        static EventedOperation(opUpdateState);
        static EventedOperation(opUpdatePlayer);
        static EventedOperation(opHitPlayer);
        static EventedOperation(opCollectedPlayer);
        
        // render
        static GLRenderPipeLayer(renderPlayers);
        static GLRenderPipeLayer(renderState);

        // api
        size_t addLayer();
        size_t addGuiLayer();
        size_t addStateLayer();
        void addInput(int playerId, size_t& move, size_t& aim, size_t& jump, size_t& dash, size_t& attack1, size_t& attack2);
        
        int bossForm(int playerClass);
        int playerForm(int playerClass);
        int monsterForm(int playerClass);
        bool isBossForm(int playerClass);
        bool isPlayerForm(int playerClass);
        bool isMonsterForm(int playerClass);
        std::string getClassName(int playerClass);
        void addPlayerInput(int inputType, int playerId);
        void addPlayer(int playerClass, int playerId, glm::vec2 position);
        
    };
    
}