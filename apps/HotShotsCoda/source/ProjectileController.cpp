
#include "HotShotsCoda.h"
#include "GunsController.h"
#include "AttackController.h"
#include "TerrainController.h"
#include "CollisionController.h"
#include "ProjectileController.h"
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

using namespace copilot;

namespace coda
{
    ProjectileController::ProjectileController(copilot::CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        entities->addTimer(settings->get().FPS(), ProjectileController::selProjectiles, ProjectileController::opUpdateProjectiles);
    }
    
    // selectors
    EventedSelector(ProjectileController::selProjectiles)
    {
        Props(ProjectileController);
        
        return collection->selectByProp(Props.strprojectile());
    }
    
    // operations
    EventedOperation(ProjectileController::opUpdateProjectiles)
    {
        opDelta;
        OtherProps(GLDebugLayer);
        Props(ProjectileController);
        OtherProps(AttackController);

        Collide2D* collide2D = manager->request<Collide2D>();
        GunsController* guns = manager->request<GunsController>();
        CollisionController* collision = manager->request<CollisionController>();
        
        Props.with(target);
        glm::vec2 collide, position = Props.position();
        glm::vec2 dist, step = Props.velocity() * delta;
        
        while (!target->getDead() && (step.x != 0.f || step.y != 0.f))
        {
            std::list<Collision2D> collisions;
            if (Props.terrainBounce())
            {
                collisions = collide2D->platformerCollide(Props.projectile(), position, 0.f, step, dist, collide);
            }
            else
            {
                collisions = collide2D->sweptCollide(Props.projectile(), position, 0.f, step, dist);
            }
            position += dist;
            
            if (collisions.size())
            {
                ReadEventedEntity entity = collection->get(collision->getEntity(collisions.front().shape));
                
                // generate impact event upon other team
                if (entity.valid() && AttackControllerProps.with(entity)->team() != Props.team() && Props.playerDamage())
                {
                    PropertyCollection props;
                    
                    AttackControllerProps.with(props);
                    AttackControllerProps.team(Props.team());
                    AttackControllerProps.position(position);
                    AttackControllerProps.handle(entity.getHandle());
                    AttackControllerProps.damage(Props.playerDamage());
                    glm::vec2 impact;
                    if (Props.popPop().x != 0.f || Props.popPop().y != 0.f)
                    {
                        float length = glm::length(Props.velocity());
                        impact = Props.popPop() * length;
                    }
                    else
                    {
                        impact = Props.velocity();
                    }
                    AttackControllerProps.impact(impact * Props.playerImpact());
                    
                    collection->trigger(AttackControllerProps.evtImpact(), props);
                    target->setDead(true);
                }
                else if (Props.terrainBounce())
                {
                    if (collide.x == 0.f)
                    {
                        Props.velocity(Props.velocity() * glm::vec2(1.f, -Props.terrainBounce()));
                    }
                    else
                    {
                        Props.velocity(Props.velocity() * glm::vec2(-1.f, Props.terrainBounce()));
                    }
                }
                else
                {
                    target->setDead(true);
                }
            }
        }
        
        Props.position(position);
        
        float angle = glm::atan(Props.velocity().y, Props.velocity().x);
        GLDebugLayerProps.with(target)->glAngle(angle);
        
        if (Props.homing())
        {
            std::list<Collision2D> collisions = collide2D->collide(collision->getCircle(512), position, 0.f, glm::vec2(0.f, 1.f));
            for (std::list<Collision2D>::iterator i=collisions.begin(); i!=collisions.end(); ++i)
            {
                ReadEventedEntity entity = collection->get(collision->getEntity(i->shape));
                if (entity.valid() && AttackControllerProps.with(entity)->team() != Props.team())
                {
                    glm::vec2 direction = glm::normalize(Props.velocity());
                    glm::vec2 toward = glm::normalize(AttackControllerProps.position() - position);
                    glm::vec2 diff = (toward - direction) * delta * Props.homing();
                    direction = glm::normalize(direction + diff);
                    Props.velocity(direction * glm::length(Props.velocity()));
                    break;
                }
            }
            Props.with(target);
        }
        
        if (Props.prox())
        {
            bool seeking = false;
            std::list<Collision2D> collisions = collide2D->collide(collision->getCircle(128), position, 0.f, glm::vec2(0.f, 1.f));
            for (std::list<Collision2D>::iterator i=collisions.begin(); i!=collisions.end(); ++i)
            {
                ReadEventedEntity entity = collection->get(collision->getEntity(i->shape));
                if (entity.valid() && AttackControllerProps.with(entity)->team() != Props.team())
                {
                    seeking = true;
                    glm::vec2 delta = AttackControllerProps.position() - Props.position();
                    float length = glm::length(delta);
                    delta = glm::normalize(delta);
                    Props.velocity(delta * (210.f - length) * 3.f);
                    break;
                }
            }
            Props.with(target);
            if (!seeking)
            {
                Props.velocity(glm::vec2(0.f));
            }
        }
        
        if (Props.slowing()) Props.velocity(Props.velocity() - (Props.velocity() * Props.slowing() * delta));
        
        if (Props.chainType() && Props.chainTimer(Props.chainTimer() - delta) < 0.f)
        {
            Props.with(target);
            Props.chainTimer(Props.chainTimeout());
            float chainAngle = (angle + glm::pi<float>()) + glm::linearRand(-Props.chainRange(), Props.chainRange());
            glm::vec2 direction = glm::vec2(glm::cos(chainAngle), glm::sin(chainAngle));
            guns->shootGun(Props.primary(), Props.team(), Props.chainType(), Props.position(), direction, Props.color());
            Props.with(target);
        }
        
        Props.decay(Props.decay() - delta);
        if (Props.decay() <= 0.f) target->setDead(true);
        
        if (target->getDead())
        {
            if (Props.blastDamage())
            {
                
            }
            if (Props.gunType())
            {
                float step = (glm::pi<float>() * 2.f) / (float)Props.gunCount();
                for (int i=0; i<Props.gunCount(); ++i)
                {
                    glm::vec2 direction = glm::vec2(glm::cos(angle), glm::sin(angle));
                    guns->shootGun(Props.primary(), Props.team(), Props.gunType(), Props.position(), direction, Props.color());
                    Props.with(target);
                    angle += step;
                }
            }
            collide2D->removeShape(Props.projectile());
        }
    }
    
    // api
    size_t ProjectileController::addLayer()
    {
        return GLDebugLayer::addLayer(manager, ProjectileController::selProjectiles);
    }
    
    void ProjectileController::addProjectile(copilot::PropertyCollection props, cpLayers layers)
    {
        Props(ProjectileController);
        
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        TerrainController* terrain = manager->request<TerrainController>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        Props.with(props);
        float angle = glm::atan(Props.velocity().y, Props.velocity().x);
        
        // generate collider shape
        cpShape* shape = cpBoxShapeNew(NULL, Props.size().x, Props.size().y);
        terrain->set(shape, layers, Props.team());
        
        Props.projectile(collide2D->addShape(shape));
        collide2D->update(Props.projectile(), glm::vec2(-100000.f), angle);
        
        entities->add(settings->get().evtInit(), props);
    }
    
}


