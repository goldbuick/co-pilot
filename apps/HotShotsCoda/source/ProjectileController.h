
#pragma once

#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    class ProjectileController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            // collision
            PropertyAccessor(primary, bool)
            PropertyAccessor(projectile, size_t)
            PropertyAccessor(team, int)
            PropertyAccessor(size, glm::vec2)

            // update
            PropertyAccessor(color, glm::vec3)
            PropertyAccessor(position, glm::vec2)
            PropertyAccessor(velocity, glm::vec2)
            PropertyAccessor(gravity, glm::vec2)
            PropertyAccessor(homing, float)
            PropertyAccessor(prox, bool)
            PropertyAccessor(slowing, float)
            PropertyAccessor(chainType, int)
            PropertyAccessor(chainTimer, float)
            PropertyAccessor(chainRange, float)
            PropertyAccessor(chainTimeout, float)
                                
            // interactions
                                
            // timeout
            PropertyAccessor(decay, float)
                                
            // player collide
            PropertyAccessor(playerDamage, int)
            PropertyAccessor(playerImpact, float)
        
            // terrain collide
            PropertyAccessor(terrainBounce, float)
        
            // post effects
                                
            // alter impact direction
            PropertyAccessor(popPop, glm::vec2)
        
            // blast
            PropertyAccessor(blastDamage, float)
            PropertyAccessor(blastRadius, float)
        
            // secondary shot ( uses GunTypes )
            PropertyAccessor(gunType, int)
            PropertyAccessor(gunCount, int)
        )
        
    public:
        ProjectileController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selProjectiles);
        
        // operations
        static EventedOperation(opUpdateProjectiles);
        
        // api
        size_t addLayer();
        void addProjectile(copilot::PropertyCollection props, cpLayers layers);
        
    };
    
}