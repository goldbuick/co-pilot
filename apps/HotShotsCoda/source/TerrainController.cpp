
#include "HotShotsCoda.h"
#include "TerrainController.h"
#include "CollisionController.h"
#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>

#include <algorithm>

using namespace copilot;

namespace coda
{
    
    TerrainController::TerrainController(copilot::CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Props(TerrainController);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        entities->addTimer(settings->get().FPS(), TerrainController::selDeco, TerrainController::opUpdateDeco);
        entities->addEvent(Props.evtClearSpawn(), TerrainController::selSpawn, TerrainController::opClearSpawn);
    }
    
    // selectors
    EventedSelector(TerrainController::selDeco)
    {
        Props(TerrainController);
        
        return collection->selectByProp(Props.strdeco());
    }
    EventedSelector(TerrainController::selTerrain)
    {
        Props(TerrainController);
        
        return collection->selectByProp(Props.strterrain());
    }
    EventedSelector(TerrainController::selBackground)
    {
        Props(TerrainController);
        
        int background = Props.with(meta)->background();
        ForEachEventedSelectorByProp(background)
            if (Props.with(*i)->background() == background) result.push_back(*i);
        return result;
    }
    EventedSelector(TerrainController::selSpawn)
    {
        Props(TerrainController);
        
        return collection->selectByProp(Props.strspawn());
    }

    // operations
    EventedOperation(TerrainController::opUpdateDeco)
    {
        opDelta;
        Props(TerrainController);
        
        Props.with(target);
        
        float offset = Props.offset();
        glm::vec2 anchor = Props.anchor();
        glm::vec2 ratio = glm::vec2(glm::cos(offset + anchor.x * 0.1f), glm::sin(offset + anchor.y));
        glm::vec2 position = glm::vec2(anchor.x + ratio.x * 8.f, anchor.y + ratio.y * 64.f);
        
        Props.position(position);
        Props.offset(offset + (delta * (0.5f + glm::abs(glm::cos(anchor.x)))) * 0.04f);
    }
    EventedOperation(TerrainController::opClearSpawn)
    {
        Props(TerrainController);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        
        Props.with(target);
        target->setDead(true);
        collide2D->removeShape(Props.blockCollider());
    }
    
    
    // api
    size_t TerrainController::addDecoLayer(glm::vec2 parallax)
    {
        OtherNamedProps(GLDebugLayer, Meta);
        
        PropertyCollection meta;
        GLDebugLayerMeta.with(meta);
        GLDebugLayerMeta.parallax(parallax);
        return GLDebugLayer::addLayer(manager, meta, TerrainController::selDeco);
    }
    size_t TerrainController::addBlockLayer()
    {
        return GLDebugLayer::addLayer(manager, TerrainController::selTerrain);
    }
    float TerrainController::layerScale(int layer)
    {
        return 1.f / glm::log2((float)layer+1);
    }
    size_t TerrainController::addBackgroundLayer(int layer)
    {
        Props(TerrainController);
        OtherNamedProps(GLDebugLayer, Meta);
        
        PropertyCollection meta;
        Props.with(meta);
        Props.background(layer);
        GLDebugLayerMeta.with(meta);
        GLDebugLayerMeta.parallax(glm::vec2(layerScale(layer)));
        return GLDebugLayer::addLayer(manager, meta, TerrainController::selBackground);
    }
    
    // setup group & layers
    cpShape* TerrainController::set(cpShape* shape, cpLayers layers, cpGroup group)
    {
        cpShapeSetGroup(shape, group);
        cpShapeSetLayers(shape, layers);
        return shape;
    }
    
    void TerrainController::addDeco(glm::vec2 position, glm::vec2 size, glm::vec3 color)
    {
        OtherProps(GLDebugLayer);
        Props(TerrainController);

        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        Props.with(props);
        Props.deco(true);
        Props.offset(0.f);
        Props.anchor(position);
        Props.position(position);
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(size);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        entities->add(settings->get().evtInit(), props);
    }
    
    void TerrainController::addBackground(int layer, glm::vec2 position, glm::vec2 size, glm::vec3 color)
    {
        OtherProps(GLDebugLayer);
        Props(TerrainController);
        
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        Props.with(props);
        Props.background(layer);
        Props.position(position);
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(size);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        entities->add(settings->get().evtInit(), props);
    }
    
    void TerrainController::addBlock(glm::vec2 position, glm::vec2 size, glm::vec3 color, bool oneSided)
    {
        OtherProps(GLDebugLayer);
        Props(TerrainController);
        
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        PropertyCollection props;
        Props.with(props);
        Props.terrain(true);
        Props.position(position);
        Props.velocity(glm::vec2(0.f));
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(size);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        cpShape* shape = cpBoxShapeNew(NULL, size.x, size.y);
        set(shape, TerrainBase);
        
        size_t collider = collide2D->addShape(shape);
        collide2D->update(collider, position, 0.f);
        Props.blockCollider(collider);
        
        entities->add(settings->get().evtInit(), props);
        
        if (oneSided)
        {
            CollisionFlags flags;
            flags.oneSided.enabled = true;
            flags.oneSided.normal = glm::vec2(0.f, 1.f);
            collide2D->setFlags(collider, flags);
        }
    }
    
    void TerrainController::addLadder(glm::vec2 position, glm::vec2 size, glm::vec3 color)
    {
        OtherProps(GLDebugLayer);
        Props(TerrainController);
        
        Settings* settings = manager->request<Settings>();
        Collide2D* collide2D = manager->request<Collide2D>();
        EventedCollection* entities = manager->request<EventedCollection>();
        
        position += size * 0.5f;
        
        PropertyCollection props;
        Props.with(props);
        Props.terrain(true);
        Props.position(position);
        Props.velocity(glm::vec2(0.f));
        GLDebugLayerProps.with(props)->defaults();
        GLDebugLayerProps.glSize(size);
        GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
        GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, color));
        
        cpShape* shape = cpBoxShapeNew(NULL, size.x, size.y);
        set(shape, TerrainLadder);

        size_t collider = collide2D->addShape(shape);
        collide2D->update(collider, position, 0.f);
        Props.blockCollider(collider);
        
        entities->add(settings->get().evtInit(), props);
    }
    
    // do we overlap a ladder ?
    bool TerrainController::collideWithLadder(size_t collider, glm::vec2 position, float angle, glm::vec2 direction, glm::vec2& center)
    {
        Collide2D* collide2D = manager->request<Collide2D>();
        
        cpShape* shape = collide2D->getShape(collider);
        if (!shape) return false;

        // cache layers
        cpLayers cacheLayers = cpShapeGetLayers(shape);
        cpShapeSetLayers(shape, TerrainLadder);
        
        std::list<Collision2D> collisions = collide2D->collide(collider, position, angle, direction);
        
        // reset layers
        cpShapeSetLayers(shape, cacheLayers);
        
        bool collide = collisions.size();
        if (collide)
        {
            cpShape* shape = collide2D->getShape(collisions.front().shape);
            cpBody* body = cpShapeGetBody(shape);
            center.x = cpBodyGetPos(body).x;
            center.y = cpBodyGetPos(body).y;
        }
        
        return collide;
    }
    
    // spawn locations
    void TerrainController::clearSpawns()
    {
        Props(TerrainController);

        EventedCollection* entities = manager->request<EventedCollection>();
        entities->trigger(Props.evtClearSpawn());
    }
    
    // locations
    void TerrainController::shuffleLocations()
    {
        for (std::map<int, std::vector<glm::vec2> >::iterator i=locations.begin(); i!=locations.end(); ++i)
            std::random_shuffle(i->second.begin(), i->second.end());
    }
    void TerrainController::addLocation(int type, glm::vec2 position)
    {
        OtherProps(GLDebugLayer);
        Props(TerrainController);
        
        if (type == TerrainSpawnLocation)
        {
            Settings* settings = manager->request<Settings>();
            Collide2D* collide2D = manager->request<Collide2D>();
            EventedCollection* entities = manager->request<EventedCollection>();
            
            glm::vec2 size(32.f, 12.f);
            PropertyCollection props;
            Props.with(props);
            Props.spawn(true);
            Props.terrain(true);
            Props.position(position + glm::vec2(0.f, 16.f));
            Props.velocity(glm::vec2(0.f));
            GLDebugLayerProps.with(props)->defaults();
            GLDebugLayerProps.glSize(size);
            GLDebugLayerProps.glShader(GLDebugLayer::shader(manager));
            GLDebugLayerProps.glTexture(GLDebugLayer::texture(manager, glm::makeColor(0.f, 0.f, 0.f)));
            
            cpShape* shape = cpBoxShapeNew(NULL, size.x, size.y);
            set(shape, TerrainBase);
            
            size_t collider = collide2D->addShape(shape);
            collide2D->update(collider, position, 0.f);
            Props.blockCollider(collider);
            
            entities->add(settings->get().evtInit(), props);
        }
        
        locations[type].push_back(position);
    }
    glm::vec2 TerrainController::getLocation(int type)
    {
        locationIndexes[type] = (locationIndexes[type] + 1) % locations[type].size();
        return locations[type][locationIndexes[type]];
    }
    size_t TerrainController::locationTotal(int type)
    {
        return locations[type].size();
    }

}
