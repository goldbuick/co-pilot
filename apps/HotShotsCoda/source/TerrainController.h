
#pragma once

#include <co+pilot/bundles/physics/Collide2D.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    enum TerrainLayers
    {
        TerrainBase = 1 << 0,
        TerrainPlayer = 1 << 1,
        TerrainLadder = 1 << 2,
        TerrainOnLadder = 1 << 3,
        TerrainPlayerBoss = 1 << 4,
        TerrainPlayerBase = (TerrainPlayer | TerrainBase),
        TerrainPlayerLadder = (TerrainPlayer | TerrainOnLadder),
        TerrainProjectilePrimary = (TerrainPlayer | TerrainPlayerBoss),
        TerrainProjectileSecondary = (TerrainPlayer | TerrainPlayerBoss | TerrainBase)
    };
    
    enum TerrainTypes
    {
        TerrainDeco = -1,
        TerrainBlock = 0,
        TerrainBackground1,
        TerrainBackground2,
        TerrainBackground3,
        TerrainBackground4,
        TerrainBackground5,
        TerrainBackground6,
        TerrainBackground7,
        TerrainBackground8,
        TerrainBackgroundMax,
    };
    
    enum TerrainLocations
    {
        TerrainSpawnLocation,
        TerrainMaskLocation,
        TerrainLowTierLocation,
        TerrainLocationMax
    };
    
    class TerrainController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyEvent(ClearSpawn)
            PropertyAccessor(deco, bool)
            PropertyAccessor(terrain, bool)
            PropertyAccessor(spawn, bool)
            PropertyAccessor(background, int)
            PropertyAccessor(offset, float)
            PropertyAccessor(blockCollider, size_t)
            PropertyAccessor(anchor, glm::vec2)
            PropertyAccessor(velocity, glm::vec2)
            PropertyAccessor(position, glm::vec2))
    public:
        TerrainController(copilot::CoreLogicManager* manager, const char* name);
        
        // selectors
        static EventedSelector(selDeco);
        static EventedSelector(selTerrain);
        static EventedSelector(selBackground);
        static EventedSelector(selSpawn);
        
        // operations
        static EventedOperation(opUpdateDeco);
        static EventedOperation(opClearSpawn);
        
        // api
        size_t addDecoLayer(glm::vec2 parallax);
        size_t addBlockLayer();
        float layerScale(int layer);
        size_t addBackgroundLayer(int layer);
        
        // setup group & layers
        cpShape* set(cpShape* shape, cpLayers layers, cpGroup group = CP_NO_GROUP);

        void addDeco(glm::vec2 position, glm::vec2 size, glm::vec3 color);
        void addBackground(int layer, glm::vec2 position, glm::vec2 size, glm::vec3 color);
        void addBlock(glm::vec2 position, glm::vec2 size, glm::vec3 color, bool oneSided);
        void addLadder(glm::vec2 position, glm::vec2 size, glm::vec3 color);
        
        // do we overlap a ladder ?
        bool collideWithLadder(size_t collider, glm::vec2 position, float angle, glm::vec2 direction, glm::vec2& center);
        
        // spawn locations
        void clearSpawns();
        
        // locations
        void shuffleLocations();
        void addLocation(int type, glm::vec2 position);
        glm::vec2 getLocation(int type);
        size_t locationTotal(int type);
        
    private:
        std::map<int, int> locationIndexes;
        std::map<int, std::vector<glm::vec2> > locations;
    };
    
}