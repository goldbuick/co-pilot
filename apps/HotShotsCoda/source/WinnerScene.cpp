
#include "WinnerScene.h"
#include "HotShotsCoda.h"
#include "AudioController.h"
#include "TerrainController.h"

#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/render/GLFont.h>
#include <co+pilot/bundles/files/AssetManager.h>
#include <co+pilot/core/common/glm/gtc/random.hpp>
#include <co+pilot/bundles/files/assetTypes/SpriterScml.h>

using namespace copilot;

namespace coda
{
    WinnerController::WinnerController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        Props(WinnerController);
        
        manager->request<AudioController>();
        Settings* settings = manager->request<Settings>();
        EventedCollection* entities = manager->request<EventedCollection>();
        AssetManager* assetManager = manager->request<AssetManager>();
        
        entities->addTimer(settings->get().FPS(), WinnerController::selWinner, WinnerController::opUpdate);
        
        PropertyCollection props;
        Props.with(props);
        Props.timer(5.f);
        
        entities->add(settings->get().evtInit(), props);
        
        assetManager->request<SpriterScmlRef>("coda/anim/arm-adillo");
    }

    // selectors
    EventedSelector(WinnerController::selWinner)
    {
        Props(WinnerController);
        
        return collection->selectByProp(Props.strtimer());
    }
    
    // operations
    EventedOperation(WinnerController::opUpdate)
    {
        opDelta;
        Props(WinnerController);
        
        Props.with(target);
        if (Props.timer(Props.timer() - delta) < 0.f)
        {
            Scene::setResult(manager, SceneCharSelect);
        }
    }
    
    // api
    size_t WinnerController::addGfxLayer()
    {
        AssetManager* assetManager = manager->request<AssetManager>();
        assetManager->request<GLShaderRef>("default,default");
        assetManager->request<GLFontTextureRef>("default,24");
        return manager->request<GLRenderPipe>()->addLayer(WinnerController::selWinner, WinnerController::render);
    }

    // render
    GLRenderPipeLayer(WinnerController::render)
    {
        Props(WinnerController);
        
        Settings* settings = manager->request<Settings>();
        AssetManager* assetManager = manager->request<AssetManager>();
        GLShaderRef* shader = assetManager->request<GLShaderRef>("default,default");
        
        GLFont* font = manager->request<GLFont>();
        GLFontTextureRef* fontTex = assetManager->request<GLFontTextureRef>("default,24");
        
        glm::vec2 size;
        glm::vec2 position;
        std::string strMessage;
        glm::mat4 projection = render->unitProjectionOrtho(units);
        glm::mat4 transform = render->camera2DTransform(units, camera, glm::vec2(1.f));
        
        strMessage = strPrint("Player %i is Winner", settings->get().Winner());
        
        GLRenderQuadPool* quadPool = batch->getQuadPool();
        size = font->getTextSize(fontTex->ref, strMessage.c_str());
        font->drawText(quadPool, fontTex->ref, position.x - size.x * 0.5f, position.y - size.y * 0.5f, strMessage.c_str());
        
        batch->render(*shader->ref, projection, transform, &fontTex->ref->texture, 1, quadPool);
        
        //Props.with(selection.front());
        //GLSpine2D* spine = manager->request<GLSpine2D>();
        //spine->drawAnim(*shader->ref, projection, transform, quadPool, quadIndices, spine->getAnim(Props.anim()));
        
    }
    
    void WinnerScene::init()
    {
        manager->request<AudioController>();
        LibSDL* sdl = manager->request<LibSDL>();
        GLRenderPipe* render = manager->request<GLRenderPipe>();;
        Settings* settings = manager->request<Settings>();
        WinnerController* winner = manager->request<WinnerController>();
        TerrainController* terrain = manager->request<TerrainController>();
        
        // stop music
        settings->playMusic(NULL);
        
        // get window
        size_t windowHandle = sdl->getWindowHandle(0);
        
        // add camera
        size_t cameraHandle = render->addCamera2D(0);
        render->addCamera2DToWindow(windowHandle, cameraHandle);
        
        GLRenderScreenUnits unit;
        unit.autosize = false;
        unit.target = settings->get().Unit();
        render->setUnitForWindow(windowHandle, unit);
        
        // add layers
        render->addLayerToCamera2D(GLClearLayer::addLayer(manager, glm::makeColor(5,205,255)), cameraHandle);
        render->addLayerToCamera2D(terrain->addDecoLayer(glm::vec2(1.f)), cameraHandle);
        render->addLayerToCamera2D(winner->addGfxLayer(), cameraHandle);
        
        // add deco
        glm::vec3 decoColor = glm::makeColor(164,145,255);
        glm::vec3 decoColor2 = glm::makeColor(243,166,255);
        for (int i=0; i<64; ++i)
            terrain->addDeco(
                glm::linearRand(unit.target * -0.5f, unit.target * 0.5f),
                glm::linearRand(glm::vec2(64.f), glm::vec2(256.f)),
                glm::linearRand(decoColor, decoColor2));
        
    }
    
}