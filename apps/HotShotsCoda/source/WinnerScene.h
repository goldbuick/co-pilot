
#pragma once

#include <co+pilot/bundles/common/Scene.h>
#include <co+pilot/bundles/render/GLRenderPipe.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
    class WinnerController : public copilot::CoreLogic
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyAccessor(pollId, size_t)
            PropertyAccessor(anim, size_t)
            PropertyAccessor(timer, float))
        
    public:
        WinnerController(copilot::CoreLogicManager* manager, const char* name);

        // selectors
        static EventedSelector(selWinner);
        
        // operations
        static EventedOperation(opUpdate);
        
        // api
        size_t addGfxLayer();
        
        // render
        static GLRenderPipeLayer(render);
    };
    
    class WinnerScene : public copilot::Scene
    {
    public:
        WinnerScene(copilot::CoreLogicManager* manager) : copilot::Scene(manager) { }
        void init();
      
    };
    
}
