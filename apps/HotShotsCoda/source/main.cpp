
#include "HotShotsCoda.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
//#include <co+pilot/bundles/audio/Plaid.h>
#include <co+pilot/bundles/files/AssetManager.h>

using namespace copilot;

int main(int argc, char * argv[])
{
    CoreLogicManager manager;
    
    // config pub dir
    AssetManager* assetManager = manager.request<AssetManager>();
    assetManager->config(argc, argv);
    
    // game globals
    coda::Settings* settings = manager.request<coda::Settings>();
    
    // add window
    LibSDL* sdl = manager.request<LibSDL>();
    sdl->addWindow("HotShotsCoda", settings->get().Unit() * 1.5f);
    
    // startup audio
//    PlaidAudio* plaid = manager.request<PlaidAudio>();
    /*/
    plaid->getAudio()->volume(0.8f);
    /*/
    //plaid->getAudio()->volume(0.2f);
    //*/
    
    // state machine
    coda::HotShotsCoda* game = manager.request<coda::HotShotsCoda>();
    game->run(coda::SceneFight, settings->get().FPS());
    
    return 0;
}

