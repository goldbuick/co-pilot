
#include "Boilerplate.h"
#include "BoilerplateScene.h"

using namespace copilot;

namespace Boilerplate
{
        
    // get next scene
    Scene* Boilerplate::next(uint8_t result)
    {
        return new BoilerplateScene(manager);
    }
}