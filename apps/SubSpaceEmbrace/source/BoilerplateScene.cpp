
#include "BoilerplateScene.h"
#include "BoxController.h"

using namespace copilot;

namespace Boilerplate
{
    
    void BoilerplateScene::init()
    {
        BoxController* boxes = manager->request<BoxController>();
        
        boxes->addBox(true, glm::vec2(-100.f));
        boxes->addBox(false, glm::vec2(100.f));
        boxes->addBox(false, glm::vec2(-200.f, -100.f));
        boxes->addBox(false, glm::vec2(200.f, 100.f));
    }
    
}