
#include "BoxController.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/sdl/InputMap.h>
#include <co+pilot/bundles/render/GLWindow.h>
#include <co+pilot/bundles/physics/Collide2D.h>

using namespace copilot;

namespace Boilerplate
{
    
    BoxController::BoxController(CoreLogicManager* manager, const char* name)
    : CoreLogic(manager, name)
    {
        // SURFACE
        LibSDL* sdl = manager->request<LibSDL>();
        // RENDERING PIPELINE
        GLWindow* window = manager->request<GLWindow>();
        // DOM AND SLOT/SIGNALs
        EventedCollection* entities = manager->request<EventedCollection>();

        // 1. Get the handle to the first available window in the SDL Surface thingy
        // NOTE: RIGHT NOW THIS IS THE FIRST WINDOW THAT WAS CREATED IN main.cpp
        //       IF YOU CREATE TWO WINDOWS IN main.cpp THE SECOND WILL BE INDEX 1
        size_t windowHandle = sdl->getWindowHandle(0);
        
        // 1a. Add a camera to the first available window (both index 0)
        size_t cameraHandle = window->addCamera2D(0);
        // 1b. Associate a camera to a window.  The camera renders to the whole window.
        window->addCamera2DToWindow(windowHandle, cameraHandle);
        
        // 1c. This sets the world units.  In HotShots these are a property, here they are defined explicitly.
        //     These units have a fixed height, but the width is going to almost certainly change based on the
        //     size of the sdl window.
        //      DAVID LIED
        //     0,0 in a 640x480 viewport is the center, so it goes from -320 to 320 (left to right) and from
        //     -240 to 240 (top to bottom) because DAVID HATES MATH.
        //     There is also a center and scale for the camera, and its aspect ratio comes from the viewport
        //     it is bound to.  However, scale has individual values for X and Y, which means ultimately the
        //     aspect ratio is a function of the scale.
        //
        GLWindowScreenUnits unit;
        // If autosize is true, the world units and the pixel units are the exact same.
        unit.autosize = false;
        // If autisize is false, this is what it uses for desired world units.
        unit.target = glm::vec2(640.f, 480.f);
        // This applies the previous unit definition we made in 1.c to the window.
        window->setUnitForWindow(windowHandle, unit);
        
        // 1.d Now that we have a camera and a window, a layer is a render operation.  This operation clears the window
        //     with the color.
        window->addLayerToCamera2D(GLClearLayer::addLayer(manager, window, glm::makeColor(0,0,0)), cameraHandle);
        
        // 2. This creates a layer to draw the boxes
        //    The selector portion (BoxController::selBoxes) selects all the objects to be rendered
        //    Then the GLDebugLayer::render operation is applied to them
        size_t layer = window->addLayer(PropertyCollection(manager), BoxController::selBoxes, GLDebugLayer::render);
        // 2.a This adds the layer to the camera.
        window->addLayerToCamera2D(layer, cameraHandle);
        
        // 3. This associates events
        //    The selector portion (BoxController::selBox) is declaring what entities are affected by the event
        //    The function, EventedCollection::opCopyAllFromMeta, is applied to the entities (and this eventuatlly
        //    gets input events to the entities)
        entities->addEvent(Props.evtBoxProps(), BoxController::selBox, EventedCollection::opCopyAllFromMeta);
        // 3.a This timer is how often the entities are update ... which is 60 times per second, aligned with frames.
        //     Timers are really cheap.
        entities->addTimer(60.f, BoxController::selBoxes, BoxController::opUpdateBoxes);
    }
    
    // interfaces
    // 4. This is a static instance of the property interface, which is used EVERYWHERE
    PropertyClass(BoxController, Props);
    
    // selectors
    // THE selBox is meant to be called on input.
    // This means the scope of calling this happens when input comes from a user.
    // THAT IS THIS IS ONLY CALLED WHEN THE USER TOUCHES OR DOES SOMETHING WHICH AFFECTS THE MODEL
    EventedSelector(BoxController::selBox)
    {
        // This line generates a box that is associated with the input we got.
        size_t collider = Props.with(meta)->collider();
        // First we limit the box
        ForEachEventedSelectorByProp(box)
            if (Props.with(*i)->collider() == collider) result.push_back(*i);
        return result;
    }
    EventedSelector(BoxController::selBoxes)
    {
        return collection->selectByProp(Props.strbox());
    }
    
    // operations
    // This is the update operation
    EventedOperation(BoxController::opUpdateBoxes)
    {
        opDelta;
        
        Collide2D* collide2D = manager->request<Collide2D>();
        
        Props.with(target);
        
        size_t collider = Props.collider();
        glm::vec2 position = Props.position();
        glm::vec2 velocity = Props.velocity();
        
        if (Props.flagMove().x || Props.flagMove().y)
        {
            // This is where we process the flagMove which we received from the input
            velocity = Props.flagMove() * 256.f;
        }
        
        glm::vec2 dist, step = velocity * delta;
        while (step.x != 0.f || step.y != 0.f)
        {
            if (collide2D->sweptCollide(collider, position, 0.f, step, dist).size() > 0)
            {
                if (velocity.x && step.x == 0.f) velocity.x = 0.f;
                if (velocity.y && step.y == 0.f) velocity.y = 0.f;
            }
            position += dist;
        }
        
        Props.position(position);
        Props.velocity(velocity);
        collide2D->update(collider, position, 0.f);
        
    }
    
    // api
    
    void BoxController::addBox(bool useController, glm::vec2 position)
    {
        PropertyLocal(GLDebugLayer, Props, GLDebug);
        
        Collide2D* collide2D = manager->request<Collide2D>();
        EventedCollection* entities = manager->request<EventedCollection>();

        PropertyCollection props(manager);
        Props.with(props);
        Props.box(true);
        Props.size(glm::vec2(64.f));
        Props.position(position);
        
        GLDebug.with(props)->defaults();
        GLDebug.size(Props.size());
        GLDebug.position(Props.position());
        GLDebug.texture(GLTextureColorStr(useController ? glm::makeColor(255,255,0) : glm::makeColor(255,0,0)));
        
        cpShape* shape = cpBoxShapeNew(NULL, Props.size().x, Props.size().y);
        size_t collider = collide2D->addShape(shape);
        collide2D->update(collider, position, 0.f);
        Props.collider(collider);

        entities->add("init", props);
        
        // This is where wee bind the input.
        // Entities->addInput basically allows us to push input values to the "meta" thingy we used above in the
        //  opCopyAllFromMeta
        if (useController)
        {
            InputMap* input = manager->request<InputMap>();
            
            uint8_t joystick = 0;
            InputMapVector playerMove;
            
            playerMove.left.keys.push_back({SDLK_LEFT});
            playerMove.right.keys.push_back({SDLK_RIGHT});
            playerMove.up.keys.push_back({SDLK_UP});
            playerMove.down.keys.push_back({SDLK_DOWN});
            
            playerMove.threshold = AxisMax;
            playerMove.left.joystickAxises.push_back({joystick, 0, AxisMin});
            playerMove.right.joystickAxises.push_back({joystick, 0, AxisMax});
            playerMove.up.joystickAxises.push_back({joystick, 1, AxisMin});
            playerMove.down.joystickAxises.push_back({joystick, 1, AxisMax});
            size_t move = input->addVector(playerMove);
            
            // common data for input events
            // THIS IS THE METADATA THAT PASSES INPUT VALUES AROUND
            PropertyCollection meta(manager);
            Props.with(meta);
            Props.collider(collider);
            
            // bind input map events to triggers
            // In this line, we associate the input device mapping we create above with a
            // particular entity.  When the update occurs, it changes the Props.flagMove
            //
            // When entities->addInput does is when a user hits a button it is going to
            // generate an event.  We have a selector and an operation for that event.
            // There is also the metadata for the event ... which is the collider id and
            // the flagmove value that the user has just changed.
            //
            // A third way: Every time a "move" input event occurs (mapped on line 178)
            //              -> CALL THE evtBoxProps event we mapped earlier
            //              -> PASS IN the meta value we created on 182, which is copied by opCopyAllFromMeta on 178
            //              -> AFTER SETTING the flagMove string property in meta
            entities->addInput(move, Props.evtBoxProps(), meta, Props.strflagMove());
        }
    }
    
}


