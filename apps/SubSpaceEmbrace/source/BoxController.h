
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace Boilerplate
{
    
    class BoxController : public copilot::CoreLogic
    {
    public:
        PropertyInterfaceEntity(Props,
                                PropertyEvent(BoxProps)
                                PropertyAccessor(box, bool)
                                PropertyAccessor(flagMove, glm::vec2)
                                PropertyAccessor(size, glm::vec2)
                                PropertyAccessor(position, glm::vec2)
                                PropertyAccessor(velocity, glm::vec2)
                                PropertyAccessor(collider, size_t))
        
    public:
        BoxController(copilot::CoreLogicManager* manager, const char* name);
        
        // interfaces
        PropertyStatic(Props);
        
        // selectors
        static EventedSelector(selBox);
        static EventedSelector(selBoxes);
        
        // operations
        static EventedOperation(opUpdateBoxes);
        
        // api
        void addBox(bool useController, glm::vec2 position);
    };
    
}