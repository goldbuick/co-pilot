/**
* This code is a part of the SubSpaceEmbrace Game.
* For full copyright and license information, please view the LICENSE file
* which should be distributed with this source code.
*
* @license MIT License
* @copyright Copyright (c) 2011, Steven Velozo
*/
#pragma once

#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
	class CelestialBody : public copilot::CoreLogic
	{
		// Interfaces

		// Selectors

		// Operations

		// Rendering

		// Api

	};
}
