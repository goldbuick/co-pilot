
#pragma once

#include <co+pilot/bundles/common/Scene.h>
#include <co+pilot/bundles/render/GLWindow.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
	class SceneBaseConfigurationController : public copilot::CoreLogic
	{
		// Interfaces

		// Selectors

		// Operations

		// Rendering

		// Api
	};

	class SceneBaseConfiguration : public copilot::Scene
	{
	public:
		SceneBaseConfiguration(copilot::CoreLogicManager* manager) : copilot::Scene(manager) { }
		void init();
	};
}
