
#pragma once

#include <co+pilot/bundles/common/Scene.h>
#include <co+pilot/bundles/render/GLWindow.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
	class SceneFightController : public copilot::CoreLogic
	{
		// Interfaces

		// Selectors

		// Operations

		// Rendering

		// Api
	};

	class SceneFight : public copilot::Scene
	{
	public:
		SceneFight(copilot::CoreLogicManager* manager) : copilot::Scene(manager) { }
		void init();
	};
}
