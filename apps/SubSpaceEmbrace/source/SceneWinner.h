
#pragma once

#include <co+pilot/bundles/common/Scene.h>
#include <co+pilot/bundles/render/GLWindow.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
	class SceneWinnerController : public copilot::CoreLogic
	{
		// Interfaces

		// Selectors

		// Operations

		// Rendering

		// Api
	};

	class SceneWinner : public copilot::Scene
	{
		public:
			SceneWinner(copilot::CoreLogicManager* manager) : copilot::Scene(manager) { }
			void init();
	};
}
