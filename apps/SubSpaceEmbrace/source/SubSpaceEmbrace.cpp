#include "SubSpaceEmbrace.h"

#include "SceneBaseConfiguration.h"
#include "SceneFight.h"
#include "SceneWinner.h"

#include <co+pilot/bundles/sdl/LibSDL.h>

using namespace copilot;

namespace coda
{
	Settings::Settings(CoreLogicManager* manager, const char* name)
		: CoreLogic(manager, name), settings(manager)
	{
		Props.with(settings);
		Props.FPS(60.f);
		Props.Unit(glm::vec2(640.f, 480.f));

		std::list<int> playerIds;
		playerIds.push_back(1);
		Props.PlayerIds(playerIds);
	}

	// interfaces
	PropertyClass(Settings, Props);

	// get next scene
	Scene* SubSpaceEmbrace::next(uint8_t result)
	{
		switch (result)
		{
			case kSceneBaseConfiguration: return new SceneBaseConfiguration(manager);
			case kSceneFight: return new SceneFight(manager);
			case kSceneWinner: return new SceneWinner(manager);
		}

		return NULL;
	}
}