
#pragma once

#include <co+pilot/core/common/Maths.h>
#include <co+pilot/bundles/common/Scene.h>
#include <co+pilot/bundles/common/EventedEntityCollection.h>

namespace coda
{
	class Settings : public copilot::CoreLogic
	{
	public:
		PropertyInterfaceEntity(Props,
			PropertyEvent(Init)
			PropertyAccessor(FPS, float)
			PropertyAccessor(Unit, glm::vec2)
			PropertyAccessor(PlayerIds, std::list<int>)
			PropertyAccessor(PlayerClasses, std::list<int>)
			PropertyAccessor(Winner, int))

	public:
		Settings(copilot::CoreLogicManager* manager, const char* name);
		PropertyStatic(Props);

	private:
		copilot::PropertyCollection settings;
	};

	enum SubSpaceEmbraceScenes
	{
		kSceneQuit = 0,
		kSceneBaseConfiguration,
		kSceneFight,
		kSceneWinner
	};

	class SubSpaceEmbrace : public copilot::SceneManager
	{
	public:
		SubSpaceEmbrace(copilot::CoreLogicManager* manager, const char* name)
			: copilot::SceneManager(manager, name) { }

		// get next scene
		copilot::Scene* next(uint8_t result);
	};

}
