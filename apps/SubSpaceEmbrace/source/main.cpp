
#include "Boilerplate.h"
#include <co+pilot/bundles/sdl/LibSDL.h>
#include <co+pilot/bundles/files/AssetManager.h>

using namespace copilot;

int main(int argc, char * argv[])
{
    CoreLogicManager manager;
    
    // config pub dir
    // To give this an absolute path:
    FileAccess* fileAccess = manager.request<FileAccess>();
    fileAccess->setDirAbsolute("/Some/FileFolder/");
    
    // Now here's the idiosyncracy... I need to create a "pub" folder in "/Some/FileFolder/" and put all the assets in there.
    AssetManager* assetManager = manager.request<AssetManager>();
    assetManager->config(argc, argv);
    
    // add window
    LibSDL* sdl = manager.request<LibSDL>();
    sdl->addWindow("SubSpaceEmbrace", glm::vec2(1024.f, 768.f));
    
    // main game loop
    Boilerplate::Boilerplate* boiler = manager.request<Boilerplate::Boilerplate>();
    boiler->run(0, 60.f);
    
    return 0;
}

