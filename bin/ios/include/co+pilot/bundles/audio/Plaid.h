
#pragma once

#include "../../core/Core.h"
#include "../../bundles/files/AssetManager.h"

#include <SDL2/SDL.h>
#include "plaid/audio/implementation.h"

namespace copilot
{    
    class PlaidAudioImp : public plaid::AudioImp
    {
    public:
        PlaidAudioImp(plaid::Audio &audio, plaid::AudioScheduler &scheduler);
        virtual ~PlaidAudioImp();
        
        void mix(Uint8 *stream, int len);
        
		virtual void startStream();
		virtual double time();
		virtual float load();
		virtual plaid::AudioFormat format();
		virtual void update();
        
    private:
        double currentTime;
        SDL_AudioSpec spec;
        SDL_AudioDeviceID deviceID;
    };
    
    typedef AssetRef<plaid::AudioClip> PlaidSoundRef;
    
    class PlaidAudio : public CoreLogic, public plaid::AudioDriver
    {
    public:
        PlaidAudio(CoreLogicManager* manager, const char* name);
        virtual ~PlaidAudio();
        
        virtual plaid::AudioImp *create(const plaid::AudioDriver::DriverParams &params);
        
        void update();
        plaid::Audio* getAudio();
        
    private:
        plaid::Audio audio;
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<PlaidSoundRef>(CoreLogicManager* manager);
    
}