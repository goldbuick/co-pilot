
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Collection.h"
#include "../../core/common/PropertyCollection.h"

#include <map>
#include <list>

namespace copilot
{
    
    class EntityCollection;
    
    // need to create Entity
    class Entity : public PropertyCollection
    {
    public:
        Entity(EntityCollection* collection, size_t handle);
        virtual ~Entity();
        
        EntityCollection* getCollection();
        size_t getHandle();
        
        // mark entity for cleanup
        bool getDead();
        void setDead(bool dead);
        
    private:
        bool dead;
        size_t handle;
        EntityCollection* collection;
    };
    
    class EntityCollection : public CoreLogic
    {
    public:
        EntityCollection(CoreLogicManager* manager, const char* name);
        virtual ~EntityCollection();
        
        Entity* add();
        Entity* get(size_t handle);
        size_t handle(size_t index);
        void remove(size_t handle);
        size_t total();
        
    private:
        PtrCollection<Entity> entities;
    };
    
}
