
#pragma once

#include "../../core/Core.h"

#include <string>

namespace copilot
{
    struct SettingsArgs
    {
        std::string pubPath;
    };
    
    class Settings : public CoreLogic
    {
    public:
        Settings(CoreLogicManager* manager, const char* name);
        virtual ~Settings();
        SettingsArgs parse(int argc, char* argv[]);
    };
    
}