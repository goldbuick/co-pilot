
#pragma once

#include "../../bundles/render/GLDraw.h"
#include "../../bundles/files/AssetManager.h"

#include <map>

namespace copilot
{
    struct GLGlyph
    {
        // draw hints
        float step;
        glm::vec2 offset;
        
        // render size
        glm::vec2 size;
        glm::vec2 halfSize;
        
        // uv sub-rect of texture to draw
        glm::vec2 uv1, uv2;
    };
    
    struct GLFontTexture
    {
        bool operator==(const GLFontTexture& rhs) { return texture.handle == rhs.texture.handle; }
        bool operator!=(const GLFontTexture& rhs) { return !(*this == rhs); }
        int fontHeight;
        GLTexture texture;
        std::map<int, GLGlyph> glyphs;
    };
    
    typedef AssetRef<GLFontTexture> GLFontTextureRef;
    
    class GLFont : public CoreLogic
    {
    public:
        GLFont(CoreLogicManager* manager, const char* name);
        
        GLFontTexture compileTexture(int fontHeight, const unsigned char* buffer, size_t bufferSize);
        
        void drawText(GLRenderQuadPool* quadPool, GLFontTexture* font, float x, float y, const char* text, glm::vec4** colors = NULL);
        float getTextHeight(GLFontTexture* font, const char* text);
        float getTextWidth(GLFontTexture* font, const char* text);
        float getTextStep(GLFontTexture* font, const char glyph);
        glm::vec2 getTextSize(GLFontTexture* font, const char* text);
        
    private:
        size_t currentHandle;
    };
    
    template<> void PropertyCollectionInit(GLFontTexture& value);
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLFontTextureRef>(CoreLogicManager* manager);

}