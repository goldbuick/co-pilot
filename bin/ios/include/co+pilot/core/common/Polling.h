
#pragma once

#include "Common.h"

#include <map>
#include <list>

namespace copilot
{
    
    template <class T>
    class Polling
    {
    public:
        Polling() : currentPollId(0) { }
        virtual ~Polling() { }
        
        uint16_t getNewPollId()
        {
            return ++currentPollId;
        }
        
        void addEntry(T entry)
        {
            for (typename std::map<uint16_t, std::list<T>>::iterator i=queue.begin(); i!=queue.end(); ++i)
                i->second.push_back(entry);
        }
        
        bool getEntry(uint16_t pollId, T& entry)
        {
            if (queue[pollId].size() == 0) return false;
            entry = queue[pollId].front();
            queue[pollId].pop_front();
            return true;
        }
        
    protected:
        uint16_t currentPollId;
        std::map<uint16_t, std::list<T>> queue;
    };
}