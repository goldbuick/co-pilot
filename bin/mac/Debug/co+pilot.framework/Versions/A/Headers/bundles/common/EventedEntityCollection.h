
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Collection.h"
#include "../../bundles/common/TaskRunner.h"
#include "../../core/common/PropertyCollection.h"

#include <map>
#include <list>
#include <queue>

namespace copilot
{
    
    class EventedCollection;
    class ReadEventedEntity;
    
    class WriteEventedEntity
    {
        friend class EventedCollection;
        friend class ReadEventedEntity;
        friend class PropertyCollectionEntityInterface;
    public:
        WriteEventedEntity(EventedCollection* collection, size_t handle, PropertyCollection& properties);
        virtual ~WriteEventedEntity();
        
        bool getDead();
        void setDead(bool dead);
        
        size_t getHandle();
        
        EventedCollection* getCollection();
        
        std::list<std::string> sync();
        
        bool has(const char* name)
        {
            return getProperties()->has(name);
        }
        
        template <class T>
        bool has(const char* name)
        {
            return getProperties()->has<T>(name);
        }
        
        template <class T>
        bool strHas(const char* format, ...)
        {
            va_list args;
            va_start(args, format);
            return has<T>(strPrint(format, args).c_str());
        }
        
        template <class T>
        T get(const char* name)
        {
            return getProperties()->get<T>(name);
        }
        
        template <class T>
        T strGet(const char* format, ...)
        {
            va_list args;
            va_start(args, format);
            return get<T>(strPrint(format, args).c_str());
        }
        
        template <class T>
        void set(const char* name, T value)
        {
            getProperties()->set(name, value);
        }
        
        template <class T>
        void strSet(const char* format, T value, ...)
        {
            va_list args;
            va_start(args, value);
            set(strPrint(format, args).c_str(), value);
        }
        
        void copyAllFrom(PropertyCollection* meta)
        {
            meta->copyAllTo(getProperties());
        }
        
        void copyFrom(const char* name, PropertyCollection* meta)
        {
            meta->copyTo(name, getProperties(), true);
        }
        
        void copyPropsFrom(std::list<std::string> props, PropertyCollection* meta)
        {
            meta->copyPropsTo(props, getProperties(), true);
        }
        
    private:
        PropertyCollection* getProperties();
        PropertyCollection* getReadProperties();
        
    private:
        bool dead;
        size_t handle;
        EventedCollection* collection;
        PropertyCollection* properties[2];
    };
    
    class ReadEventedEntity
    {
        friend class EventedCollection;
        friend class PropertyCollectionEntityInterface;
    public:
        ReadEventedEntity()
        : entity(NULL) { }
        
        ReadEventedEntity(WriteEventedEntity* inEntity)
        : entity(inEntity) { }
        
        ReadEventedEntity(ReadEventedEntity& other)
        {
            entity = other.entity;
        }
        
        ReadEventedEntity(const ReadEventedEntity& other)
        {
            entity = ((ReadEventedEntity*)&other)->entity;
        }
        
        bool valid()
        {
            return entity != NULL;
        }
        
        size_t getHandle()
        {
            if (!entity) return 0;
            return entity->getHandle();
        }
        
        std::list<std::string> keys()
        {
            if (!entity) return std::list<std::string>();
            return entity->getReadProperties()->keys();
        }
        
        bool has(const char* name)
        {
            if (!entity) return false;
            return entity->getReadProperties()->has(name);
        }
        
        template <class T>
        bool has(const char* name)
        {
            if (!entity) return false;
            return entity->getReadProperties()->has<T>(name);
        }
        
        template <class T>
        bool strHas(const char* format, ...)
        {
            va_list args;
            va_start(args, format);
            return has<T>(strPrint(format, args).c_str());
        }
        
        template <class T>
        T get(const char* name)
        {
            if (!entity) return T();
            return entity->getReadProperties()->get<T>(name);
        }
        
        template <class T>
        T strGet(const char* format, ...)
        {
            va_list args;
            va_start(args, format);
            return get<T>(strPrint(format, args).c_str());
        }
        
        void dump()
        {
            entity->getReadProperties()->dump();
        }
        
    private:
        WriteEventedEntity* entity;
    };
    
    class PropertyCollectionEntityInterface : public PropertyCollectionInterface
    {
    public:
        PropertyCollectionEntityInterface() : PropertyCollectionInterface() { }
        void with(PropertyCollection* inProps) { props = inProps; }
        void with(PropertyCollection& inProps) { props = &inProps; }
        void with(ReadEventedEntity& read) { props = read.entity->getReadProperties(); }
        void with(ReadEventedEntity* read) { props = read->entity->getReadProperties(); }
        void with(WriteEventedEntity& write) { props = write.getProperties(); }
        void with(WriteEventedEntity* write) { props = write->getProperties(); }
    };
    
    #define PropertyInterfaceEntity(name, propContent) \
        class _##name : public copilot::PropertyCollectionEntityInterface \
        { \
        public: \
            _##name* with(copilot::PropertyCollection* inProps) { copilot::PropertyCollectionEntityInterface::with(inProps); return this; } \
            _##name* with(copilot::PropertyCollection& inProps) { copilot::PropertyCollectionEntityInterface::with(inProps); return this; } \
            _##name* with(copilot::ReadEventedEntity& read) { copilot::PropertyCollectionEntityInterface::with(read); return this; } \
            _##name* with(copilot::ReadEventedEntity* read) { copilot::PropertyCollectionEntityInterface::with(read); return this; } \
            _##name* with(copilot::WriteEventedEntity& write) { copilot::PropertyCollectionEntityInterface::with(write); return this; } \
            _##name* with(copilot::WriteEventedEntity* write) { copilot::PropertyCollectionEntityInterface::with(write); return this; } \
            propContent \
        }; \

    #define PropertyEvent(name) \
        inline const char* evt##name() \
        { \
            return #name;\
        }

    // selection signature
    typedef std::list<ReadEventedEntity> (*EventedSelectorFunc)(CoreLogicManager* manager,
        EventedCollection* collection, PropertyCollection& meta, std::list<ReadEventedEntity>& current);
    
    // operation signature
    typedef void (*EventedOperationFunc)(CoreLogicManager* manager, EventedCollection* collection,
        PropertyCollection& meta, WriteEventedEntity* target);
    
    #define EventedSelector(name) std::list<copilot::ReadEventedEntity> name( \
        copilot::CoreLogicManager* manager, \
        copilot::EventedCollection* collection, \
        copilot::PropertyCollection& meta, \
        std::list<copilot::ReadEventedEntity>& current)
    
    #define ForEachEventedSelector std::list<copilot::ReadEventedEntity> result; \
        for (std::list<copilot::ReadEventedEntity>::iterator i=current.begin(); i!=current.end(); ++i)
    
    #define ForEachEventedSelectorByProp(prop) std::list<copilot::ReadEventedEntity> result; \
        std::list<copilot::ReadEventedEntity> list = collection->selectByProp(#prop); \
        for (std::list<copilot::ReadEventedEntity>::iterator i=list.begin(); i!=list.end(); ++i)

    #define EventedOperation(name) void name( \
        copilot::CoreLogicManager* manager, \
        copilot::EventedCollection* collection, \
        copilot::PropertyCollection& meta, \
        copilot::WriteEventedEntity* target)
    
    #define opDelta float delta = meta.get<float>("delta");
    
    // provides a template for generating incidents
    struct EventedTemplate
    {
        EventedSelectorFunc selector;
        EventedOperationFunc operation;
    };
    
    // queue instance of an event
    struct EventedEvent
    {
        size_t eventId;
        PropertyCollection meta;
        ReadEventedEntity origin;
    };
    
    // queue entity add
    struct EventedAddEvent
    {
        size_t handle;
        std::string eventName;
    };
    
    // tasks structs
    struct EventedTemplateTask
    {
        EventedTemplate event;
        PropertyCollection* meta;
        EventedCollection* collection;
        std::list<ReadEventedEntity>* current;
        std::list<ReadEventedEntity> selection;
    };
    
    struct EventedOperationTaskFunc
    {
        PropertyCollection* meta;
        EventedOperationFunc operation;
    };
    
    struct EventedOperationTask
    {
        WriteEventedEntity* target;
        EventedCollection* collection;
        std::list<EventedOperationTaskFunc> operations;
    };
    
    struct EventedSyncTask
    {
        WriteEventedEntity* target;
        std::list<std::string> changed;
    };
    
    // event generators
    class EventedDispatch : public CoreLogic
    {
    public:
        EventedDispatch(CoreLogicManager* manager, const char* name);
        virtual ~EventedDispatch();
        
        // trigger events
        virtual void dispatch(EventedCollection* collection) = 0;
    };
    
    // built in generators
    class EventedDispatchTimer : public EventedDispatch
    {
    public:
        EventedDispatchTimer(CoreLogicManager* manager, const char* name);
        virtual ~EventedDispatchTimer();
        
        // add timer
        void addTimer(EventedCollection* collection, float ticksPerSecond, EventedSelectorFunc selector, EventedOperationFunc operation);
        
        // trigger events
        void dispatch(EventedCollection* collection);
        
    private:
        size_t pollId;
        std::list<size_t> timers;
    };
    
    struct EventedDispatchInputMap
    {
        size_t mapId;
        std::string eventName;
        PropertyCollection meta;
        std::string property;
    };
    
    class EventedDispatchInput : public EventedDispatch
    {
    public:
        EventedDispatchInput(CoreLogicManager* manager, const char* name);
        virtual ~EventedDispatchInput();
        
        // add input
        void addInput(EventedCollection* collection, size_t mapId, const char* eventName, PropertyCollection& meta, const char* property);
        
        // trigger events
        void dispatch(EventedCollection* collection);
        
    private:
        size_t pollId;
        std::list<EventedDispatchInputMap> inputs;
    };
    
    struct EventedCollectionThreading;
    
    class EventedCollection : public CoreLogic
    {
    public:
        EventedCollection(CoreLogicManager* manager, const char* name);
        virtual ~EventedCollection();
        
        // add generators and handlers
        void addDispatch(EventedDispatch* dispatch);
        void addEvent(const char* eventName, EventedSelectorFunc selector, EventedOperationFunc operation);
        void addTimer(float ticksPerSecond, EventedSelectorFunc selector, EventedOperationFunc operation);
        void addInput(size_t mapId, const char* eventName, PropertyCollection& meta, const char* property);

        // trigger external event
        void trigger(const char* eventName);
        void trigger(const char* eventName, PropertyCollection& meta);
        void trigger(const char* eventName, PropertyCollection& meta, ReadEventedEntity& origin);
        
        // add an entity with given properties, and trigger event to init it
        // returns a handle to created entity
        size_t add(const char* initEventName, PropertyCollection properties);

        // get read only properties of given entity
        ReadEventedEntity get(size_t handle);

        // get handle for given index
        size_t handle(size_t index);
        
        // total entities
        size_t total();
        
        // select entities
        std::list<ReadEventedEntity> select(EventedSelectorFunc selector);
        std::list<ReadEventedEntity> select(EventedSelectorFunc selector, PropertyCollection& meta);
        
        // select entities by property
        std::list<ReadEventedEntity> selectByProp(const char* property);
        
        // process any queued events
        void dispatch();
                
        // built-in selections
        static EventedSelector(selEmpty);
        static EventedSelector(selByHandle);
        
        // built-in operations
        static EventedOperation(opCopyAllFromMeta);
        
    private:
        void _updateCache();
        void _flushAddQueue();
        bool _handleEventQueue();
        
    private:
        // threading state
        EventedCollectionThreading* threading;
        
        // templates to create incidents from external events
        std::map<size_t,std::list<EventedTemplate> > triggers;
        
        // for dispatch
        std::list<EventedDispatch*> dispatches;
        
        // events to process
        std::list<EventedEvent*> eventQueue;
        
        // new entities to create & process
        std::queue<EventedAddEvent*> addQueue;
        
        // write-able entities
        PtrCollection<WriteEventedEntity> entities;
        
        // cache of read-only entites
        std::list<size_t> handleCache;
        std::list<ReadEventedEntity> readCache;
        
        // cache of read-only by property
        std::map<std::string, std::map<size_t, ReadEventedEntity> > readCacheByProp;
    };
    
    
}
