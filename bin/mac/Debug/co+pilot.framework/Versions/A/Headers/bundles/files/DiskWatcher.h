
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Polling.h"

namespace copilot
{
    struct FileChanged
    {
        time_t timestamp;
        std::string filename;
    };
    
    struct DiskWatcherState;
    
    class DiskWatcher : public CoreLogic
    {
    public:
        DiskWatcher(CoreLogicManager* manager, const char* name);
        virtual ~DiskWatcher();
        
        uint16_t getNewPollId();
        bool getChangedFile(uint16_t pollId, FileChanged& fileChanged);
        
        void checkFiles();
        void trackResource(const char* resource, const char* extension);
        
    private:
        DiskWatcherState* state;
        std::list<FileChanged> tracking;
        Polling<FileChanged> changedFiles;
    };
    
}