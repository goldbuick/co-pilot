
#pragma once

#include "../../core/Core.h"

#include <list>
#include <string>
#include <time.h>

namespace copilot
{
    
    class FileBuffer
    {
    public:
        // create a file buffer from data & length
        FileBuffer(unsigned char* inData, time_t inTimestamp, size_t inLength) : data(inData), timestamp(inTimestamp), length(inLength) { }
        
        // destroy a file buffer
        virtual ~FileBuffer() { if (data) delete[] data; }
        
        // get buffer info
        inline size_t getLength() { return length; }
        inline time_t getTimestamp() { return timestamp; }
        inline unsigned char* getData() { return data; }
        inline const char* getText() { return (const char*)data; }
        
    private:
        size_t length;
        time_t timestamp;
        unsigned char* data;
    };
    
    struct FilePathInfo
    {
        // path before the filename
        std::string path;
        
        // just the filename
        std::string filename;
        
        // file type info
        std::string extension;
        
        // path + filename
        std::string pathFilename;
        
        // path + filename + extension
        std::string fullPath;
        
        // path info beyond the extension
        std::string remainder;
    };
    
    struct NetPathInfo
    {
        // address
        std::string address;
        
        // port
        std::string port;
        
        // remainder path
        std::string remotePath;
        
        // full address
        std::string fullAddress;
    };
    
    class FileAccess : public CoreLogic
    {
    public:
        FileAccess(CoreLogicManager* manager, const char* name);
        virtual ~FileAccess();
        
        // set current working directory
        void setDirRelative();
        void setDirAbsolute(const char* path);
        
        // return a timestamp of a given file
        time_t timestamp(const char* path);
        
        // make this a single call that breaks the path
        // up into interesting parts ..
        FilePathInfo pathInfo(const char* path);
        
        // attempts to parse a net path
        bool netPathInfo(const char* path, NetPathInfo* result);
        
        // helper function to make sure the required directories exist
        void createDirectories(const char* path);
        
        // a directory for public content, human editable
        
        // path for editor project directories
        const char* pathForPublic();
        
        // translate resource path into a public path
        const char *pathForPublicResource(const char* resource, const char* extension);
        
        // write a block of data to disk
        void bufferToPublic(const char* resource, const char* extension, FileBuffer* buffer);
        
        // list of public resources
        std::list<std::string> listOfPublicResources(const char* extension);
        
        // abstraction to allow reading from disk or bundled resource
        // is text added a null to the end of the buffer
        FileBuffer* bufferFromApp(const char* resource, const char* extension, bool isText = false);
        
        // load from given file path
        FileBuffer* bufferFromDisk(const char* path, bool isText = false);
        
        // first try app, then fallback to public
        FileBuffer* bufferFromAppOrPub(const char* resource, const char* extension, bool isText = false);
        
        // save to given file path
        bool bufferToDisk(const char* path, FileBuffer* buffer);
        
    private:
    };
    
}


