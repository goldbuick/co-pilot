
#pragma once

#include "../../../core/Core.h"
#include "../../../core/common/Maths.h"
#include "../../../bundles/files/AssetManager.h"

#include <string>
#include <vector>

namespace copilot
{
    namespace Spriter
    {
        // all time is measured in seconds
        
        struct BoneRef
        {
            int id;
            int key;
            int parent;
            int timeline;
        };
        
        struct ObjectRef
        {
            int id;
            int key;
            int parent;
            int timeline;
            int zindex;
        };
        
        struct AbsObjectRef
        {
            int id;
            int key;
            int parent;
            int timeline;
            int zindex;
            int file;
            int folder;
            float angle;
            float alpha;
            glm::vec2 pivot;
            glm::vec2 scale;
            std::string name;
            glm::vec2 position;
        };
        
        struct MainlineKey
        {
            int id;
            float time;
            std::vector<BoneRef> boneRefs;
            std::vector<ObjectRef> objectRefs;
        };
        
        enum TimelineKeyCurve
        {
            TimelineKeyCurveLinear = 1,
            TimelineKeyCurveInstant,
            TimelineKeyCurveQuadratic,
            TimelineKeyCurveCubic,
            TimelineKeyCurveQuartic,
            TimelineKeyCurveQuintic,
            TimelineKeyCurveBezier,
            TimelineKeyCurveMax
        };
        
        struct SpatialInfo
        {
            float c1;
            float c2;
            float c3;
            float c4;
            int spin;
            float time;
            float alpha;
            float angle;
            glm::vec2 scale;
            glm::vec2 position;
            TimelineKeyCurve curveType;
        };
        
        struct BoxTimelineKey : public SpatialInfo
        {
            glm::vec2 pivot;
        };
        
        struct PointTimelineKey : public SpatialInfo
        {
            
        };
        
        struct BoneTimelineKey : public SpatialInfo
        {
        };
        
        struct ObjectTimelineKey : public SpatialInfo
        {
            int file;
            int folder;
            glm::vec2 pivot;
            bool useDefaultPivot;
        };
        
        struct TimelineKey
        {
            int id;
            std::vector<BoxTimelineKey> box;
            std::vector<BoneTimelineKey> bone;
            std::vector<PointTimelineKey> point;
            std::vector<ObjectTimelineKey> object;
        };
        
        enum TimelineType
        {
            TimelineObject = 1,
            TimelineBone,
            TimelineBox,
            TimelinePoint
        };
        
        struct Timeline
        {
            int id;
            int obj;
            std::string name;
            TimelineType type;
            std::vector<TimelineKey> keys;
        };
        
        struct Animation
        {
            int id;
            bool looping;
            float length;
            std::string name;
            std::vector<Timeline> timelines;
            std::vector<MainlineKey> mainlineKeys;
            std::vector<AbsObjectRef> absObjectRefs;
        };
        
        struct MapInstruction
        {
            int file;
            int folder;
            int targetFile;
            int targetFolder;
        };
        
        struct CharacterMap
        {
            int id;
            std::string name;
            std::vector<MapInstruction> mapInstructions;
        };
        
        struct ObjectInfoBox
        {
            glm::vec2 size;
            glm::vec2 pivot;
            std::string name;
        };
        
        struct ObjectInfoBone
        {
            glm::vec2 size;
            std::string name;
        };
        
        struct ObjectInfoFrame
        {
            int file;
            int folder;
        };
        
        struct ObjectInfoSprite
        {
            std::string name;
            std::vector<ObjectInfoFrame> frames;
        };
        
        struct Entity
        {
            int id;
            std::string name;
            std::vector<Animation> animations;
            std::vector<CharacterMap> characterMaps;
            std::vector<ObjectInfoBox> objectBoxes;
            std::vector<ObjectInfoBone> objectBones;
            std::vector<ObjectInfoSprite> objectSprites;
        };
        
        struct File
        {
            int id;
            glm::vec2 size;
            glm::vec2 pivot;
            std::string name;
        };
        
        struct Folder
        {
            int id;
            std::string name;
            std::vector<File> files;
        };
        
        struct Scml
        {
            std::vector<Folder> folders;
            std::vector<Entity> entities;
        };
    }
    
    class SpriterScml : public CoreLogic
    {
    public:
        SpriterScml(CoreLogicManager* manager, const char* name);
        
        // load scml from disk
        Spriter::Scml loadScml(const char* filename);
        
        // load scml from given buffer
        Spriter::Scml loadScmlFromBuffer(const char* buffer, size_t length);
        
    private:
        
    };
    
    typedef AssetRef<Spriter::Scml> SpriterScmlRef;
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<SpriterScmlRef>(CoreLogicManager* manager);
    
}