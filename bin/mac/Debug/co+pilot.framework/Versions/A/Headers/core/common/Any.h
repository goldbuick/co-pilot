
#pragma once

#include "Common.h"

namespace copilot
{
    
    class Any
    {
    public:
        // constructors
        Any() : content(0) { }
        
        template<typename T>
        Any(const T &value) : content(new Holder<T>(value)) { }
        
        Any(const Any &other) : content(other.content ? other.content->clone() : NULL) { }
        
        // destructor
        ~Any() { delete content; }
        
        // helper functions
        const std::type_info &type_info() const
        {
            return content ? content->type_info() : typeid(void);
        }
        
        Any &swap(Any &rhs)
        {
            std::swap(content, rhs.content);
            return *this;
        }
        
        template<typename T>
        bool copyTo(T &value) const
        {
            const T* copyable = toPtr<T>();
            if (copyable) value = *copyable;
            return copyable != NULL;
        }
        
        template<typename T>
        const T* toPtr() const
        {
            return is<T>() ? &static_cast<Holder<T> *>(content)->held : NULL;
        }
        
        template<typename T>
        T* toUnsafePtr()
        {
            return (T*)(is<T>() ? &static_cast<Holder<T> *>(content)->held : NULL);
        }
        
        template<typename T>
        bool is() const
        {
            //void* a = (void*)&type_info();
            //void* b = (void*)&typeid(T);
            //return a == b;
            
            return type_info() == typeid(T);
            
            //return strcmp(type_info().name(), typeid(T).name()) == 0;
        }
        
        // operators
        Any &operator=(const Any &rhs)
        {
            Any temp(rhs);
            return swap(temp);
        }
        
        template<typename T>
        Any &operator=(const T &rhs)
        {
            Any temp(rhs);
            return swap(temp);
        }
        
        operator const void *() const
        {
            return content;
        }
        
    private:
        class Placeholder
        {
        public:
            virtual ~Placeholder() { }
            virtual Placeholder *clone() const = 0;
            virtual const std::type_info &type_info() const = 0;
        };
        
        template <typename T>
        class Holder : public Placeholder
        {
        public:
            Holder(const T &value) : held(value) { }
            
            virtual Placeholder* clone() const
            {
                return new Holder(held);
            }
            
            virtual const std::type_info &type_info() const
            {
                return typeid(T);
            }
            
        public:
            const T held;
        };
        Placeholder* content;
    };

    template<typename T>
    T AnyCast(const Any &operand)
    {
        const T* result = operand.toPtr<T>();
        return result ? *result : throw std::bad_cast();
    }
    
}
