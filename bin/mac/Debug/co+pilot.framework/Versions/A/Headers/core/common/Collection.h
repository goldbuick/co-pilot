
#pragma once

#include "Common.h"

#include <map>
#include <list>
#include <vector>

namespace copilot
{
    
    template <class T>
    class PtrCollection
    {
    public:
        PtrCollection() : currentHandle(0) { }
        
        virtual ~PtrCollection()
        {
            for (typename std::map<size_t, T*>::iterator i=collection.begin(); i!=collection.end(); ++i)
                delete i->second;
        }
        
        size_t nextHandle()
        {
            return currentHandle + 1;
        }
        
        size_t add(T* ptr)
        {
            ++currentHandle;
            collection[currentHandle] = ptr;
            indexes.clear();
            return currentHandle;
        }
        
        size_t queue(T* ptr)
        {
            ++currentHandle;
            pending[currentHandle] = ptr;
            return currentHandle;
        }
        
        void flush()
        {
            for (typename std::map<size_t, T*>::iterator i=pending.begin(); i!=pending.end(); ++i)
                collection[i->first] = i->second;
            indexes.clear();
            pending.clear();
        }
        
        T* get(size_t handle)
        {
            typename std::map<size_t, T*>::iterator i=collection.find(handle);
            if (i == collection.end()) return NULL;
            return i->second;
        }
        
        void remove(size_t handle, bool destroy = true)
        {
            typename std::map<size_t, T*>::iterator i=collection.find(handle);
            if (i == collection.end()) return;
            if (destroy) delete i->second;
            collection.erase(i);
            indexes.clear();
        }
        
        size_t handle(T* ptr)
        {
            for (typename std::map<size_t, T*>::iterator i=collection.begin(); i!=collection.end(); ++i)
                if (i->second == ptr) return i->first;
            return 0;
        }
        
        size_t handle(size_t index)
        {
            if (indexes.size() != collection.size()) updateIndex();
            if (index > indexes.size()) return 0;
            return indexes[index];
        }
        
        size_t total()
        {
            return collection.size();
        }
    protected:
        void updateIndex()
        {
            indexes.clear();
            for (typename std::map<size_t, T*>::iterator i=collection.begin(); i!=collection.end(); ++i)
                indexes.push_back(i->first);
        }
        
    protected:
        size_t currentHandle;
        std::vector<size_t> indexes;
        std::map<size_t, T*> collection;
        std::map<size_t, T*> pending;
    };

    
    template <class T>
    class StructCollection
    {
    public:
        StructCollection() : currentHandle(0) { }
        
        virtual ~StructCollection() { }
        
        size_t nextHandle()
        {
            return currentHandle + 1;
        }
        
        size_t add(T _struct)
        {
            ++currentHandle;
            collection[currentHandle] = _struct;
            indexes.clear();
            return currentHandle;
        }
        
        T* get(size_t handle)
        {
            typename std::map<size_t, T>::iterator i=collection.find(handle);
            if (i == collection.end()) return NULL;
            return &i->second;
        }
        
        void remove(size_t handle)
        {
            typename std::map<size_t, T>::iterator i=collection.find(handle);
            if (i == collection.end()) return;
            collection.erase(i);
            indexes.clear();
        }
        
        size_t handle(size_t index)
        {
            if (indexes.size() != collection.size()) updateIndex();
            if (index > indexes.size()) return 0;
            return indexes[index];
        }
        
        size_t total()
        {
            return collection.size();
        }
    protected:
        void updateIndex()
        {
            indexes.clear();
            for (typename std::map<size_t, T>::iterator i=collection.begin(); i!=collection.end(); ++i)
                indexes.push_back(i->first);
        }
        
    protected:
        size_t currentHandle;
        std::vector<size_t> indexes;
        std::map<size_t, T> collection;
    };
    
}


