
#pragma once

#include "../../core/Core.h"

namespace copilot
{
    
    class Example : public CoreLogic
    {
    public:
        Example(CoreLogicManager* manager, const char* name);
        virtual ~Example();
    private:
    };
    
}