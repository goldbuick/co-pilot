
#pragma once

#include "../../core/Core.h"

namespace copilot
{
    
    class SceneResult : public CoreLogic
    {
    public:
        SceneResult(CoreLogicManager* manager, const char* name);
        virtual ~SceneResult();
        
        uint8_t getResult();
        void setResult(uint8_t result);
        
    private:
        uint8_t result;
    };
    
    class Scene
    {
    public:
        Scene(CoreLogicManager* manager);
        virtual ~Scene();
        
        // mix-ins
        static uint8_t getResult(CoreLogicManager* scene);
        static void setResult(CoreLogicManager* scene, uint8_t result);
        
        // setup the scene
        virtual void init() = 0;
        
        // run a scene and get a return code
        uint8_t run(float renderFPS);
        
        // run a single iteration
        void render();
        bool step(uint8_t& result);
        
    protected:
        // disposable singletons
        CoreLogicManager* manager;
    };
    
    class SceneManager : public CoreLogic
    {
    public:
        SceneManager(CoreLogicManager* manager, const char* name);
        virtual ~SceneManager();
        
        // get next scene
        virtual Scene* next(uint8_t result) = 0;
        
        // loop until exit state is reached
        void run(uint8_t start, float renderFPS);
        
        // run a single iteration
        void render();
        void step(uint8_t start);
        
    private:
        Scene* current;
    };

}
