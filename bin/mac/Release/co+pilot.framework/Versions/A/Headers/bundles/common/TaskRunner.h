
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Any.h"

#include <vector>

namespace copilot
{
    
    typedef void (*TaskRunnerFunc)(CoreLogicManager* manager, Any* data);
    
    #define TaskRunnerLogic(name) void name( \
        copilot::CoreLogicManager* manager, \
        copilot::Any* data)
    
    class TaskRunnerExec;
    
    struct TaskRunnerState;
    
    struct TaskRunnerTask
    {
        Any data;
        bool complete;
    };
    
    /*
     
     treat it as a queue,
     that worked before
     
     
     processIndex vs writeIndex
     
     
     */
    
    class TaskRunner : public CoreLogic
    {
        friend class TaskRunnerExec;
    public:
        TaskRunner(CoreLogicManager* manager, const char* name);
        virtual ~TaskRunner();
        
        // get a task to setup
        Any* write();
        
        // execute all tasks
        void run(TaskRunnerFunc operation);
        
        // get a task that has been processed
        Any* result();
        
        // runner still active?
        bool active();

        // process a task
        void process();
        
    public:
        int killId;
        static int gKillId;
        
    public:
        bool dead;
        bool halt;
        int readIndex;
        int writeIndex;
        int processIndex;
        TaskRunnerState* state;
        TaskRunnerFunc operation;
        std::vector<TaskRunnerTask*> tasks;
    };
    
}