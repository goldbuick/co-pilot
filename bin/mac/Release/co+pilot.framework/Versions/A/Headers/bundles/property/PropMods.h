
#pragma once

#include "../../bundles/common/EventedEntityCollection.h"

namespace copilot
{
    
    class PropMods
    {
    public:
        // mix-in api
        
        // A -> B, every 6 seconds
        static void cooldownInit(PropertyCollection& props, const char* name);
        static float cooldownValue(WriteEventedEntity* entity, const char* name);
        static void cooldownTrigger(WriteEventedEntity* entity, const char* name, float next);
        static bool cooldownUpdate(WriteEventedEntity* entity, const char* name, float delta);
    };
    
}