
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Maths.h"
#include "../../bundles/files/AssetManager.h"
#include "../../core/common/PropertyCollection.h"

#include <string>

namespace copilot
{
    
    // Render data
    struct GLRenderStruct
    {
        struct
        {
            float x;
            float y;
            float z;
        } vert;
        
        struct
        {
            float u;
            float v;
        } tex;
        
        struct
        {
            float r;
            float g;
            float b;
            float a;
        } color;
    };
    
    struct GLRenderTri
    {
        GLRenderStruct p1;
        GLRenderStruct p2;
        GLRenderStruct p3;
    };
    
    struct GLRenderQuad
    {
        GLRenderStruct tl;
        GLRenderStruct bl;
        GLRenderStruct tr;
        GLRenderStruct br;
    };
    
    #define GLRenderMaxTri 2048
    
    struct GLRenderTriPool
    {
        uint16_t index;
        GLRenderTri triangles[GLRenderMaxTri];
    };
    
    struct GLRenderTriIndices
    {
        uint16_t indices[GLRenderMaxTri * 3];
    };
    
    #define GLRenderMaxQuad 2048
    
    struct GLRenderQuadPool
    {
        uint16_t index;
        GLRenderQuad quads[GLRenderMaxQuad];
    };
    
    struct GLRenderQuadIndices
    {
        uint16_t indices[GLRenderMaxQuad * 6];
    };
    
    struct GLTexture
    {
        bool operator==(const GLTexture& rhs) { return handle == rhs.handle; }
        bool operator!=(const GLTexture& rhs) { return !(*this == rhs); }
        uint32_t handle;
        glm::vec2 uv;
        glm::vec2 size;
        bool linear;
        bool repeatX;
        bool repeatY;
    };
    
    typedef AssetRef<GLTexture> GLTextureRef;
    
    #define GLShaderMaxTextures 4
    
    struct GLShader
    {
        bool operator==(const GLShader& rhs) { return handle == rhs.handle; }
        bool operator!=(const GLShader& rhs) { return !(*this == rhs); }
        uint32_t handle;
        int32_t uniTex[GLShaderMaxTextures];
        int32_t uniProjection;
        int32_t uniTransform;
        int32_t attrVertex;
        int32_t attrTexCoord0;
        int32_t attrColors;
    };
    
    struct GLShaderVert
    {
        std::string content;
    };
    
    struct GLShaderFrag
    {
        std::string content;
    };
    
    typedef AssetRef<GLShaderVert> GLShaderVertRef;
    typedef AssetRef<GLShaderFrag> GLShaderFragRef;
    
    class GLShaderRef : public AssetRef<GLShader>
    {
    public:
        GLShaderVertRef* vert;
        GLShaderFragRef* frag;
    };
    
    inline std::string GLTextureColorStr(glm::vec3 color)
    {
        return strPrint("%f,%f,%f", color.r, color.g, color.b);
    }
    
    class GLDraw : public CoreLogic
    {
    public:
        GLDraw(CoreLogicManager* manager, const char* name);
        virtual ~GLDraw();
        
        void config(glm::vec2 size);
        
        void clear(glm::vec3 color);
        
        void clippingOn(glm::rect rect);
        void clippingOff();
        
        int nextPowerOfTwo(int size);
        
        GLRenderTriIndices compileTriIndices();
        GLRenderQuadIndices compileQuadIndices();
        GLTexture compileTexture(glm::vec2 size, const unsigned char* buffer, size_t bufferSize);
        GLShader compileShader(const char* vertex, const char* fragment);
        
        // quick draw texture with custom uv coords
        // assume we're passing in 4 colors (bl, tl, tr, br)
        GLRenderQuad compileQuadTiled(GLTexture texture, glm::vec2 center, glm::vec2 size,
                                      glm::vec2* uvOffset = NULL, glm::vec2 *anchor = NULL, glm::vec4** colors = NULL, glm::mat4* transform = NULL);
        GLRenderQuad compileQuad(GLTexture texture, glm::vec2 center, glm::vec2 size,
                                 glm::vec2 *anchor = NULL, glm::vec4** colors = NULL, glm::mat4* transform = NULL, glm::vec2* uv1 = NULL, glm::vec2* uv2 = NULL);
        
        void render(GLShader shader,
                    glm::mat4 projection, glm::mat4 transform,
                    GLTexture* textures, uint16_t totalTextures,
                    GLRenderTri* triangles, GLRenderTriIndices* triangleIndices, uint16_t totalTriangles);
        
        void render(GLShader shader,
                    glm::mat4 projection, glm::mat4 transform,
                    GLTexture* textures, uint16_t totalTextures,
                    GLRenderQuad* quads, GLRenderQuadIndices* quadIndices, uint16_t totalQuads);
    protected:
        void use(GLShader shader,
                 glm::mat4 projection, glm::mat4 transform,
                 GLTexture* textures, uint16_t totalTextures,
                 long offset, int32_t stride);
    };

    template<> void PropertyCollectionInit(GLTextureRef*& value);
    template<> void PropertyCollectionInit(GLShaderRef*& value);
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLTextureRef>(CoreLogicManager* manager);
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLShaderVertRef>(CoreLogicManager* manager);
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLShaderFragRef>(CoreLogicManager* manager);
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLShaderRef>(CoreLogicManager* manager);
    
}

