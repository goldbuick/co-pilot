
#pragma once

#include "../../../bundles/render/GLRenderPipe.h"

namespace copilot
{
    
    class GLClearLayer
    {
    public:
        // render
        static GLRenderPipeLayer(render);
        
        // mix-ins
        static size_t addLayer(CoreLogicManager* manager, glm::vec3 color);
    };
    
}