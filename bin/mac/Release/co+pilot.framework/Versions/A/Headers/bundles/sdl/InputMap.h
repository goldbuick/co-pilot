
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Maths.h"
#include "../../core/common/Polling.h"
#include "../../core/common/Collection.h"

#include <SDL2/SDL.h>

#include <list>

namespace copilot
{
    struct InputMapKeyTrigger
    {
        SDL_Keycode key;
        int16_t pressed;
    };
    
    struct InputMapMouseButtonTrigger
    {
        uint8_t button;
        int16_t pressed;
    };
    
    struct InputMapJoystickButtonTrigger
    {
        uint8_t index;
        uint8_t button;
        int16_t pressed;
    };
    
    #define InputMapJoystickAxisTriggerFilter 9000
    enum InputMapJoystickAxisTriggerRange
    {
        AxisMin = -InputMapJoystickAxisTriggerFilter,
        AxisMax = InputMapJoystickAxisTriggerFilter
    };
    const float fAxisMin = -InputMapJoystickAxisTriggerFilter / 32767.f;
    const float fAxisMax = InputMapJoystickAxisTriggerFilter / 32767.f;
    
    struct InputMapJoystickAxisTrigger
    {
        uint8_t index;
        uint8_t axis;
        int16_t direction;
        float pressed;
    };
    
    struct InputMapTriggers
    {
        float pressed;
        float lastPressed;
        std::list<InputMapKeyTrigger> keys;
        std::list<InputMapMouseButtonTrigger> mouseButtons;
        std::list<InputMapJoystickAxisTrigger> joystickAxises;
        std::list<InputMapJoystickButtonTrigger> joystickButtons;
    };
    
    struct InputMapButton
    {
        InputMapTriggers triggers;
    };
    
    struct InputMapVector
    {
        float x;
        float y;
        float threshold;
        InputMapTriggers up;
        InputMapTriggers down;
        InputMapTriggers left;
        InputMapTriggers right;
    };
    
    struct InputMapEvent
    {
        size_t mapId;
        bool isButton;
        union
        {
            float pressed;
            struct
            {
                float x;
                float y;
            } vector;
        } value;
    };
    
    struct InputMapPair
    {
        size_t buttonId;
        size_t vectorsId;
    };
    
    class InputMap : public CoreLogic
    {
    public:
        InputMap(CoreLogicManager* manager, const char* name);
        virtual ~InputMap();
        
        uint16_t getNewPollId();
        bool getEvents(uint16_t pollId, InputMapEvent& event);
        
        size_t addButton(InputMapButton button);
        void clearButton(size_t mapId);
        
        size_t addVector(InputMapVector vector);
        void clearVector(size_t mapId);
        
    private:
        void updateTriggers(SDL_Event event, InputMapTriggers& triggers);
        
    private:
        uint16_t eventId;
        Polling<InputMapEvent> polling;
        StructCollection<InputMapPair> handles;
        StructCollection<InputMapButton> buttons;
        StructCollection<InputMapVector> vectors;
    };
    
}