
#pragma once

#include "Common/Common.h"

#include <map>
#include <string>
#include <cstdarg>

namespace copilot
{
    
    std::string format(const char* format, ... );
    
    class CoreLogicManager;

    class CoreLogic
    {
    public:
        CoreLogic(CoreLogicManager* manager, const char* name);
        virtual ~CoreLogic();
        
        CoreLogicManager* getManager();
        const char* getName();
        
    protected:
        std::string name;
        CoreLogicManager* manager;
    };
    
    class CoreLogicManager
    {
    public:
        CoreLogicManager(CoreLogicManager* chain = NULL);
        virtual ~CoreLogicManager();
        
        template <class T>
        T* request(const char* name)
        {
            T* instance = (T*)requestLogic(name);
            if (!instance)
            {
                instance = new T(this, name);
                addLogic(instance, name);
            }
            return instance;
        }
        
        template <class T>
        T* request()
        {
            const char* name = typeid(T).name();
            T* instance = (T*)requestLogic(name);
            if (!instance)
            {
                instance = new T(this, name);
                addLogic(instance, name);
            }
            return instance;
        }
        
    private:
        CoreLogic* requestLogic(const char* name);
        void addLogic(CoreLogic* entry, const char* name);
        
    private:
        CoreLogicManager* chain;
        std::map<std::string, CoreLogic*> logistics;
    };
    
}
