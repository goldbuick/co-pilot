
#pragma once

// Using radians
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/constants.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtx/vector_angle.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace glm
{
    inline glm::vec3 makeColor(float r, float g, float b)
    {
        return glm::vec3(r/255.f, g/255.f, b/255.f);
    }
    
    class rect
    {
    public:
        static rect withLTRB(float left, float top, float right, float bottom)
        {
            rect rect;
            rect.min.x = left;
            rect.min.y = top;
            rect.max.x = right;
            rect.max.y = bottom;
            return rect;
        }
        
        static rect withXYWH(float x, float y, float width, float height)
        {
            return withLTRB(x, y, x+width, y+height);
        }
        
        static rect withMinMax(vec2 inMin, vec2 inMax)
        {
            return withLTRB(inMin.x, inMin.y, inMax.x, inMax.y);
        }
        
        // default constructor
        rect() : min(0.f), max(0.f) { }
        
        // copy constructor
        rect(rect& other)
        {
            min = other.min;
            max = other.max;
        }
        rect(const rect& other)
        {
            min = other.min;
            max = other.max;
        }
        
        // assignment operator
        rect& operator=(rect& rhs)
        {
            min = rhs.min;
            max = rhs.max;
            return *this;
        }
        rect& operator=(const rect& rhs)
        {
            min = rhs.min;
            max = rhs.max;
            return *this;
        }
        
        bool operator==(const rect& rhs)
        {
            return (min == rhs.min && max == rhs.max);
        }
        
        bool operator!=(const rect& rhs)
        {
            return !(*this == rhs);
        }
        
    public:
        
        inline float getWidth() { return max.x - min.x; }
        inline float getHeight() { return max.y - min.y; }
        inline vec2 getSize() { return vec2(getWidth(), getHeight()); }
        
        inline float getLeft() { return min.x; }
        inline float getRight() { return max.x; }
        inline float getTop() { return min.y; }
        inline float getBottom() { return max.y; }
        
        inline vec2 getMin() { return min; }
        inline vec2 getMax() { return max; }
        
        inline bool isInside(vec2* point)
        {
            return (point->x >= min.x && point->x <= max.x && point->y >= min.y && point->y <= max.y);
        }
        
        inline bool isEqual(rect other)
        {
            return (min.x == other.min.x && min.y == other.min.y &&
                    max.x == other.max.x && max.y == other.max.y);
        }
        
        inline vec2 clip(vec2 position)
        {
            if (position.x < min.x) position.x = min.x;
            if (position.x > max.x) position.x = max.x;
            if (position.y < min.y) position.y = min.y;
            if (position.y > max.y) position.y = max.y;
            return position;
        }
        
    private:
        vec2 min;
        vec2 max;
    };
    
    inline mat4 makeRotation(float angle)
    {
        return rotate(angle, vec3(0.f, 0.f, 1.f));
    }
    
}