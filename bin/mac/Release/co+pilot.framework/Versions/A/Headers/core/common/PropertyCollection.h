
#pragma once

#include "Any.h"
#include "Maths.h"
#include "Common.h"
#include "../Core.h"

#include <map>
#include <list>
#include <string>

namespace copilot
{
    
    struct Property
    {
        Any value;
        bool dirty;
    };
    
    template <class T>
    void PropertyCollectionInit(T& value)
    {
        // default is no action
        printf("No PropertyCollectionInit %s\n", typeid(T).name());
    }
    
    class PropertyCollection
    {
    public:
        PropertyCollection() { }
        
        // copy constructor
        PropertyCollection(PropertyCollection& other);
        PropertyCollection(const PropertyCollection& other);

        bool has(const char* name);
        
        template <class T>
        bool has(const char* name)
        {
            Property* property = request(name);
            if (!property || !property->value.is<T>()) return false;
            return true;
        }
        
        template <class T>
        bool strHas(const char* format, ...)
        {
            va_list args;
            va_start(args, format);
            return has<T>(strPrint(format, args).c_str());
        }
        
        template <class T>
        T get(const char* name)
        {
            Property* property = request(name);
            if (!property) property = add(name);
            
            if (!property->value.is<T>())
            {
                T value;
                PropertyCollectionInit(value);
                
                //printf("PropertyCollection failed prop get: %s\n", name);
                
                property->dirty = true;
                property->value = value;
            }
            
            return *property->value.toPtr<T>();
        }
        
        template <class T>
        T strGet(const char* format, ...)
        {
            va_list args;
            va_start(args, format);
            return get<T>(strPrint(format, args).c_str());
        }
        
        template <class T>
        void set(const char* name, T value)
        {
            Property* property = request(name);
            
            if (!property)
            {
                property = add(name);
                property->dirty = true;
                property->value = value;
            }
            else if (!property->value.is<T>() || *((T*)property->value.toPtr<T>()) != value)
            {
                property->dirty = true;
                property->value = value;
            }
        }
        
        template <class T>
        void strSet(const char* format, T value, ...)
        {
            va_list args;
            va_start(args, value);
            set(strPrint(format, args).c_str(), value);
        }
        
        std::list<std::string> keys();
        std::list<std::string> dirty();

        std::list<std::string> copyDirtyTo(PropertyCollection* dest);
        void copyAllTo(PropertyCollection* dest, bool markDirty = true);
        void copyTo(const char* name, PropertyCollection* dest, bool markDirty);
        void copyPropsTo(std::list<std::string> props, PropertyCollection* dest, bool markDirty);
        
        // debug
        void dump();
        
    private:
        Property* add(const char* name);
        Property* request(const char* name);
        
    private:
        std::map<std::string, Property> properties;
    };
    
    // interface boilerplate
    
    class PropertyCollectionInterface
    {
    public:
        PropertyCollectionInterface() : props(NULL) { }
        void with(PropertyCollection* inProps) { props = inProps; }
        void with(PropertyCollection& inProps) { props = &inProps; }
        
    protected:
        PropertyCollection* props;
    };
    
    #define PropertyInterface(name, propContent) \
    class _##name : public copilot::PropertyCollectionInterface \
    { \
    public: \
        propContent \
    };
    
    #define ExtendPropertyInterface(parent, name, propContent) \
    class _##name : public parent \
    { \
    public: \
        propContent \
    };

    #define Props(klass) klass::_Props Props
    #define NamedProps(klass, name) klass::_##name name
    #define OtherProps(klass) klass::_Props klass##Props
    #define OtherNamedProps(klass, name) klass::_##name klass##name
    
    #define PropertyDefault(name, value) \
        inline void def##name() \
        { \
            if (!has##name()) name(value);\
        }
    
    #define PropertyDefaults(defContent) \
        inline void defaults() \
        { \
           defContent \
        }
    
    #define PropertyAccessor(name, type) \
        inline type name() \
        { \
            return props->get<type>(#name); \
        } \
        \
        \
        inline type name(type value) \
        { \
            props->set(#name, value); \
            return value; \
        } \
        \
        \
        inline const char* str##name() \
        { \
            return #name;\
        } \
        \
        \
        inline bool has##name() \
        { \
            return props->has<type>(#name);\
        }

    // built-in inits
    template<> void PropertyCollectionInit(int& value);
    template<> void PropertyCollectionInit(bool& value);
    template<> void PropertyCollectionInit(float& value);
    template<> void PropertyCollectionInit(size_t& value);
    template<> void PropertyCollectionInit(glm::rect& value);
    template<> void PropertyCollectionInit(glm::vec2& value);
    template<> void PropertyCollectionInit(glm::vec3& value);
    template<> void PropertyCollectionInit(glm::vec4& value);
    template<> void PropertyCollectionInit(glm::mat4& value);

}


