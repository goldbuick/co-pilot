
#pragma once

#include "../../core/Core.h"
#include "../../bundles/files/FileAccess.h"

#include <map>
#include <string>

namespace copilot
{
    class AssetManager;
    class AssetManagerLoader;
    
    struct Asset
    {
        size_t loadId;
        AssetManagerLoader* loader;
    };
    
    class AssetManagerLoader : public CoreLogic
    {
    public:
        AssetManagerLoader(CoreLogicManager* manager, const char* name) : CoreLogic(manager, name) { }
        virtual ~AssetManagerLoader() { }
        
        virtual const char* getExtension(AssetManager* assetManager) = 0;
        virtual Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension) = 0;
        virtual void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef) = 0;
    };
    
    template <class T>
    AssetManagerLoader* AssetManagerLoaderFactory(CoreLogicManager* manager)
    {
        // default is no action
        printf("No AssetManagerLoaderFactory %s\n", typeid(T).name());
        return NULL;
    }
    
    template <class T>
    class AssetRef : public Asset
    {
    public:
        AssetRef() { ref = NULL; }
    public:
        T* ref;
    };
    
    struct AssetManagerState;
    
    class AssetManager : public CoreLogic
    {
    public:
        AssetManager(CoreLogicManager* manager, const char* name);
        virtual ~AssetManager();
        
        // config pub path
        void config(int argc, char * argv[]);
        
        std::string assetPath(const char* resource, const char* extension);
        FileBuffer* assetBuffer(const char* resource, const char* extension, bool isText);
        
        // pass in what kind of AssetRef you are looking for
        template <class T>
        T* request(const char* resource)
        {
            AssetManagerLoader* loader = AssetManagerLoaderFactory<T>(manager);
            if (!loader) return NULL;
            
            lock();
            Asset* assetRef = assetRequest(resource, loader->getExtension(this));
            if (!assetRef)
            {
                assetRef = loader->getAsset(this, resource, loader->getExtension(this));
                if (assetRef)
                {
                    assetRef->loadId = 1;
                    assetRef->loader = loader;
                    std::string path = assetPath(resource, loader->getExtension(this));
                    assets[path] = assetRef;
                    
                    std::list<std::string> watchers = watching[path];
                    for (std::list<std::string>::iterator i=watchers.begin(); i!=watchers.end(); ++i)
                    {
                        std::map<std::string, Asset*>::iterator a = assets.find(*i);
                        if (a != assets.end())
                        {
                            a->second->loader->updateAsset(this, resource, loader->getExtension(this), assetRef, a->second);
                        }
                    }
                    
                    watchers = inverseWatching[path];
                    for (std::list<std::string>::iterator i=watchers.begin(); i!=watchers.end(); ++i)
                    {
                        std::map<std::string, Asset*>::iterator a = assets.find(*i);
                        if (a != assets.end())
                        {
                            loader->updateAsset(this, resource, loader->getExtension(this), a->second, assetRef);
                        }
                    }
                }
            }
            unlock();
            
            return (T*)assetRef;
        }
        
        template <class T, class W>
        void watch(const char* thisResource, const char* watchResource)
        {
            AssetManagerLoader* T_loader = AssetManagerLoaderFactory<T>(manager);
            AssetManagerLoader* W_loader = AssetManagerLoaderFactory<W>(manager);
            if (!T_loader || !W_loader) return;
            
            std::string T_path = assetPath(thisResource, T_loader->getExtension(this));
            std::string W_path = assetPath(watchResource, W_loader->getExtension(this));
            
            // add T_path to the list of things watching W_path
            watching[W_path].push_back(T_path);
            inverseWatching[T_path].push_back(W_path);
        }
        
    private:
        void lock();
        void unlock();
        Asset* assetRequest(const char* resource, const char* extension);

    private:
        AssetManagerState* state;
        
        // map path to resource name
        std::map<std::string, std::string> resources;
        
        // currently loaded assets
        std::map<std::string, Asset*> assets;
        
        // watch lists
        std::map<std::string,std::list<std::string> > watching;
        std::map<std::string,std::list<std::string> > inverseWatching;
    };
    
}