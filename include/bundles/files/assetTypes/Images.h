
#pragma once

#include "../../../core/Core.h"
#include "../../../core/common/Maths.h"

#include <vector>

namespace copilot
{
    struct Image
    {
        glm::vec2 size;
        std::vector<unsigned char> pixels;
        glm::vec4 get(glm::vec2 position);
        void set(glm::vec2 position, glm::vec4 color);
    };
    
    class Images : public CoreLogic
    {
    public:
        Images(CoreLogicManager* manager, const char* name);
        virtual ~Images();
        
        // check to see if image is valid
        bool valid(Image image);
        
        // create an empty image of given size
        Image createImage(glm::vec2 size);
        
        // load an image from disk
        Image loadImage(const char* filename);
        
        // load an image from given buffer
        Image loadImageFromBuffer(const unsigned char* buffer, size_t length);
        
        // create an image from given alpha map
        Image createImageFromAlpha(const unsigned char* alphaImage, glm::vec2 size);
        
        // create an image from given color
        Image createImageFromColor(glm::vec3 color, glm::vec2 size);
        
        // write an image to disk
        bool saveImage(Image image, const char* filename);
    };
    
}