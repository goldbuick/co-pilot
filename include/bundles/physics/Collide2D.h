
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Maths.h"
#include "../../core/common/Collection.h"
#include "chipmunk/chipmunk.h"

#include <map>
#include <list>

namespace copilot
{
    class Collide2D;
    
    struct Collision2D
    {
        size_t shape;
        cpContactPointSet contacts;
    };
    
    struct CollisionIgnores
    {
        Collide2D* collide2D;
        std::map<size_t, bool> shapes;
    };
    
    struct Collision2DSet
    {
        glm::vec2 start;
        glm::vec2 direction;
        Collide2D* collide2D;
        std::map<size_t, bool> ignores;
        std::list<Collision2D> collisions;
    };
    
    struct CollisionFlags
    {
        struct {
            bool enabled;
            glm::vec2 normal;
        } oneSided;
    };
    
    struct Collide2DThreading;
    
    class Collide2D : public CoreLogic
    {
    public:
        Collide2D(CoreLogicManager* manager, const char* name);
        virtual ~Collide2D();
        
        size_t addShape(cpShape* shape);
        void removeShape(size_t handle);
        cpShape* getShape(size_t handle);
        size_t getShapeHandle(size_t index);
        size_t getShapeHandle(cpShape* shape);
        size_t totalShapes();
        
        CollisionFlags getFlags(size_t handle);
        void setFlags(size_t handle, CollisionFlags flags);

        void update(size_t handle, glm::vec2 position, float angle);
        std::list<Collision2D> segmentCollide(std::map<size_t, bool> ignores, glm::vec2 start, glm::vec2 end, cpLayers layers, cpGroup group);
        std::list<Collision2D> collide(size_t handle, glm::vec2 position, float angle, glm::vec2 direction);
        std::list<Collision2D> sweptCollide(size_t handle, glm::vec2 position, float angle, glm::vec2& velocity, glm::vec2& dist);
        std::list<Collision2D> platformerCollide(size_t handle, glm::vec2 position, float angle, glm::vec2& velocity, glm::vec2& dist, glm::vec2& collide);
        
    private:
        std::list<Collision2D> query(std::map<size_t, bool> ignores, cpBody* body, cpShape* shape, glm::vec2 position, float angle, glm::vec2 direction);
        
    private:
        cpSpace* space;
        Collide2DThreading* threading;
        PtrCollection<cpShape> shapes;
        std::map<size_t, CollisionFlags> shapeFlags;
    };
    
}