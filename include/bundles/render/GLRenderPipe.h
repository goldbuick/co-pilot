
#pragma once

#include "../../bundles/render/GLDraw.h"
#include "../../bundles/common/TaskRunner.h"
#include "../../bundles/common/EventedEntityCollection.h"

#include <map>
#include <list>

namespace copilot
{
    class GLRenderPipe;
    class GLRenderPipeBatch;
    
    struct GLRenderScreenUnits
    {
        bool autosize;
        glm::vec2 target;
        glm::vec2 calculated;
        glm::vec2 screenToUnit;
        glm::vec2 unitToScreen;
    };
    
    struct GLRenderCamera2D
    {
        uint8_t rank;
        float angle;
        glm::vec2 scale;
        glm::vec2 position;
        std::list<size_t> layers;
        struct
        {
            bool autosize;
            glm::rect rect;
        } viewport;
        glm::rect cachedViewable;
    };
    
    // layer signature
    typedef void (*GLRenderLayerFunc)(CoreLogicManager* manager, EventedCollection* collection,
        GLRenderPipe* render, GLDraw* draw, GLRenderPipeBatch* batch,
        GLRenderScreenUnits* units, GLRenderCamera2D* camera2D,
        PropertyCollection meta, std::list<ReadEventedEntity>& selection);
    
    #define GLRenderPipeLayer(name) void name( \
        copilot::CoreLogicManager* manager, \
        copilot::EventedCollection* collection, \
        copilot::GLRenderPipe* render, \
        copilot::GLDraw* draw, \
        copilot::GLRenderPipeBatch* batch, \
        copilot::GLRenderScreenUnits* units, \
        copilot::GLRenderCamera2D* camera, \
        copilot::PropertyCollection meta, \
        std::list<copilot::ReadEventedEntity>& selection)
    
    class GLRenderPipeLayerData
    {
    public:
        GLRenderPipeLayerData(bool inParallel, PropertyCollection inMeta,
                              EventedSelectorFunc inSelector, GLRenderLayerFunc inRender)
        : parallel(inParallel), meta(inMeta), selector(inSelector), render(inRender) { }
    public:
        bool parallel;
        PropertyCollection meta;
        GLRenderLayerFunc render;
        EventedSelectorFunc selector;
    };
        
    struct GLRenderPipeTaskTri
    {
        GLShader shader;
        glm::mat4 projection;
        glm::mat4 transform;
        GLTexture* textures;
        uint16_t totalTextures;
        GLRenderTri* triangles;
        uint16_t totalTriangles;
    };

    struct GLRenderPipeTaskQuad
    {
        GLShader shader;
        glm::mat4 projection;
        glm::mat4 transform;
        GLTexture* textures;
        uint16_t totalTextures;
        GLRenderQuad* quads;
        uint16_t totalQuads;
    };
    
    struct GLRenderPipeTask
    {
        GLRenderPipe* render;
        GLRenderCamera2D* camera;
        GLRenderTriPool* triPool;
        GLRenderQuadPool* quadPool;
        GLRenderScreenUnits* units;
        GLRenderPipeLayerData* layer;
        EventedCollection* collection;
        std::list<ReadEventedEntity> selection;
        std::queue<GLRenderPipeTaskTri> tris;
        std::queue<GLRenderPipeTaskQuad> quads;
    };
    
    class GLRenderPipeBatch
    {
    public:
        GLRenderPipeBatch(GLRenderPipeTask* task);
        GLRenderTriPool* getTriPool();
        GLRenderQuadPool* getQuadPool();
        void render(GLShader shader,
                    glm::mat4 projection, glm::mat4 transform,
                    GLTexture* textures, uint16_t totalTextures,
                    GLRenderTriPool* triPool);
        void render(GLShader shader,
                    glm::mat4 projection, glm::mat4 transform,
                    GLTexture* textures, uint16_t totalTextures,
                    GLRenderQuadPool* quadPool);
    private:
        uint16_t triIndex;
        uint16_t quadIndex;
        GLRenderPipeTask* task;
    };
    
    #define GLRenderPipeDepth 32
    
    class GLRenderPipe : public CoreLogic
    {
    public:
        GLRenderPipe(CoreLogicManager* manager, const char* name);
        virtual ~GLRenderPipe();
        
        size_t addCamera2D(int rank);
        GLRenderCamera2D* getCamera2D(size_t handle);
        void removeCamera2D(size_t handle);
        size_t camera2DHandle(size_t index);
        size_t totalCamera2Ds();
        
        void setUnitForWindow(size_t windowHandle, GLRenderScreenUnits unit);
        glm::vec2 getUnitForWindow(size_t windowHandle);
        
        glm::vec2 unitVecToScreen(GLRenderScreenUnits* unit, glm::vec2 vec);
        glm::rect unitRectToScreen(GLRenderScreenUnits* unit, glm::rect rect);
        glm::vec2 screenVecToUnit(GLRenderScreenUnits* unit, glm::vec2 vec);
        glm::rect screenRectToUnit(GLRenderScreenUnits* unit, glm::rect rect);
        
        glm::mat4 unitProjectionOrtho(GLRenderScreenUnits* unit);
        glm::mat4 camera2DTransform(GLRenderScreenUnits* unit, GLRenderCamera2D* camera2D, glm::vec2 parallax);
        
        size_t addLayer(EventedSelectorFunc selector, GLRenderLayerFunc render, bool parallel = true);
        size_t addLayer(PropertyCollection meta, EventedSelectorFunc selector, GLRenderLayerFunc render, bool parallel = true);
        void removeLayer(size_t handle);
        size_t layerHandle(size_t index);
        size_t totalLayers();
        
        void addLayerToCamera2D(size_t layerHandle, size_t cameraHandle);
        void removeLayerFromCamera2D(size_t layerHandle, size_t cameraHandle);
        std::list<size_t> layersForCamera2D(size_t cameraHandle);
        
        void addCamera2DToWindow(size_t cameraHandle, size_t windowHandle);
        void removeCamera2DFromWindow(size_t cameraHandle, size_t windowHandle);
        std::list<size_t> camera2DsForWindow(size_t windowHandle);
        
        // draw all the things in the pipe!
        void dispatch(EventedCollection* entities);
        
    private:
        GLRenderTriPool* readTriPool();
        GLRenderQuadPool* readQuadPool();
        GLRenderTriPool* writeTriPool();
        GLRenderQuadPool* writeQuadPool();
        
    private:
        // only need one set of these
        GLRenderTriIndices triIndices;
        GLRenderQuadIndices quadIndices;
        
        // ring buffer of quads
        struct
        {
            int read;
            int write;
            GLRenderTriPool pool[GLRenderPipeDepth];
        } tri;
        struct
        {
            int read;
            int write;
            GLRenderQuadPool pool[GLRenderPipeDepth];
        } quad;
        
        PtrCollection<GLRenderPipeLayerData> layers;
        StructCollection<GLRenderCamera2D> camera2Ds;
        std::map<size_t,GLRenderScreenUnits> unitsForWindows;
        std::map<size_t,std::list<size_t> > camerasForWindows;
    };
    
}

// built-in layer types
#include "../../bundles/render/layers/GLDebugLayer.h"
#include "../../bundles/render/layers/GLClearLayer.h"


