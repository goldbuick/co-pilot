
#pragma once

#include "../../../bundles/render/GLRenderPipe.h"

namespace copilot
{
    
    class GLDebugLayer
    {
    public:
        // interfaces
        PropertyInterfaceEntity(Props,
            PropertyDefault(glSize, glm::vec2(8.f))
            PropertyDefault(glAngle, 0.f)
            PropertyDefault(glTexture, NULL)
            PropertyDefault(glShader, NULL)
            PropertyDefaults(
                defglSize();
                defglAngle();
                defglTexture();
            )
            PropertyAccessor(glSize, glm::vec2)
            PropertyAccessor(glAngle, float)
            PropertyAccessor(glTexture, GLTextureRef*)
            PropertyAccessor(glShader, GLShaderRef*)
            PropertyAccessor(position, glm::vec2))
        
        PropertyInterface(Meta,
            PropertyAccessor(parallax, glm::vec2))
        
    public:
        // render
        static GLRenderPipeLayer(render);
        
        // mix-ins
        static GLShaderRef* shader(CoreLogicManager* manager);
        static GLTextureRef* texture(CoreLogicManager* manager, glm::vec3 color);
        static size_t addLayer(CoreLogicManager* manager, EventedSelectorFunc selector);
        static size_t addLayer(CoreLogicManager* manager, PropertyCollection& meta, EventedSelectorFunc selector);
    };
    
}