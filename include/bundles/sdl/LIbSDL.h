
#pragma once

#include "../../core/Core.h"
#include "../../core/common/Maths.h"
#include "../../core/common/Polling.h"
#include "../../core/common/Collection.h"

#include <SDL2/SDL.h>

#include <map>
#include <list>
#include <vector>

namespace copilot
{
    struct LibSDLWindow
    {
        bool focus;
        SDL_Window* state;
    };
    
    struct LibSDLTimer
    {
        float delta;
        uint32_t eventId;
        uint64_t lastTime;
    };
    
    class LibSDL : public CoreLogic
    {
    public:
        LibSDL(CoreLogicManager* manager, const char* name);
        virtual ~LibSDL();
        
        bool active();
        void setEscToQuit(bool enable);
        
        uint16_t getNewPollId();
        bool getEvents(uint16_t pollId, SDL_Event& event);
        
        uint32_t addTimer(float ticksPerSecond);
        
        uint32_t addCustomEventId(const char* name);
        
        uint64_t getTime();
        float getDeltaTime(uint64_t now, uint64_t lastTime);
        
        size_t addWindow(const char* title, glm::vec2 size);
        void removeWindow(size_t handle);
        LibSDLWindow* getWindow(size_t handle);
        size_t getWindowHandle(size_t index);
        size_t totalWindows();
        void makeCurrentWindow(size_t handle);
        void swapBuffersWindow(size_t handle);
        
        const char* getJoystickName(size_t index);
        SDL_JoystickID getJoystickID(size_t index);
        
    private:
        bool escToQuit;
        bool shouldQuit;
        size_t currentWindow;
        SDL_GLContext context;
        Polling<SDL_Event> eventQueue;
        std::list<LibSDLTimer> timers;
        std::vector<SDL_Joystick*> joysticks;
        StructCollection<LibSDLWindow> windows;
        std::map<std::string, uint32_t> customEvents;
    };
}