
#pragma once

#include "LibSDL.h"

#include <queue>

namespace copilot
{
    class SDLThreadLock
    {
    public:
        SDLThreadLock()
        {
            mutex = SDL_CreateMutex();
        }
        virtual ~SDLThreadLock()
        {
            SDL_DestroyMutex(mutex);
        }
        
        void lock()
        {
            SDL_LockMutex(mutex);
        }
        
        void unlock()
        {
            SDL_UnlockMutex(mutex);
        }
        
    private:
        SDL_mutex* mutex;
    };
    
}