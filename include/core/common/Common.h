
#pragma once

#ifdef _MSC_VER

    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif

    typedef __int16 int16_t;
    typedef unsigned __int16 uint16_t;
    typedef __int32 int32_t;
    typedef unsigned __int32 uint32_t;
    typedef __int64 int64_t;
    typedef unsigned __int64 uint64_t;

    #pragma warning( disable : 4244 )

#else

    #include <string.h>
    #include <stdint.h>

#endif

#include <vector>
#include <string>
#include <stddef.h>
#include <assert.h>

namespace copilot
{
    std::wstring strToWide(std::string& str);
    std::string strPrint(const char* format, ...);
    std::string strPrint(const char* format, va_list args);
    std::vector<std::string> strSplit(const char* source, const char* delim);
}
