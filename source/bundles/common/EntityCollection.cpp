
#include <bundles/common/EntityCollection.h>

using namespace copilot;

Entity::Entity(EntityCollection* inCollection, size_t inHandle)
: collection(inCollection), handle(inHandle) { }

Entity::~Entity() { }

EntityCollection* Entity::getCollection()
{
    return collection;
}
size_t Entity::getHandle()
{
    return handle;
}

// mark entity for cleanup
bool Entity::getDead()
{
    return dead;
}
void Entity::setDead(bool inDead)
{
    dead = inDead;
}


EntityCollection::EntityCollection(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name) { }

EntityCollection::~EntityCollection() { }

Entity* EntityCollection::add()
{
    Entity* entity = new Entity(this, entities.nextHandle());
    entities.add(entity);
    return entity;
}
Entity* EntityCollection::get(size_t handle)
{
    return entities.get(handle);
}
size_t EntityCollection::handle(size_t index)
{
    return entities.handle(index);
}
void EntityCollection::remove(size_t handle)
{
    entities.remove(handle);
}
size_t EntityCollection::total()
{
    return entities.total();
}
