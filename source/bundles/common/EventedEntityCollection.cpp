
#include <bundles/sdl/LibSDL.h>
#include <bundles/sdl/InputMap.h>
#include <bundles/sdl/LibSDLThreads.h>
#include <bundles/common/EventedEntityCollection.h>

namespace copilot
{
    
    struct EventedCollectionThreading
    {
        SDLThreadLock addQueue;
        SDLThreadLock readCache;
        SDLThreadLock eventQueue;
    };
    
}

using namespace copilot;

WriteEventedEntity::WriteEventedEntity(EventedCollection* inCollection, size_t inHandle, PropertyCollection& inProperties)
: dead(false), collection(inCollection), handle(inHandle)
{
    for (int i=0; i<2; ++i)
    {
        properties[i] = new PropertyCollection();
        inProperties.copyAllTo(properties[i], false);
    }
}
WriteEventedEntity::~WriteEventedEntity()
{
    for (int i=0; i<2; ++i) delete properties[i];
}

bool WriteEventedEntity::getDead()
{
    return dead;
}
void WriteEventedEntity::setDead(bool inDead)
{
    dead = inDead;
}

size_t WriteEventedEntity::getHandle()
{
    return handle;
}

PropertyCollection* WriteEventedEntity::getProperties()
{
    return properties[0];
}
EventedCollection* WriteEventedEntity::getCollection()
{
    return collection;
}

PropertyCollection* WriteEventedEntity::getReadProperties()
{
    return properties[1];
}

std::list<std::string> WriteEventedEntity::sync()
{
    std::list<std::string> props = properties[0]->copyDirtyTo(properties[1]);
    return props;
}



EventedDispatch::EventedDispatch(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
}
EventedDispatch::~EventedDispatch()
{
    
}



EventedDispatchTimer::EventedDispatchTimer(CoreLogicManager* manager, const char* name)
: EventedDispatch(manager, name)
{
    LibSDL* sdl = manager->request<LibSDL>();
    pollId = sdl->getNewPollId();
}
EventedDispatchTimer::~EventedDispatchTimer()
{
    
}

// add timer
void EventedDispatchTimer::addTimer(EventedCollection* collection, float ticksPerSecond, EventedSelectorFunc selector, EventedOperationFunc operation)
{
    LibSDL* sdl = manager->request<LibSDL>();

    size_t timerId = sdl->addTimer(ticksPerSecond);
    timers.push_back(timerId);
    timers.unique();
    
    std::string eventName = strPrint("timer.%u", timerId);
    
    collection->addEvent(eventName.c_str(), selector, operation);
}

// trigger events
void EventedDispatchTimer::dispatch(EventedCollection* collection)
{
    LibSDL* sdl = manager->request<LibSDL>();
    
    std::map<size_t, bool> timerCap;
    for (std::list<size_t>::iterator i=timers.begin(); i!=timers.end(); ++i)
        timerCap[*i] = false;
    
    SDL_Event event;
    while (sdl->getEvents(pollId, event))
    {
        for (std::list<size_t>::iterator i=timers.begin(); i!=timers.end(); ++i)
        {
            size_t timerId = (*i);
            std::string eventName = strPrint("timer.%u", timerId);
            
            if (!timerCap[timerId] && event.type == timerId)
            {
                timerCap[timerId] = true;
                PropertyCollection meta;
                meta.set("delta", (float)event.user.code / 1000.f);
                collection->trigger(eventName.c_str(), meta);
            }
        }
    }
}



EventedDispatchInput::EventedDispatchInput(CoreLogicManager* manager, const char* name)
: EventedDispatch(manager, name)
{
    LibSDL* sdl = manager->request<LibSDL>();
    pollId = sdl->getNewPollId();
}
EventedDispatchInput::~EventedDispatchInput()
{
    
}

// add input
void EventedDispatchInput::addInput(EventedCollection* collection, size_t mapId, const char* eventName, PropertyCollection& meta, const char* property)
{
    EventedDispatchInputMap inputMap;
    inputMap.mapId = mapId;
    inputMap.eventName = eventName;
    inputMap.meta = meta;
    inputMap.property = property;

    inputs.push_back(inputMap);
}

// trigger events
void EventedDispatchInput::dispatch(EventedCollection* collection)
{
    InputMap* input = manager->request<InputMap>();
    
    InputMapEvent mapEvent;
    while (input->getEvents(pollId, mapEvent))
    {
        for (std::list<EventedDispatchInputMap>::iterator i=inputs.begin(); i!=inputs.end(); ++i)
        {
            if (mapEvent.mapId == i->mapId)
            {
                if (mapEvent.isButton)
                {
                    i->meta.set(i->property.c_str(), mapEvent.value.pressed);
                }
                else
                {
                    i->meta.set(i->property.c_str(), glm::vec2(mapEvent.value.vector.x, mapEvent.value.vector.y));
                }
                collection->trigger(i->eventName.c_str(), i->meta);
            }
        }
    }
}



EventedCollection::EventedCollection(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
    threading = new EventedCollectionThreading();
    addDispatch(manager->request<EventedDispatchTimer>());
    addDispatch(manager->request<EventedDispatchInput>());
}
EventedCollection::~EventedCollection()
{
    delete threading;
}

void EventedCollection::addDispatch(EventedDispatch* dispatch)
{
    threading->eventQueue.lock();
    dispatches.push_back(dispatch);
    dispatches.unique();
    threading->eventQueue.unlock();
}

void EventedCollection::addEvent(const char* eventName, EventedSelectorFunc selector, EventedOperationFunc operation)
{
    threading->eventQueue.lock();
    LibSDL* sdl = manager->request<LibSDL>();
    size_t eventId = sdl->addCustomEventId(eventName);
    
    triggers[eventId].push_back({ selector, operation });
    threading->eventQueue.unlock();
}
void EventedCollection::addTimer(float ticksPerSecond, EventedSelectorFunc selector, EventedOperationFunc operation)
{
    threading->eventQueue.lock();
    EventedDispatchTimer* timer = manager->request<EventedDispatchTimer>();
    timer->addTimer(this, ticksPerSecond, selector, operation);
    threading->eventQueue.unlock();
}
void EventedCollection::addInput(size_t mapId, const char* eventName, PropertyCollection& meta, const char* property)
{
    threading->eventQueue.lock();
    EventedDispatchInput* input = manager->request<EventedDispatchInput>();
    input->addInput(this, mapId, eventName, meta, property);
    threading->eventQueue.unlock();
}

// trigger external event
void EventedCollection::trigger(const char* eventName)
{
    threading->eventQueue.lock();
    LibSDL* sdl = manager->request<LibSDL>();
    size_t eventId = sdl->addCustomEventId(eventName);
    EventedEvent* event = new EventedEvent;
    event->eventId = eventId;
    eventQueue.push_back(event);
    threading->eventQueue.unlock();
}

void EventedCollection::trigger(const char* eventName, PropertyCollection& meta)
{
    threading->eventQueue.lock();
    LibSDL* sdl = manager->request<LibSDL>();
    size_t eventId = sdl->addCustomEventId(eventName);
    EventedEvent* event = new EventedEvent;
    event->eventId = eventId;
    event->meta = meta;
    eventQueue.push_back(event);
    threading->eventQueue.unlock();
}

void EventedCollection::trigger(const char* eventName, PropertyCollection& meta, ReadEventedEntity& origin)
{
    threading->eventQueue.lock();
    LibSDL* sdl = manager->request<LibSDL>();
    size_t eventId = sdl->addCustomEventId(eventName);
    EventedEvent* event = new EventedEvent;
    event->eventId = eventId;
    event->meta = meta;
    event->origin = origin;
    eventQueue.push_back(event);
    threading->eventQueue.unlock();
}

// add an entity with given properties, and trigger event to init it
// returns a handle to created entity
size_t EventedCollection::add(const char* initEventName, PropertyCollection properties)
{
    threading->addQueue.lock();
    WriteEventedEntity* entity = new WriteEventedEntity(this, entities.nextHandle(), properties);
    size_t handle = entities.queue(entity);
    EventedAddEvent* event = new EventedAddEvent;
    event->handle = handle;
    event->eventName = initEventName;
    addQueue.push(event);
    threading->addQueue.unlock();
    return handle;
}

// get read only properties of given entity
ReadEventedEntity EventedCollection::get(size_t handle)
{
    return ReadEventedEntity(entities.get(handle));
}

// get handle for given index
size_t EventedCollection::handle(size_t index)
{
    return entities.handle(index);
}

// total entities
size_t EventedCollection::total()
{
    return entities.total();
}

// select entities
std::list<ReadEventedEntity> EventedCollection::select(EventedSelectorFunc selector)
{
    // rebuild cache on-demand
    if (readCache.size() != entities.total()) _updateCache();
    
    threading->readCache.lock();
    std::list<ReadEventedEntity> start = readCache;
    threading->readCache.unlock();
    
    // invoke selector
    PropertyCollection meta;
    return (*selector)(manager, this, meta, start);
}

std::list<ReadEventedEntity> EventedCollection::select(EventedSelectorFunc selector, PropertyCollection& meta)
{
    // rebuild cache on-demand
    if (readCache.size() != entities.total()) _updateCache();
    
    threading->readCache.lock();
    std::list<ReadEventedEntity> start = readCache;
    threading->readCache.unlock();
    
    // invoke selector
    return (*selector)(manager, this, meta, start);
}

// select entities by property
std::list<ReadEventedEntity> EventedCollection::selectByProp(const char* property)
{
    // rebuild cache on-demand
    if (readCache.size() != entities.total()) _updateCache();
    
    std::list<ReadEventedEntity> result;
    std::map<std::string, std::map<size_t, ReadEventedEntity> >::iterator cache;
    
    threading->readCache.lock();
    cache = readCacheByProp.find(property);
    if (cache != readCacheByProp.end())
    {
        for (std::map<size_t, ReadEventedEntity>::iterator i=cache->second.begin(); i!=cache->second.end(); ++i)
        {
            result.push_back(i->second);
        }
        
        // return cached
        threading->readCache.unlock();
        return result;
    }
    
    // build initial list
    readCacheByProp[property].size();
    for (std::list<ReadEventedEntity>::iterator i=readCache.begin(); i!=readCache.end(); ++i)
    {
        if (i->has(property))
        {
            result.push_back(*i);
            readCacheByProp[property][i->getHandle()] = *i;
        }
    }

    // return cached
    threading->readCache.unlock();
    return result;
}

void EventedCollection::_updateCache()
{
    // rebuild cache
    threading->readCache.lock();
    readCache.clear();
    handleCache.clear();
    for (int i=0; i<entities.total(); ++i)
    {
        handleCache.push_back(entities.handle(i));
        readCache.push_back(ReadEventedEntity(entities.get(handleCache.back())));
    }
    threading->readCache.unlock();
}

void EventedCollection::_flushAddQueue()
{
    // flush added objects
    entities.flush();
    
    // track added objects
    EventedAddEvent* addEntity;
    WriteEventedEntity* entity;
    std::map<size_t, ReadEventedEntity>::iterator readEntity;
    std::map<std::string, std::map<size_t, ReadEventedEntity> >::iterator cache;
    
    while (addQueue.size())
    {
        addEntity = addQueue.front();
        entity = entities.get(addEntity->handle);
        if (entity)
        {
            // trigger event
            PropertyCollection meta;
            ReadEventedEntity _entity(entity);
            trigger(addEntity->eventName.c_str(), meta, _entity);
            
            // update selByProp caches
            for (cache=readCacheByProp.begin(); cache!=readCacheByProp.end(); ++cache)
            {
                if (entity->has(cache->first.c_str()))
                {
                    readEntity = cache->second.find(entity->getHandle());
                    if (readEntity == cache->second.end())
                    {
                        cache->second[entity->getHandle()] = ReadEventedEntity(entity);
                    }
                }
            }
        }
        addQueue.pop();
    }
}

TaskRunnerLogic(EventedTemplateTaskExec)
{
    EventedTemplateTask* task = data->toUnsafePtr<EventedTemplateTask>();
    task->selection = (*task->event.selector)(manager, task->collection, *task->meta, *task->current);
}

TaskRunnerLogic(EventedOperationTaskExec)
{
    EventedOperationTask* task = data->toUnsafePtr<EventedOperationTask>();
    for (std::list<EventedOperationTaskFunc>::iterator i=task->operations.begin(); i!=task->operations.end(); ++i)
        if (!task->target->getDead()) (*(*i).operation)(manager, task->collection, *(*i).meta, task->target);
}

TaskRunnerLogic(EventedSyncTaskExec)
{
    EventedSyncTask* task = data->toUnsafePtr<EventedSyncTask>();
    task->changed = task->target->sync();
}

bool EventedCollection::_handleEventQueue()
{
    if (eventQueue.size() == 0) return false;

    // get generic task executer 
    TaskRunner* tasks = manager->request<TaskRunner>();
    
    // create objects added outside dispatch
    _flushAddQueue();
    
    // rebuild cache on-demand
    if (readCache.size() != entities.total()) _updateCache();
    
    Any* data;
    size_t processedEvents = eventQueue.size();
    
    // build tasks from event queue
    
    for (std::list<EventedEvent*>::iterator e=eventQueue.begin(); e!=eventQueue.end(); ++e)
    {
        // events to process
        EventedEvent* event = (*e);
        std::list<EventedTemplate> events = triggers[event->eventId];
        if (events.size())
        {
            for (std::list<EventedTemplate>::iterator i=events.begin(); i!=events.end(); ++i)
            {
                EventedTemplateTask task;
                task.event = *i;
                task.meta = &event->meta;
                task.collection = this;
                task.current = &readCache;
                
                data = tasks->write();
                *data = task;
            }
        }
    }
    
    // kickoff processing
    tasks->run(EventedTemplateTaskExec);
    
    // convert into incidents
    std::map<WriteEventedEntity*,std::list<EventedOperationTaskFunc> > incidents;
    data = tasks->result();
    while (data)
    {
        EventedTemplateTask* task = data->toUnsafePtr<EventedTemplateTask>();
        for (std::list<ReadEventedEntity>::iterator i=task->selection.begin(); i!=task->selection.end(); ++i)
        {
            incidents[i->entity].push_back({
                task->meta,
                task->event.operation
            });
        }
        data = tasks->result();
    }
    
    // build operations by entity
    for (std::map<WriteEventedEntity*,std::list<EventedOperationTaskFunc> >::iterator i=incidents.begin(); i!=incidents.end(); ++i)
    {
        EventedOperationTask task;
        task.target = i->first;
        task.collection = this;
        task.operations = i->second;
        
        data = tasks->write();
        *data = task;
    }
    
    // kickoff processing
    tasks->run(EventedOperationTaskExec);
    
    // convert into syncs
    std::list<WriteEventedEntity*> changed;
    data = tasks->result();
    while (data)
    {
        EventedOperationTask* task = data->toUnsafePtr<EventedOperationTask>();
        changed.push_back(task->target);
        data = tasks->result();
    }
    
    // build syncs
    for (std::list<WriteEventedEntity*>::iterator i=changed.begin(); i!=changed.end(); ++i)
    {
        EventedSyncTask task;
        task.target = *i;
        
        data = tasks->write();
        *data = task;
    }
    
    // kickoff processing
    tasks->run(EventedSyncTaskExec);
    
    // update caching
    std::map<size_t, ReadEventedEntity>::iterator readEntity;
    std::map<std::string, std::map<size_t, ReadEventedEntity> >::iterator cache;
    data = tasks->result();
    while (data)
    {
        EventedSyncTask* task = data->toUnsafePtr<EventedSyncTask>();
        
        // update selByProp caches
        for (std::list<std::string>::iterator i=task->changed.begin(); i!=task->changed.end(); ++i)
        {
            cache = readCacheByProp.find(*i);
            if (cache != readCacheByProp.end())
            {
                readEntity = cache->second.find(task->target->getHandle());
                if (readEntity == cache->second.end())
                {
                    cache->second[task->target->getHandle()] = ReadEventedEntity(task->target);
                }
            }
        }
        
        data = tasks->result();
    }
    
    // drop entities marked as dead
    WriteEventedEntity* entity;
    for (std::list<size_t>::iterator i=handleCache.begin(); i!=handleCache.end(); ++i)
    {
        entity = entities.get(*i);
        if (entity && entity->getDead())
        {
            // if we have removed an entity update selByProp caches
            std::list<std::string> props = entity->getProperties()->keys();
            for (std::list<std::string>::iterator p=props.begin(); p!=props.end(); ++p)
            {
                cache = readCacheByProp.find(*p);
                if (cache != readCacheByProp.end())
                {
                    cache->second.erase(entity->getHandle());
                }
            }
            entities.remove(*i);
        }
    }
    
    // dump processed events
    while (processedEvents)
    {
        delete eventQueue.front();
        eventQueue.pop_front();
        --processedEvents;
    }
    
    // return remaining events
    return eventQueue.size();
}

// process any queued events
void EventedCollection::dispatch()
{
    // event generators
    for (std::list<EventedDispatch*>::iterator i=dispatches.begin(); i!=dispatches.end(); ++i)
        (*i)->dispatch(this);
    
    // crunch through queue
    int iterations = 0;
    while (_handleEventQueue() && iterations++ < 10);
    
    // make sure readCache is current
    _updateCache();
}

// built-in selections
EventedSelector(EventedCollection::selEmpty)
{
    return {};
}
EventedSelector(EventedCollection::selByHandle)
{
    std::list<ReadEventedEntity> result;
    size_t handle = meta.get<size_t>("handle");
    
    ReadEventedEntity entity = collection->get(handle);
    if (entity.valid()) result.push_back(entity);
    
    return result;
}

// built-in operations
EventedOperation(EventedCollection::opCopyAllFromMeta)
{
    target->copyAllFrom(&meta);
}


