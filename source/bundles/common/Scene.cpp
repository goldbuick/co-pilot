
#include <bundles/sdl/LibSDL.h>
#include <bundles/common/Scene.h>
#include <bundles/render/GLRenderPipe.h>

using namespace copilot;

SceneResult::SceneResult(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name), result(0) { }

SceneResult::~SceneResult() { }

uint8_t SceneResult::getResult()
{
    return result;
}
void SceneResult::setResult(uint8_t inResult)
{
    result = inResult;
}

Scene::Scene(CoreLogicManager* managerChain)
{
    // disposable logics
    manager = new CoreLogicManager(managerChain);
    manager->request<GLRenderPipe>();
    manager->request<SceneResult>();
    manager->request<EventedCollection>();
}

Scene::~Scene()
{
    delete manager;
}

// mix-ins
uint8_t Scene::getResult(CoreLogicManager* scene)
{
    return scene->request<SceneResult>()->getResult();
}
void Scene::setResult(CoreLogicManager* scene, uint8_t result)
{
    return scene->request<SceneResult>()->setResult(result);
}

// run a scene and get a return code
uint8_t Scene::run(float renderFPS)
{
    // logics
    LibSDL* sdl = manager->request<LibSDL>();
    GLRenderPipe* render = manager->request<GLRenderPipe>();
    SceneResult* sceneResult = manager->request<SceneResult>();
    EventedCollection* entities = manager->request<EventedCollection>();
    
    // init scene
    init();
    
    // fixed timestep
    uint64_t lastTime = sdl->getTime();
    float timestep = 1.f / renderFPS;
    
    while (sdl->active() && sceneResult->getResult() == 0)
    {
        // process external events
        entities->dispatch();
        
        // render dom to windows
        if (sdl->getDeltaTime(sdl->getTime(), lastTime) >= timestep)
        {
            lastTime = sdl->getTime();
            render->dispatch(entities);
        }
    
        // prevent CPU swamp
        SDL_Delay(1);
    }
    
    return sceneResult->getResult();
}

// run a single iteration
void Scene::render()
{
    GLRenderPipe* render = manager->request<GLRenderPipe>();
    EventedCollection* entities = manager->request<EventedCollection>();
    // render dom to windows
    render->dispatch(entities);
}
bool Scene::step(uint8_t& result)
{
    // logics
    SceneResult* sceneResult = manager->request<SceneResult>();
    EventedCollection* entities = manager->request<EventedCollection>();
    
    if (sceneResult->getResult() == 0)
    {
        // process external events
        entities->dispatch();
        
        // not done yet!
        return false;
    }

    result = sceneResult->getResult();
    return true;
}


SceneManager::SceneManager(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name), current(NULL) { }

SceneManager::~SceneManager() { }

// loop until exit state is reached
void SceneManager::run(uint8_t start, float renderFPS)
{
    uint8_t result = start;
    do {
        current = next(result);
        if (current)
        {
            result = current->run(renderFPS);
            delete current;
        }
        else
        {
            result = 0;
        }
    } while (result != 0);
}

// run a single iteration
void SceneManager::render()
{
    if (current) current->render();
}
void SceneManager::step(uint8_t start)
{
    if (!current)
    {
        current = next(start);
        if (current) current->init();
    }
    
    uint8_t result;
    if (current->step(result))
    {
        delete current;
        current = next(result);
        if (current) current->init();
    }
}

