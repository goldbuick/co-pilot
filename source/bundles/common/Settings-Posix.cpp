

#include <bundles/common/Settings.h>

#include <stdlib.h>
#include <unistd.h>

using namespace copilot;

Settings::Settings(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name) { }

Settings::~Settings() { }

SettingsArgs Settings::parse(int argc, char * argv[])
{
    SettingsArgs args;
    
    for (int i=0; i<argc; ++i)
        printf("arg: %i, %s\n", i, argv[i]);
    
	int arg;
	while ((arg = getopt(argc, argv, "p:")) != -1)
    {
        switch (arg)
        {
            case 'p':
                args.pubPath = optarg;
                break;
            case '?':
                if (optopt == 'p')
                {
                    // log that p requires a path value
                }
                else
                {
                    // log options
                }
                break;
        }
    }
    
    return args;
}
