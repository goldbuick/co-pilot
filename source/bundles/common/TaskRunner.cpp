
#include <bundles/sdl/LibSDL.h>
#include <bundles/common/TaskRunner.h>

namespace copilot
{
    
    int TaskRunnerExecThread(TaskRunner* runner)
    {
        while (runner->active())
            runner->process();
        return 0;
    }
    
    struct TaskRunnerState
    {
        int cores;
        int started;
        SDL_cond* cond;
        SDL_mutex* mutex;
        SDL_Thread* threads[32];
    };
    
}

using namespace copilot;

TaskRunner::TaskRunner(CoreLogicManager* inManager, const char* name)
: CoreLogic(inManager, name), dead(false), halt(true), readIndex(0), writeIndex(0), processIndex(0)
{
    state = new TaskRunnerState;
    state->started = 0;
    state->cond = SDL_CreateCond();
    state->mutex = SDL_CreateMutex();

    // pre-alloc block of 32
    for (int i=0; i<32; ++i) tasks.push_back(new TaskRunnerTask);
    
    state->cores = SDL_GetCPUCount() << 1;
    for (int i=0; i<state->cores; ++i)
        state->threads[i] = SDL_CreateThread((SDL_ThreadFunction)&TaskRunnerExecThread, "TaskRunnerExecThread", this);
}

TaskRunner::~TaskRunner()
{
    SDL_LockMutex(state->mutex);
    
    dead = true;
    SDL_CondBroadcast(state->cond);
    
    SDL_UnlockMutex(state->mutex);

    for (int i=0; i<state->cores; ++i)
        SDL_WaitThread(state->threads[i], NULL);
    
    SDL_DestroyCond(state->cond);
    SDL_DestroyMutex(state->mutex);
    delete state;
    
    for (std::vector<TaskRunnerTask*>::iterator i=tasks.begin(); i!=tasks.end(); ++i)
        delete *i;
}

// get a task to setup
Any* TaskRunner::write()
{
    // allocate blocks tasks
    while (writeIndex >= tasks.size())
        for (int i=0; i<32; ++i) tasks.push_back(new TaskRunnerTask);
    
    int index = writeIndex++;
    TaskRunnerTask* task = tasks[index];
    task->complete = false;
    
    return &task->data;
}

// execute all tasks
void TaskRunner::run(TaskRunnerFunc inOperation)
{
    operation = inOperation;
    readIndex = processIndex = 0;
    if (writeIndex == 0) return;
    
    SDL_LockMutex(state->mutex);
    halt = false;
    SDL_CondBroadcast(state->cond);
    SDL_UnlockMutex(state->mutex);
}

// get a task that has been processed
Any* TaskRunner::result()
{
    SDL_LockMutex(state->mutex);
    
    if (dead || writeIndex == 0 || readIndex >= writeIndex)
    {
        // reset
        halt = true;
        writeIndex = 0;
        SDL_UnlockMutex(state->mutex);
        return NULL;
    }
    
    int index = readIndex++;
    TaskRunnerTask* task = tasks[index];
    while (!task->complete)
        SDL_CondWait(state->cond, state->mutex);
    
    SDL_UnlockMutex(state->mutex);
    return &task->data;
}

// idle until run
bool TaskRunner::active()
{
    SDL_LockMutex(state->mutex);
    SDL_CondBroadcast(state->cond);
    SDL_UnlockMutex(state->mutex);
    return !dead;
}

// get a task to process
void TaskRunner::process()
{
    SDL_LockMutex(state->mutex);

    // wait for run, or on done
    while (!dead && (halt || writeIndex == 0 || processIndex >= writeIndex))
        SDL_CondWait(state->cond, state->mutex);

    // bail if dead or no tasks
    if (dead)
    {
        SDL_UnlockMutex(state->mutex);
        return;
    }
    
    int index = processIndex++;
    
    TaskRunnerTask* task = tasks[index];
    SDL_UnlockMutex(state->mutex);
    
    (*operation)(manager, &task->data);
    
    SDL_LockMutex(state->mutex);
    task->complete = true;
    SDL_CondBroadcast(state->cond);
    SDL_UnlockMutex(state->mutex);
}

