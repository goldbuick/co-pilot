
#include <bundles/common/Settings.h>
#include <bundles/files/AssetManager.h>
#include <bundles/sdl/LibSDLThreads.h>

namespace copilot
{
    struct AssetManagerState
    {
        SDLThreadLock* threadLock;
    };
}

using namespace copilot;

AssetManager::AssetManager(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
    state = new AssetManagerState;
    state->threadLock = new SDLThreadLock();
}

AssetManager::~AssetManager()
{
    delete state->threadLock;
    delete state;
    
    for (std::map<std::string, Asset*>::iterator i=assets.begin(); i!=assets.end(); ++i)
        delete i->second;
}

// config pub path
void AssetManager::config(int argc, char * argv[])
{
    Settings* settings = manager->request<Settings>();
    copilot::FileAccess* fileAccess = manager->request<copilot::FileAccess>();
    
    SettingsArgs args = settings->parse(argc, argv);
    args.pubPath.length() ? fileAccess->setDirAbsolute(args.pubPath.c_str()) : fileAccess->setDirRelative();
}

void AssetManager::lock()
{
    state->threadLock->lock();
}

void AssetManager::unlock()
{
    state->threadLock->unlock();
}

std::string AssetManager::assetPath(const char* resource, const char* extension)
{
    std::string path = resource;
    path += ".";
    path += extension;
	
	// store for later!
	resources[path] = resource;
	
	// return result
    return path;
}

FileBuffer* AssetManager::assetBuffer(const char* resource, const char* extension, bool isText)
{
    FileAccess* fileAccess = manager->request<FileAccess>();
    return fileAccess->bufferFromAppOrPub(resource, extension, isText);
}

Asset* AssetManager::assetRequest(const char* resource, const char* extension)
{
    std::string path = assetPath(resource, extension);
    std::map<std::string, Asset*>::iterator i = assets.find(path);
    if (i == assets.end()) return NULL;
    return i->second;
}
