
#include <bundles/files/DiskWatcher.h>
#include <bundles/files/FileAccess.h>

#import <Foundation/Foundation.h>
#import <CoreServices/CoreServices.h>

namespace copilot
{
    struct DiskWatcherState
    {
        FSEventStreamRef stream;
    };
}

using namespace copilot;

void fsevents_callback(ConstFSEventStreamRef streamRef, void *userData, size_t numEvents, void *eventPaths, const FSEventStreamEventFlags eventFlags[], const FSEventStreamEventId eventIds[])
{
    ((DiskWatcher*)userData)->checkFiles();
}

DiskWatcher::DiskWatcher(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
    state = new DiskWatcherState();
    
	NSArray* paths = [NSArray arrayWithObject:[NSString stringWithFormat:@"%s",manager->request<FileAccess>()->pathForPublic()]];
	
	void* diskWatcherPtr = (void *)this;
    
	NSTimeInterval latency = 1.0;
	FSEventStreamContext context = {0, diskWatcherPtr, NULL, NULL, NULL};
	
    state->stream = FSEventStreamCreate(NULL, &fsevents_callback, &context, (__bridge CFArrayRef)paths, kFSEventStreamEventIdSinceNow, (CFAbsoluteTime)latency, kFSEventStreamCreateFlagUseCFTypes);
    FSEventStreamScheduleWithRunLoop(state->stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
    FSEventStreamStart(state->stream);
}

DiskWatcher::~DiskWatcher()
{
	FSEventStreamStop(state->stream);
    FSEventStreamInvalidate(state->stream);
    delete state;
}
