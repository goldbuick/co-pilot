
#include <bundles/files/DiskWatcher.h>
#include <bundles/files/FileAccess.h>

using namespace copilot;

uint16_t DiskWatcher::getNewPollId()
{
    return changedFiles.getNewPollId();
}

bool DiskWatcher::getChangedFile(uint16_t pollId, FileChanged& fileChanged)
{
    return changedFiles.getEntry(pollId, fileChanged);
}

void DiskWatcher::checkFiles()
{
	time_t timestamp;
    for (std::list<FileChanged>::iterator i=tracking.begin(); i!=tracking.end(); ++i)
    {
        timestamp = manager->request<FileAccess>()->timestamp(i->filename.c_str());
        if (timestamp != i->timestamp)
        {
            i->timestamp = timestamp;
            changedFiles.addEntry((*i));
        }
    }
}

void DiskWatcher::trackResource(const char* resource, const char* extension)
{
    FileChanged file;
    file.filename = manager->request<FileAccess>()->pathForPublicResource(resource, extension);
    file.timestamp = manager->request<FileAccess>()->timestamp(file.filename.c_str());
    tracking.push_back(file);
}
