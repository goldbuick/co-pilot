
#include <bundles/files/FileAccess.h>

//#import <AppKit/AppKit.h>
#include <sys/param.h>
#import <Foundation/Foundation.h>

using namespace copilot;

// set current working directory
void FileAccess::setDirRelative()
{
    char parentdir[MAXPATHLEN];
    CFURLRef url = CFBundleCopyBundleURL(CFBundleGetMainBundle());
    CFURLRef url2 = CFURLCreateCopyDeletingLastPathComponent(0, url);
    if (CFURLGetFileSystemRepresentation(url2, true, (UInt8 *)parentdir, MAXPATHLEN))
    {
        chdir(parentdir);
    }
    CFRelease(url);
    CFRelease(url2);
}
void FileAccess::setDirAbsolute(const char* path)
{
    chdir(path);
}

// helper function to make sure the required directories exist
void FileAccess::createDirectories(const char* path)
{
    [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%s", path]
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
}

// path for editor project directories
const char* FileAccess::pathForPublic()
{
    // check current path for /pub/
    NSFileManager* fm = [NSFileManager defaultManager];
    
    // look for /pub/
    NSString* path = [NSString stringWithFormat:@"%@/pub/", [fm currentDirectoryPath]];
    
    BOOL isDir;
    BOOL exists = [fm fileExistsAtPath:path isDirectory:&isDir];
    if (exists && isDir) return [path UTF8String];
    
    path = [NSSearchPathForDirectoriesInDomains(NSDesktopDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [[NSString stringWithFormat:@"%@/pub/", path] UTF8String];
}

// abstraction to allow reading from disk or bundled resource

// is text added a null to the end of the buffer
FileBuffer* FileAccess::bufferFromApp(const char* resource, const char* extension, bool isText)
{
	// find path to resource
	NSString* _path = [NSString stringWithFormat:@"%@/bundled-resources/%s.%s", [[NSBundle mainBundle] resourcePath], resource, extension];
	return bufferFromDisk([_path UTF8String], isText);
}

// list of public resources
std::list<std::string> FileAccess::listOfPublicResources(const char* extension)
{
	std::list<std::string> resources;
	
    // result file list
    NSMutableArray* fileList = [[NSMutableArray alloc] init];
    
    // solve by stack instead of recursion
    NSMutableArray* stack = [[NSMutableArray alloc] init];
    NSFileManager* manager = [NSFileManager defaultManager];
    
    // start with /pub/
    NSString* root = [NSString stringWithFormat:@"%s", pathForPublic()];
    [stack addObject:root];
    while (stack.count)
    {
        // stack pop
        NSString* path = [stack lastObject];
        [stack removeLastObject];
        
        // iterate through files
        for (NSString* file in [manager contentsOfDirectoryAtPath:path
                                                            error:nil])
        {
            // get full file path
            NSString* filePath = [path stringByAppendingPathComponent:file];
            
            // add it to list
            [fileList addObject:filePath];
            
            // stack push
            BOOL isDir;
            [manager fileExistsAtPath:filePath isDirectory:&isDir];
            if (isDir) [stack addObject:filePath];
        }
    }
    
    // only return matching files
    NSString* ext = [NSString stringWithFormat:@"%s", extension];
    for (NSString* file in fileList)
    {
        if ([file hasSuffix:ext])
            resources.push_back([[file substringFromIndex:root.length-1] UTF8String]);
    }
	
	return resources;
}
