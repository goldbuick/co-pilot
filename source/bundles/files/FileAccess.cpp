
#include <bundles/files/FileAccess.h>

#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/stat.h>

using namespace copilot;

FileAccess::FileAccess(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name) { }

FileAccess::~FileAccess() { }


// a directory for public content, human editable

// translate resource path into a public path
const char *FileAccess::pathForPublicResource(const char* resource, const char* extension)
{
	static char strPathForPublicResource[2048];
	snprintf(strPathForPublicResource, 2048, "%s%s.%s", pathForPublic(), resource, extension);
	return strPathForPublicResource;
}


// write a block of data to disk
void FileAccess::bufferToPublic(const char* resource, const char* extension, FileBuffer* buffer)
{
	bufferToDisk(pathForPublicResource(resource, extension), buffer);
}


// return a timestamp of a given file
time_t FileAccess::timestamp(const char* path)
{
	struct stat attrib;
	if (stat(path, &attrib) == 0)
	{
		return attrib.st_mtime;
	}
	return 0;
}

// make this a single call that breaks the path
// up into interesting parts ..
FilePathInfo FileAccess::pathInfo(const char* path)
{
    FilePathInfo result;
    
    std::string fullPath = path;
    
    // get fullPath without extension
    std::string fullPathNoExtension;
    size_t pos = fullPath.find_last_of(".");
    if (pos == std::string::npos)
    {
        fullPathNoExtension = fullPath;
        result.extension = "";
        result.remainder = "";
    }
    else
    {
        fullPathNoExtension = fullPath.substr(0, pos);
        std::string extensionWithRemainder = fullPath.substr(pos+1);
        pos = extensionWithRemainder.find_first_of("/");
        if (pos == std::string::npos)
        {
            result.extension = extensionWithRemainder;
            result.remainder = "";
        }
        else
        {
            result.extension = extensionWithRemainder.substr(0, pos);
            result.remainder = extensionWithRemainder.substr(pos);
        }
    }
    
    // get fullPathNoExtension without filename
    pos = fullPathNoExtension.find_last_of("/");
    if (pos == std::string::npos)
    {
        result.path = "";
        result.filename = fullPathNoExtension;
    }
    else
    {
        result.path = fullPathNoExtension.substr(0, pos);
        result.filename = fullPathNoExtension.substr(pos+1);
    }
    
    result.pathFilename = result.path + "/" + result.filename;
    result.fullPath = result.pathFilename + "." + result.extension;
    
    // extension always lowercase
    std::transform(result.extension.begin(),
                   result.extension.end(),
                   result.extension.begin(),
                   ::tolower);
    
    return result;
}

// attempts to parse a net path
bool FileAccess::netPathInfo(const char* path, NetPathInfo* result)
{
    // need dest data
    if (result == NULL)
        return false;
    
    // /net/10.0.1.27:23423/files/png/
    std::string fullPath = path;
    
    // if there is no net prefix bail
    if (fullPath.find("/net/") != 0)
        return false;
    
    // see if there is an address
    size_t colonPos = fullPath.find(":");
    
    // if there isn't bail
    if (colonPos == std::string::npos)
        return false;
    
    // capture address
    result->address = fullPath.substr(5, colonPos - 5);
    
    // find next slash
    size_t slashPos = fullPath.find("/", colonPos);
    
    // if there isn't bail
    if (slashPos == std::string::npos)
        return false;
    
    // capture port
    result->port = fullPath.substr(colonPos + 1, slashPos - colonPos - 1);
    
    // capture remote path
    result->remotePath = fullPath.substr(slashPos);
    
    // create fullAddress
    result->fullAddress = fullPath.substr(5, slashPos - 5);
    
    return true;
}

// load from given file path
FileBuffer* FileAccess::bufferFromDisk(const char* path, bool isText)
{
	// open file
	FILE* fp = fopen(path, "rb");
	if (fp)
	{
		// track timestamp
		time_t time = timestamp(path);
		
		// how much data
		fseek(fp, 0, SEEK_END);
		long length = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		
		// add one extra byte for null
		if (isText) ++length;
		
		// read data
		unsigned char* data = new unsigned char[length];
		fread(data, sizeof(unsigned char), length, fp);
		
		// null terminated string
		if (isText) data[length-1] = NULL;
		
		// close file
		fclose(fp);
		
		// return buffer
		return new FileBuffer(data, time, length);
	}
    else if (path)
    {
        //App::error("failed to open file: %s\n", path);
    }
	
	// failure
	return NULL;
}

// first try app, then fallback to public
FileBuffer* FileAccess::bufferFromAppOrPub(const char* resource, const char* extension, bool isText)
{
    FileBuffer* buffer = bufferFromApp(resource, extension, isText);
    if (buffer) return buffer;
    return bufferFromDisk(pathForPublicResource(resource, extension), isText);
}

// save to given file path
bool FileAccess::bufferToDisk(const char* path, FileBuffer* buffer)
{
	// make sure we have a spot to write
	createDirectories(path);
	
	// open file
	FILE* fp = fopen(path, "wb");
	if (fp)
	{
		size_t written = fwrite(buffer->getData(), buffer->getLength(), sizeof(char), fp);
		fclose(fp);
		
		return (written == buffer->getLength());
	}
	
	return false;
}


