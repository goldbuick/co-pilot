
#include <bundles/files/assetTypes/Images.h>

#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

using namespace copilot;

glm::vec4 Image::get(glm::vec2 position)
{
    glm::vec4 color(0.f);
    
    if (position.x >= 0.f && position.x < size.x &&
        position.y >= 0.f && position.y < size.y)
    {
        size_t offset = (position.x + position.y * size.x) * 4;
        color.r = (float)pixels[offset] / 255.f;
        color.g = (float)pixels[offset+1] / 255.f;
        color.b = (float)pixels[offset+2] / 255.f;
        color.a = (float)pixels[offset+3] / 255.f;
    }
    
    return color;
}

void Image::set(glm::vec2 position, glm::vec4 color)
{
    if (position.x >= 0.f && position.x < size.x &&
        position.y >= 0.f && position.y < size.y)
    {
        size_t offset = (position.x + position.y * size.x) * 4;
        pixels[offset] = glm::clamp(color.r * 255.f, 0.f, 255.f);
        pixels[offset+1] = glm::clamp(color.g * 255.f, 0.f, 255.f);
        pixels[offset+2] = glm::clamp(color.b * 255.f, 0.f, 255.f);
        pixels[offset+3] = glm::clamp(color.a * 255.f, 0.f, 255.f);
    }
}


Images::Images(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name) { }

Images::~Images() { }

// check to see if image is valid
bool Images::valid(Image image)
{
    return image.size.x != 0 && image.size.y != 0;
}

// create an empty image of given size
Image Images::createImage(glm::vec2 size)
{
    Image image;
    image.size = size;
    image.pixels.resize(image.size.x * image.size.y, 0);
    return image;
}

// load an image from disk
Image Images::loadImage(const char* filename)
{
    Image image;
    image.size = glm::vec2(0.f);
    
    int width, height, n;
    unsigned char* data = stbi_load(filename, &width, &height, &n, 4);
    if (data)
    {
        image.size.x = width;
        image.size.y = height;
        int total = width * height * 4;
        image.pixels.reserve(total);
        for (int i=0; i<total; ++i)
        {
            image.pixels.push_back(data[i]);
        }
        
        delete[] data;
    }
    
    
    return image;
}

// load an image from given buffer
Image Images::loadImageFromBuffer(const unsigned char* buffer, size_t length)
{
    Image image;
    image.size = glm::vec2(0.f);
    
    int width, height, n;
    unsigned char* data = stbi_load_from_memory(buffer, (int)length, &width, &height, &n, 4);
    if (data)
    {
        image.size.x = width;
        image.size.y = height;
        int total = width * height * 4;
        image.pixels.reserve(total);
        for (int i=0; i<total; ++i)
            image.pixels.push_back(data[i]);
        
        delete[] data;
    }
    
    return image;
}

// load an image from given alpha map
Image Images::createImageFromAlpha(const unsigned char* alphaImage, glm::vec2 size)
{
    Image image;
    
    image.size = size;
    image.pixels.reserve(image.size.x * image.size.y * 4);
    
    // offset
    int source = 0;
    for (int y=0; y<image.size.y; ++y)
    {
        for (int x=0; x<image.size.x; ++x)
        {
            image.pixels.push_back(alphaImage[source] ? 255 : 0);
            image.pixels.push_back(alphaImage[source] ? 255 : 0);
            image.pixels.push_back(alphaImage[source] ? 255 : 0);
            image.pixels.push_back(alphaImage[source++]);
        }
    }
    
    return image;
}

// create an image from given color
Image Images::createImageFromColor(glm::vec3 color, glm::vec2 size)
{
    Image image;
    
    image.size = size;
    image.pixels.reserve(image.size.x * image.size.y * 4);
    
    for (int y=0; y<image.size.y; ++y)
    {
        for (int x=0; x<image.size.x; ++x)
        {
            image.pixels.push_back(255 * color.r);
            image.pixels.push_back(255 * color.g);
            image.pixels.push_back(255 * color.b);
            image.pixels.push_back(255);
        }
    }
    
    return image;
}

// write an image to disk
bool Images::saveImage(Image image, const char* filename)
{
    return stbi_write_png(filename, image.size.x, image.size.y, 4, image.pixels.data(), 0);
}

