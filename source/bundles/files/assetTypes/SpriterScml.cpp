
#include <bundles/files/FileAccess.h>
#include <bundles/files/assetTypes/SpriterScml.h>

#include "tinyxml2.h"

namespace copilot
{
    
    
    class SpriterScmlLoader : public AssetManagerLoader
    {
    public:
        SpriterScmlLoader(CoreLogicManager* manager, const char* name) : AssetManagerLoader(manager, name) { }
        const char* getExtension(AssetManager* assetManager) { return "scml"; }
        Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension)
        {
            SpriterScml* scml = manager->request<SpriterScml>();
            
            SpriterScmlRef* spriterScmlRef = new SpriterScmlRef;
            spriterScmlRef->ref = new Spriter::Scml;
            
            FileBuffer* buffer = assetManager->assetBuffer(resource, extension, false);
            if (buffer)
            {
                *spriterScmlRef->ref = scml->loadScmlFromBuffer(buffer->getText(), buffer->getLength());
                delete buffer;
            }
            else
            {
                
            }
            
            return spriterScmlRef;
        }
        void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef)
        {
        }
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<SpriterScmlRef>(CoreLogicManager* manager)
    {
        return manager->request<SpriterScmlLoader>();
    }
}

using namespace copilot;

SpriterScml::SpriterScml(CoreLogicManager* manager, const char* name) : CoreLogic(manager, name) { }

// load scml from disk
Spriter::Scml SpriterScml::loadScml(const char* filename)
{
    Spriter::Scml doc;
    
    FileBuffer* buffer = manager->request<FileAccess>()->bufferFromDisk(filename);
    if (buffer)
    {
        doc = loadScmlFromBuffer(buffer->getText(), buffer->getLength());
        delete buffer;
    }
    
    return doc;
}

// xml handlers
bool strequals(const char* a, const char* b)
{
    if (a == NULL) return false;
    return strcmp(a, b) == 0;
}

void parseFile(Spriter::Folder& folder, tinyxml2::XMLElement* node)
{
    Spriter::File file;
    
    if (node->QueryIntAttribute("id", &file.id) != tinyxml2::XML_SUCCESS) file.id = 0;
    if (node->Attribute("name")) file.name = node->Attribute("name");
    if (node->QueryFloatAttribute("width", &file.size.x) != tinyxml2::XML_SUCCESS) file.size.x = 0.f;
    if (node->QueryFloatAttribute("height", &file.size.y) != tinyxml2::XML_SUCCESS) file.size.y = 0.f;
    if (node->QueryFloatAttribute("pivot_x", &file.pivot.x) != tinyxml2::XML_SUCCESS) file.pivot.x = 0.f;
    if (node->QueryFloatAttribute("pivot_y", &file.pivot.y) != tinyxml2::XML_SUCCESS) file.pivot.y = 1.f;
    
    folder.files.push_back(file);
}

void parseFolder(Spriter::Scml& doc, tinyxml2::XMLElement* node)
{
    Spriter::Folder folder;
    
    if (node->QueryIntAttribute("id", &folder.id) != tinyxml2::XML_SUCCESS) folder.id = 0;
    if (node->Attribute("name")) folder.name = node->Attribute("name");
    
    tinyxml2::XMLElement* child = node->FirstChildElement();
    while (child)
    {
        if (strequals(child->Name(), "file")) parseFile(folder, child);
        child = child->NextSiblingElement();
    }
    
    doc.folders.push_back(folder);
}

void parseObjectInfoSpriteFrame(Spriter::ObjectInfoSprite& sprite, tinyxml2::XMLElement* node)
{
    Spriter::ObjectInfoFrame frame;
    
    if (node->QueryIntAttribute("file", &frame.file) != tinyxml2::XML_SUCCESS) frame.file = 0;
    if (node->QueryIntAttribute("folder", &frame.folder) != tinyxml2::XML_SUCCESS) frame.folder = 0;
    
    sprite.frames.push_back(frame);
}

void parseObjectInfo(Spriter::Entity& entity, tinyxml2::XMLElement* node)
{
    if (strequals(node->Attribute("type"), "sprite"))
    {
        Spriter::ObjectInfoSprite sprite;
        
        if (node->Attribute("name")) sprite.name = node->Attribute("name");
        
        tinyxml2::XMLElement* child = node->FirstChildElement();
        if (child)
        {
            child = child->FirstChildElement();
            while (child)
            {
                if (strequals(child->Name(), "i")) parseObjectInfoSpriteFrame(sprite, child);
                child = child->NextSiblingElement();
            }
        }
        
        entity.objectSprites.push_back(sprite);
    }
    else if (strequals(node->Attribute("type"), "bone"))
    {
        Spriter::ObjectInfoBone bone;
        
        if (node->Attribute("name")) bone.name = node->Attribute("name");
        if (node->QueryFloatAttribute("w", &bone.size.x) != tinyxml2::XML_SUCCESS) bone.size.x = 0.f;
        if (node->QueryFloatAttribute("h", &bone.size.y) != tinyxml2::XML_SUCCESS) bone.size.y = 0.f;
        
        entity.objectBones.push_back(bone);
    }
}

void parseCharacterMapInstruction(Spriter::CharacterMap& charMap, tinyxml2::XMLElement* node)
{
    Spriter::MapInstruction mapInstruction;
    
    if (node->QueryIntAttribute("file", &mapInstruction.file) != tinyxml2::XML_SUCCESS) mapInstruction.file = 0;
    if (node->QueryIntAttribute("folder", &mapInstruction.folder) != tinyxml2::XML_SUCCESS) mapInstruction.folder = 0;
    if (node->QueryIntAttribute("target_file", &mapInstruction.targetFile) != tinyxml2::XML_SUCCESS) mapInstruction.targetFile = -1;
    if (node->QueryIntAttribute("target_folder", &mapInstruction.targetFolder) != tinyxml2::XML_SUCCESS) mapInstruction.targetFolder = -1;
    
    charMap.mapInstructions.push_back(mapInstruction);
}

void parseCharacterMap(Spriter::Entity& entity, tinyxml2::XMLElement* node)
{
    Spriter::CharacterMap charMap;
    
    if (node->QueryIntAttribute("id", &charMap.id) != tinyxml2::XML_SUCCESS) charMap.id = 0;
    if (node->Attribute("name")) charMap.name = node->Attribute("name");
    
    tinyxml2::XMLElement* child = node->FirstChildElement();
    while (child)
    {
        if (strequals(child->Name(), "map")) parseCharacterMapInstruction(charMap, child);
        child = child->NextSiblingElement();
    }
    
    entity.characterMaps.push_back(charMap);
}

void parseBoneRef(Spriter::MainlineKey& mainlineKey, tinyxml2::XMLElement* node)
{
    Spriter::BoneRef boneRef;
    
    if (node->QueryIntAttribute("id", &boneRef.id) != tinyxml2::XML_SUCCESS) boneRef.id = 0;
    if (node->QueryIntAttribute("key", &boneRef.key) != tinyxml2::XML_SUCCESS) boneRef.key = 0;
    if (node->QueryIntAttribute("parent", &boneRef.parent) != tinyxml2::XML_SUCCESS) boneRef.parent = 0;
    if (node->QueryIntAttribute("timeline", &boneRef.timeline) != tinyxml2::XML_SUCCESS) boneRef.timeline = 0;
    
    mainlineKey.boneRefs.push_back(boneRef);
}

void parseObjectRef(Spriter::Animation& animation, Spriter::MainlineKey& mainlineKey, tinyxml2::XMLElement* node)
{
    Spriter::ObjectRef objectRef;
    
    if (node->QueryIntAttribute("id", &objectRef.id) != tinyxml2::XML_SUCCESS) objectRef.id = 0;
    if (node->QueryIntAttribute("key", &objectRef.key) != tinyxml2::XML_SUCCESS) objectRef.key = 0;
    if (node->QueryIntAttribute("parent", &objectRef.parent) != tinyxml2::XML_SUCCESS) objectRef.parent = 0;
    if (node->QueryIntAttribute("timeline", &objectRef.timeline) != tinyxml2::XML_SUCCESS) objectRef.timeline = 0;
    if (node->QueryIntAttribute("z_index", &objectRef.zindex) != tinyxml2::XML_SUCCESS) objectRef.zindex = 0;
    
    // mainline key 0 extra - maybe dump this to a seperate kind of obj ref ?
    if (node->Attribute("name"))
    {
        Spriter::AbsObjectRef absObjectRef;
        
        absObjectRef.id = objectRef.id;
        absObjectRef.key = objectRef.key;
        absObjectRef.parent = objectRef.parent;
        absObjectRef.timeline = objectRef.timeline;
        absObjectRef.zindex = objectRef.zindex;
        
        absObjectRef.name = node->Attribute("name");
        if (node->QueryIntAttribute("file", &absObjectRef.file) != tinyxml2::XML_SUCCESS) absObjectRef.file = 0;
        if (node->QueryIntAttribute("folder", &absObjectRef.folder) != tinyxml2::XML_SUCCESS) absObjectRef.folder = 0;
        if (node->QueryFloatAttribute("abs_a", &absObjectRef.alpha) != tinyxml2::XML_SUCCESS) absObjectRef.alpha = 1.f;
        if (node->QueryFloatAttribute("abs_angle", &absObjectRef.angle) != tinyxml2::XML_SUCCESS) absObjectRef.angle = 0.f;
        if (node->QueryFloatAttribute("abs_x", &absObjectRef.position.x) != tinyxml2::XML_SUCCESS) absObjectRef.position.x = 0.f;
        if (node->QueryFloatAttribute("abs_y", &absObjectRef.position.y) != tinyxml2::XML_SUCCESS) absObjectRef.position.y = 0.f;
        if (node->QueryFloatAttribute("abs_scale_x", &absObjectRef.scale.x) != tinyxml2::XML_SUCCESS) absObjectRef.scale.x = 1.f;
        if (node->QueryFloatAttribute("abs_scale_y", &absObjectRef.scale.y) != tinyxml2::XML_SUCCESS) absObjectRef.scale.y = 1.f;
        if (node->QueryFloatAttribute("abs_pivot_x", &absObjectRef.pivot.x) != tinyxml2::XML_SUCCESS) absObjectRef.pivot.x = 0.f;
        if (node->QueryFloatAttribute("abs_pivot_y", &absObjectRef.pivot.y) != tinyxml2::XML_SUCCESS) absObjectRef.pivot.y = 1.f;
        
        animation.absObjectRefs.push_back(absObjectRef);
    }
    
    mainlineKey.objectRefs.push_back(objectRef);
}

void parseMainlineKey(Spriter::Animation& animation, tinyxml2::XMLElement* node)
{
    Spriter::MainlineKey mainlineKey;
    
    if (node->QueryIntAttribute("id", &mainlineKey.id) != tinyxml2::XML_SUCCESS) mainlineKey.id = 0;
    if (node->QueryFloatAttribute("time", &mainlineKey.time) != tinyxml2::XML_SUCCESS) mainlineKey.time = 0.f;
    
    // convert to seconds
    mainlineKey.time /= 1000.f;
    
    tinyxml2::XMLElement* child = node->FirstChildElement();
    while (child)
    {
        if (strequals(child->Name(), "bone_ref")) parseBoneRef(mainlineKey, child);
        if (strequals(child->Name(), "object_ref")) parseObjectRef(animation, mainlineKey, child);
        child = child->NextSiblingElement();
    }
    
    animation.mainlineKeys.push_back(mainlineKey);
}

void parseTimelineKey(Spriter::SpatialInfo* info, tinyxml2::XMLElement* key, tinyxml2::XMLElement* node)
{
    if (key->QueryFloatAttribute("time", &info->time) != tinyxml2::XML_SUCCESS) info->time = 0.f;
    if (key->QueryIntAttribute("spin", &info->spin) != tinyxml2::XML_SUCCESS) info->spin = 0;
    
    if (strequals(key->Attribute("curve_type"), "instant"))
    {
        info->curveType = Spriter::TimelineKeyCurveInstant;
    }
    else if (strequals(key->Attribute("curve_type"), "quadratic"))
    {
        info->curveType = Spriter::TimelineKeyCurveQuadratic;
    }
    else if (strequals(key->Attribute("curve_type"), "cubic"))
    {
        info->curveType = Spriter::TimelineKeyCurveCubic;
    }
    else if (strequals(key->Attribute("curve_type"), "quartic"))
    {
        info->curveType = Spriter::TimelineKeyCurveQuartic;
    }
    else if (strequals(key->Attribute("curve_type"), "quintic"))
    {
        info->curveType = Spriter::TimelineKeyCurveQuintic;
    }
    else if (strequals(key->Attribute("curve_type"), "bezier"))
    {
        info->curveType = Spriter::TimelineKeyCurveBezier;
    }
    else
    {
        info->curveType = Spriter::TimelineKeyCurveLinear;
    }
    
    if (key->QueryFloatAttribute("c1", &info->c1) != tinyxml2::XML_SUCCESS) info->c1 = 0.f;
    if (key->QueryFloatAttribute("c2", &info->c2) != tinyxml2::XML_SUCCESS) info->c2 = 0.f;
    if (key->QueryFloatAttribute("c3", &info->c3) != tinyxml2::XML_SUCCESS) info->c3 = 0.f;
    if (key->QueryFloatAttribute("c4", &info->c4) != tinyxml2::XML_SUCCESS) info->c4 = 0.f;
    
    if (node->QueryFloatAttribute("a", &info->alpha) != tinyxml2::XML_SUCCESS) info->alpha = 1.f;
    if (node->QueryFloatAttribute("angle", &info->angle) != tinyxml2::XML_SUCCESS) info->angle = 0.f;
    if (node->QueryFloatAttribute("x", &info->position.x) != tinyxml2::XML_SUCCESS) info->position.x = 0.f;
    if (node->QueryFloatAttribute("y", &info->position.y) != tinyxml2::XML_SUCCESS) info->position.y = 0.f;
    if (node->QueryFloatAttribute("scale_x", &info->scale.x) != tinyxml2::XML_SUCCESS) info->scale.x = 1.f;
    if (node->QueryFloatAttribute("scale_y", &info->scale.y) != tinyxml2::XML_SUCCESS) info->scale.y = 1.f;
    
    // convert to seconds
    info->time /= 1000.f;
}

void parseTimelineBox(Spriter::Timeline& timeline, tinyxml2::XMLElement* key, tinyxml2::XMLElement* node)
{
    Spriter::TimelineKey timelineKey;
    Spriter::BoxTimelineKey boxTimelineKey;
    
    if (key->QueryIntAttribute("id", &timelineKey.id) != tinyxml2::XML_SUCCESS) timelineKey.id = 0;
    parseTimelineKey((Spriter::SpatialInfo*)&boxTimelineKey, key, node);
    
    if (node->QueryFloatAttribute("pivot_x", &boxTimelineKey.pivot.x) != tinyxml2::XML_SUCCESS) boxTimelineKey.pivot.x = 0.f;
    if (node->QueryFloatAttribute("pivot_y", &boxTimelineKey.pivot.y) != tinyxml2::XML_SUCCESS) boxTimelineKey.pivot.y = 1.f;
    
    timelineKey.box.push_back(boxTimelineKey);
    timeline.keys.push_back(timelineKey);
}

void parseTimelinePoint(Spriter::Timeline& timeline, tinyxml2::XMLElement* key, tinyxml2::XMLElement* node)
{
    Spriter::TimelineKey timelineKey;
    Spriter::PointTimelineKey pointTimelineKey;
    
    if (key->QueryIntAttribute("id", &timelineKey.id) != tinyxml2::XML_SUCCESS) timelineKey.id = 0;
    parseTimelineKey((Spriter::SpatialInfo*)&pointTimelineKey, key, node);
    
    timelineKey.point.push_back(pointTimelineKey);
    timeline.keys.push_back(timelineKey);
}

void parseTimelineBone(Spriter::Timeline& timeline, tinyxml2::XMLElement* key, tinyxml2::XMLElement* node)
{
    Spriter::TimelineKey timelineKey;
    Spriter::BoneTimelineKey boneTimelineKey;
    
    if (key->QueryIntAttribute("id", &timelineKey.id) != tinyxml2::XML_SUCCESS) timelineKey.id = 0;
    parseTimelineKey((Spriter::SpatialInfo*)&boneTimelineKey, key, node);
    
    timelineKey.bone.push_back(boneTimelineKey);
    timeline.keys.push_back(timelineKey);
}

void parseTimelineObject(Spriter::Timeline& timeline, tinyxml2::XMLElement* key, tinyxml2::XMLElement* node)
{
    Spriter::TimelineKey timelineKey;
    Spriter::ObjectTimelineKey objectTimelineKey;
    
    if (key->QueryIntAttribute("id", &timelineKey.id) != tinyxml2::XML_SUCCESS) timelineKey.id = 0;
    parseTimelineKey((Spriter::SpatialInfo*)&objectTimelineKey, key, node);
    
    if (node->QueryIntAttribute("file", &objectTimelineKey.file) != tinyxml2::XML_SUCCESS) objectTimelineKey.file = 0;
    if (node->QueryIntAttribute("folder", &objectTimelineKey.folder) != tinyxml2::XML_SUCCESS) objectTimelineKey.folder = 0;

    objectTimelineKey.useDefaultPivot = (node->Attribute("pivot_x") == NULL && node->Attribute("pivot_y") == NULL);
    if (node->QueryFloatAttribute("pivot_x", &objectTimelineKey.pivot.x) != tinyxml2::XML_SUCCESS) objectTimelineKey.pivot.x = 0.f;
    if (node->QueryFloatAttribute("pivot_y", &objectTimelineKey.pivot.y) != tinyxml2::XML_SUCCESS) objectTimelineKey.pivot.y = 1.f;
    
    timelineKey.object.push_back(objectTimelineKey);
    timeline.keys.push_back(timelineKey);
}

void parseTimeline(Spriter::Animation& animation, tinyxml2::XMLElement* node)
{
    Spriter::Timeline timeline;
    
    if (node->QueryIntAttribute("id", &timeline.id) != tinyxml2::XML_SUCCESS) timeline.id = 0;
    if (node->QueryIntAttribute("obj", &timeline.obj) != tinyxml2::XML_SUCCESS) timeline.obj = 0;
    if (node->Attribute("name")) timeline.name = node->Attribute("name");
    
    if (strequals(node->Attribute("object_type"), "box"))
    {
        timeline.type = Spriter::TimelineBox;
    }
    else if (strequals(node->Attribute("object_type"), "bone"))
    {
        timeline.type = Spriter::TimelineBone;
    }
    else if (strequals(node->Attribute("object_type"), "point"))
    {
        timeline.type = Spriter::TimelinePoint;
    }
    else
    {
        timeline.type = Spriter::TimelineObject;
    }
    
    tinyxml2::XMLElement* child = node->FirstChildElement();
    while (child)
    {
        if (strequals(child->Name(), "key"))
        {
            tinyxml2::XMLElement* child2 = child->FirstChildElement();
            if (child2)
            {
                switch (timeline.type) {
                    case Spriter::TimelineBox: parseTimelineBox(timeline, child, child2); break;
                    case Spriter::TimelineBone: parseTimelineBone(timeline, child, child2); break;
                    case Spriter::TimelinePoint: parseTimelinePoint(timeline, child, child2); break;
                    case Spriter::TimelineObject: parseTimelineObject(timeline, child, child2); break;
                }
            }
        }
        child = child->NextSiblingElement();
    }
    
    animation.timelines.push_back(timeline);
}

void parseAnimation(Spriter::Entity& entity, tinyxml2::XMLElement* node)
{
    Spriter::Animation animation;
    
    if (node->QueryIntAttribute("id", &animation.id) != tinyxml2::XML_SUCCESS) animation.id = 0;
    if (node->Attribute("name")) animation.name = node->Attribute("name");
    if (node->QueryFloatAttribute("length", &animation.length) != tinyxml2::XML_SUCCESS) animation.length = 0.f;
    if (node->QueryBoolAttribute("looping", &animation.looping) != tinyxml2::XML_SUCCESS) animation.looping = false;
    
    // convert to seconds
    animation.length /= 1000.f;
    
    tinyxml2::XMLElement* child = node->FirstChildElement();
    while (child)
    {
        if (strequals(child->Name(), "mainline"))
        {
            tinyxml2::XMLElement* child2 = child->FirstChildElement();
            while (child2)
            {
                if (strequals(child2->Name(), "key")) parseMainlineKey(animation, child2);
                child2 = child2->NextSiblingElement();
            }
        }
        if (strequals(child->Name(), "timeline")) parseTimeline(animation, child);
        child = child->NextSiblingElement();
    }
    
    entity.animations.push_back(animation);
}

void parseEntity(Spriter::Scml& doc, tinyxml2::XMLElement* node)
{
    Spriter::Entity entity;
    
    if (node->QueryIntAttribute("id", &entity.id) != tinyxml2::XML_SUCCESS) entity.id = 0;
    if (node->Attribute("name")) entity.name = node->Attribute("name");
    
    tinyxml2::XMLElement* child = node->FirstChildElement();
    while (child)
    {
        if (strequals(child->Name(), "obj_info")) parseObjectInfo(entity, child);
        if (strequals(child->Name(), "character_map")) parseCharacterMap(entity, child);
        if (strequals(child->Name(), "animation")) parseAnimation(entity, child);
        child = child->NextSiblingElement();
    }
    
    doc.entities.push_back(entity);
}

// load scml from given buffer
Spriter::Scml SpriterScml::loadScmlFromBuffer(const char* buffer, size_t length)
{
    Spriter::Scml doc;
    
    tinyxml2::XMLDocument xmlDoc;
    if (xmlDoc.Parse(buffer, length) == tinyxml2::XML_SUCCESS && xmlDoc.FirstChildElement())
    {
        tinyxml2::XMLElement* child = xmlDoc.FirstChildElement()->FirstChildElement();
        while (child)
        {
            if (strequals(child->Name(), "folder")) parseFolder(doc, child);
            if (strequals(child->Name(), "entity")) parseEntity(doc, child);
            child = child->NextSiblingElement();
        }
    }
    
    return doc;
}
