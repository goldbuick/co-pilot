
#include <bundles/sdl/LibSDLThreads.h>
#include <bundles/physics/Collide2D.h>
#include <core/common/PropertyCollection.h>

namespace copilot
{
    struct Collide2DThreading
    {
        SDLThreadLock safe;
    };
}

using namespace copilot;

Collide2D::Collide2D(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
    space = cpSpaceNew();
    cpSpaceSetUserData(space, this);
    threading = new Collide2DThreading();
}

Collide2D::~Collide2D()
{
    cpSpaceFree(space);
    delete threading;
}

size_t Collide2D::addShape(cpShape* shape)
{
    threading->safe.lock();
    cpBody* body = cpBodyNew(INFINITY, INFINITY);
    cpShapeSetBody(shape, body);
    cpSpaceAddBody(space, body);
    cpSpaceAddShape(space, shape);
    size_t handle = shapes.add(shape);
    threading->safe.unlock();
    
    return handle;
}
void Collide2D::removeShape(size_t handle)
{
    cpShape* shape = shapes.get(handle);
    if (!shape) return;
    
    threading->safe.lock();
    cpSpaceRemoveBody(space, cpShapeGetBody(shape));
    cpSpaceRemoveShape(space, shape);
    cpShapeFree(shape);
    shapes.remove(handle, false);
    threading->safe.unlock();
}
cpShape* Collide2D::getShape(size_t handle)
{
    return shapes.get(handle);
}
size_t Collide2D::getShapeHandle(size_t index)
{
    return shapes.handle(index);
}
size_t Collide2D::getShapeHandle(cpShape* shape)
{
    return shapes.handle(shape);
}
size_t Collide2D::totalShapes()
{
    return shapes.total();
}


CollisionFlags Collide2D::getFlags(size_t handle)
{
    CollisionFlags flags;
    std::map<size_t, CollisionFlags>::iterator i=shapeFlags.find(handle);
    if (i != shapeFlags.end())
    {
        flags = i->second;
    }
    else
    {
        // set defaults
        flags.oneSided.enabled = false;
        shapeFlags[handle] = flags;
    }
    return flags;
}
void Collide2D::setFlags(size_t handle, CollisionFlags flags)
{
    shapeFlags[handle] = flags;
}

void Collide2D::update(size_t handle, glm::vec2 position, float angle)
{
    cpShape* shape = shapes.get(handle);
    if (!shape) return;
    
    threading->safe.lock();
    cpBody* body = cpShapeGetBody(shape);
    cpBodySetPos(body, cpv(position.x, position.y));
    cpBodySetAngle(body, angle);
    cpSpaceReindexShapesForBody(space, body);
    threading->safe.unlock();
}

static void collideIgnoreCallback(cpShape *collideShape, cpContactPointSet *contacts, CollisionIgnores* ignores)
{
    CollisionFlags flags;
    
    size_t shape = ignores->collide2D->getShapeHandle(collideShape);
    flags = ignores->collide2D->getFlags(shape);
    
    if (!flags.oneSided.enabled)
    {
        // not oneSided we don't care
        return;
    }
    
    ignores->shapes[shape] = true;
}

static void collideCallback(cpShape *collideShape, cpContactPointSet *contacts, Collision2DSet* set)
{
    Collision2D collide;
    CollisionFlags flags;
    
    collide.shape = set->collide2D->getShapeHandle(collideShape);
    
    // do we ignore ?
    if (set->ignores.find(collide.shape) != set->ignores.end())
    {
        return;
    }
    
    // validate collision
    flags = set->collide2D->getFlags(collide.shape);
    if (flags.oneSided.enabled && glm::dot(set->direction, flags.oneSided.normal) < 0.f)
    {
        return;
    }
    
    collide.contacts.count = contacts->count;
    for (int i=0; i<contacts->count; ++i)
        collide.contacts.points[i] = contacts->points[i];
    set->collisions.push_back(collide);
}

std::list<Collision2D> Collide2D::query(std::map<size_t, bool> ignores, cpBody* body, cpShape* shape, glm::vec2 position, float angle, glm::vec2 direction)
{
    // update position
    cpBodySetPos(body, cpv(position.x, position.y));
    cpBodySetAngle(body, angle);
    
    // query collisions
    Collision2DSet set;
    set.collide2D = this;
    set.ignores = ignores;
    set.direction = direction;
    
    threading->safe.lock();
    cpSpaceShapeQuery(space, shape, (cpSpaceShapeQueryFunc)collideCallback, &set);
    threading->safe.unlock();
    
    return set.collisions;
}

static void segmentCollideCallback(cpShape *collideShape, cpFloat t, cpVect n, Collision2DSet* set)
{
    Collision2D collide;
    CollisionFlags flags;
    
    collide.shape = set->collide2D->getShapeHandle(collideShape);
    
    // do we ignore ?
    if (set->ignores.find(collide.shape) != set->ignores.end())
    {
        return;
    }
    
    // validate collision
    flags = set->collide2D->getFlags(collide.shape);
    if (flags.oneSided.enabled && glm::dot(set->direction, flags.oneSided.normal) < 0.f)
    {
        return;
    }
    
    // store the contact with the line
    glm::vec2 point = set->start + set->direction * (float)t;
    collide.contacts.points[0].point = cpv(point.x, point.y);
    collide.contacts.points[0].normal = n;
    collide.contacts.points[0].dist = 0.f;
    collide.contacts.count = 1;
    
    set->collisions.push_back(collide);
}


std::list<Collision2D> Collide2D::segmentCollide(std::map<size_t, bool> ignores, glm::vec2 start, glm::vec2 end, cpLayers layers, cpGroup group)
{
    // query collisions
    Collision2DSet set;
    set.collide2D = this;
    set.direction = glm::normalize(end - start);
    
    threading->safe.lock();
    cpSpaceSegmentQuery(space, cpv(start.x, start.y), cpv(end.x, end.y), layers, group, (cpSpaceSegmentQueryFunc)segmentCollideCallback, &set);
    threading->safe.unlock();
    
    return set.collisions;
}

std::list<Collision2D> Collide2D::collide(size_t handle, glm::vec2 position, float angle, glm::vec2 direction)
{
    std::list<Collision2D> collisions;
    cpShape* shape = shapes.get(handle);
    if (!shape) return collisions;
    
    // cache
    cpBody* body = cpShapeGetBody(shape);
    cpVect pos = cpBodyGetPos(body);
    cpFloat a = cpBodyGetAngle(body);
    
    // query
    CollisionIgnores ignores;
    collisions = query(ignores.shapes, body, shape, position, angle, direction);
    
    // reset
    cpBodySetPos(body, pos);
    cpBodySetAngle(body, a);
    
    return collisions;
}

std::list<Collision2D> Collide2D::sweptCollide(size_t handle, glm::vec2 position, float angle, glm::vec2& velocity, glm::vec2& dist)
{
    std::list<Collision2D> collisions;
    cpShape* shape = shapes.get(handle);
    if (!shape || (velocity.x == 0.f && velocity.y == 0.f)) return collisions;
    
    // cache
    cpBody* body = cpShapeGetBody(shape);
    cpVect pos = cpBodyGetPos(body);
    cpFloat a = cpBodyGetAngle(body);
    
    float length;
    glm::vec2 step;
    glm::vec2 axis = glm::abs(velocity);
    
    // pick longest axis
    if (axis.x > axis.y)
    {
        length = axis.x;
        step.x = glm::sign(velocity.x);
        step.y = velocity.y / axis.x;
    }
    else
    {
        length = axis.y;
        step.y = glm::sign(velocity.y);
        step.x = velocity.x / axis.y;
    }
    
    // update position
    cpBodySetPos(body, cpv(position.x, position.y));
    cpBodySetAngle(body, angle);
    
    // query ignores
    CollisionIgnores ignores;
    ignores.collide2D = this;
    
    threading->safe.lock();
    cpSpaceShapeQuery(space, shape, (cpSpaceShapeQueryFunc)collideIgnoreCallback, &ignores);
    threading->safe.unlock();

    float prev, current = 0.f;
    glm::vec2 remain, stepx(0.f), stepy(0.f);

    while (current < length)
    {
        prev = current;
        current += 1.f;
        if (current > length) current = length;
        
        glm::vec2 newPosition = position + (current * step);
        
        collisions = query(ignores.shapes, body, shape, newPosition, angle, step);
        if (collisions.size())
        {
            // step back one
            current = prev;
            
            // set distance
            dist = current * step;
            
            // check for slide
            newPosition = position + (current * step);
            
            // get remainder velocity
            remain = step * (length - current);
            
            // check for x slide
            if (velocity.x)
            {
                stepx.x = glm::clamp(remain.x, -1.f, 1.f);
                if (query(ignores.shapes, body, shape, newPosition + stepx, angle, stepx).size() == 0)
                {
                    velocity.x = remain.x;
                }
                else
                {
                    velocity.x = 0.f;
                }
            }
            
            // check for y slide
            if (velocity.y)
            {
                stepy.y = glm::clamp(remain.y, -1.f, 1.f);
                if (query(ignores.shapes, body, shape, newPosition + stepy, angle, stepy).size() == 0)
                {
                    velocity.y = remain.y;
                }
                else
                {
                    velocity.y = 0.f;
                }
            }
            
            // hit on perfect corner
            if (velocity.x != 0.f && velocity.y != 0.f)
            {
                velocity.x = velocity.y = 0.f;
            }
            
            // reset
            cpBodySetPos(body, pos);
            cpBodySetAngle(body, a);
            return collisions;
        }
    }
    
    // made it all the way!
    dist = velocity;
    velocity = glm::vec2(0.f);
    
    // reset
    cpBodySetPos(body, pos);
    cpBodySetAngle(body, a);
    return collisions;
}

std::list<Collision2D> Collide2D::platformerCollide(size_t handle, glm::vec2 position, float angle, glm::vec2& velocity, glm::vec2& dist, glm::vec2& collide)
{
    std::list<Collision2D> collisions;
    cpShape* shape = shapes.get(handle);
    if (!shape || (velocity.x == 0.f && velocity.y == 0.f)) return collisions;
    
    // cache
    cpBody* body = cpShapeGetBody(shape);
    cpVect pos = cpBodyGetPos(body);
    cpFloat a = cpBodyGetAngle(body);
    
    // update position
    cpBodySetPos(body, cpv(position.x, position.y));
    cpBodySetAngle(body, angle);
    
    // query ignores
    CollisionIgnores ignores;
    ignores.collide2D = this;
    
    threading->safe.lock();
    cpSpaceShapeQuery(space, shape, (cpSpaceShapeQueryFunc)collideIgnoreCallback, &ignores);
    threading->safe.unlock();
    
    // starting position
    glm::vec2 newPosition;
    glm::vec2 start = position;
    
    // sweep vars
    glm::vec2 step;
    float length, prev, current;
    
    // sweep
    if (velocity.x)
    {
        step = glm::vec2(glm::sign(velocity.x), 0.f);
        length = glm::abs(velocity.x);
    }
    else
    {
        step = glm::vec2(0.f, glm::sign(velocity.y));
        length = glm::abs(velocity.y);
    }
    
    current = 0.f;
    while (current < length)
    {
        prev = current;
        current += 1.f;
        if (current > length) current = length;
        
        newPosition = position + (current * step);
        
        collisions = query(ignores.shapes, body, shape, newPosition, angle, step);
        if (collisions.size())
        {
            bool didCollide = true;
            
            if (step.x)
            {
                for (int i=1; i<3; ++i)
                {
                    // slope up
                    glm::vec2 slopeCheck(0.f, -i);
                    
                    // step up
                    if (query(ignores.shapes, body, shape, newPosition + slopeCheck, angle, step).size() == 0)
                    {
                        didCollide = false;
                        position += slopeCheck;
                        break;
                    }
                }
            }
            
            if (step.y)
            {
                glm::vec2 slopeCheck(-1.f, 0.f);
                
                // step left
                if (query(ignores.shapes, body, shape, newPosition + slopeCheck, angle, step).size() == 0)
                {
                    didCollide = false;
                    position += slopeCheck;
                }
                else
                {
                    // step right
                    slopeCheck.x = 1.f;
                    if (query(ignores.shapes, body, shape, newPosition + slopeCheck, angle, step).size() == 0)
                    {
                        didCollide = false;
                        position += slopeCheck;
                    }
                }
            }
            
            if (didCollide)
            {
                // step back one
                current = prev;
                
                // check for slopes
                newPosition = position + (current * step);
                
                // set distance
                dist = newPosition - start;
                
                // clip velocity
                if (step.x)
                {
                    velocity.x = 0.f;
                    collide = glm::vec2(1.f, 0.f);
                }
                else
                {
                    velocity.y = 0.f;
                    collide = glm::vec2(0.f, 1.f);
                }
                
                // reset
                cpBodySetPos(body, pos);
                cpBodySetAngle(body, a);
                return collisions;
            }
        }
        else if (step.x)
        {
            // check for downward snap
            float shouldSnap = 0.f;
            
            for (int i=1; i<3; ++i)
            {
                // slope up
                glm::vec2 slopeCheck(0.f, i);
                
                // step up
                if (query(ignores.shapes, body, shape, newPosition + slopeCheck, angle, step).size())
                {
                    if (i > 1) shouldSnap = i;
                    break;
                }
            }
            
            if (shouldSnap)
            {
                position.y += shouldSnap;
            }
        }
    }
    // made it all the way!
    
    // set distance
    dist = newPosition - start;
    
    // clip velocity
    if (step.x)
    {
        velocity.x = 0.f;
    }
    else
    {
        velocity.y = 0.f;
    }
    collide = glm::vec2(0.f);
    
    // reset
    cpBodySetPos(body, pos);
    cpBodySetAngle(body, a);
    return collisions;
}

