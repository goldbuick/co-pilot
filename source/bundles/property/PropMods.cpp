
#include <bundles/property/PropMods.h>

using namespace copilot;

// A -> B, every 6 seconds
void PropMods::cooldownInit(PropertyCollection& props, const char* name)
{
    props.strSet("cooldown-%s", 0.f, name);
}
float PropMods::cooldownValue(WriteEventedEntity* entity, const char* name)
{
    return entity->strGet<float>("cooldown-%s", name);
}
void PropMods::cooldownTrigger(WriteEventedEntity* entity, const char* name, float next)
{
    entity->strSet("cooldown-%s", next, name);
}
bool PropMods::cooldownUpdate(WriteEventedEntity* entity, const char* name, float delta)
{
    float timer = entity->strGet<float>("cooldown-%s", name) - delta;
    entity->strSet("cooldown-%s", timer, name);
    return (timer < 0.f);
}
