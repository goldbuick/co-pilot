
#include <bundles/render/GLDraw.h>
#include <bundles/files/FileAccess.h>
#include <bundles/files/AssetManager.h>
#include <bundles/files/assetTypes/Images.h>
#include <core/common/PropertyCollection.h>
#include <core/common/glm/gtc/noise.hpp>

#ifdef MOBILE_RENDER
    #include <SDL2/SDL_opengles2.h>
#else
    #include <SDL2/SDL_opengl.h>
#endif

namespace copilot
{
    
    template<> void PropertyCollectionInit(GLTextureRef*& value) { value->ref = NULL; }
    template<> void PropertyCollectionInit(GLShaderRef*& value) { value->ref = NULL; }
    
    class GLTextureLoader : public AssetManagerLoader
    {
    public:
        GLTextureLoader(CoreLogicManager* manager, const char* name) : AssetManagerLoader(manager, name) { }
        const char* getExtension(AssetManager* assetManager) { return "png"; }
        Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension)
        {
            GLDraw* draw = manager->request<GLDraw>();
            Images* images = manager->request<Images>();
            
            // r,g,b
            const float scale = 2.f;
            glm::vec2 macroSize(16.f);
            glm::vec2 defaultSize = macroSize * scale;
            std::vector<std::string> components = strSplit(resource, ",");
            
            Image image;
            
            // generated
            if (components.size() == 3)
            {
                glm::vec3 color(atof(components[0].c_str()), atof(components[1].c_str()), atof(components[2].c_str()));
                image = images->createImageFromColor(color, defaultSize);
                
                glm::vec2 position;
                for (position.y=0; position.y<image.size.y; ++position.y)
                    for (position.x=0; position.x<image.size.x; ++position.x)
                    {
                        const float amp = 0.1973;
                        const float ratio = 11.0237f;
                        glm::vec4 value = image.get(position);
                        glm::vec3 _value = value.swizzle(glm::comp::R, glm::comp::G, glm::comp::B);
                        _value -= glm::perlin(position * ratio) * amp * 0.25f;
                        _value -= glm::perlin(glm::round(position / scale) * ratio) * amp;
                        image.set(position, glm::vec4(_value, 1.f));
                    }
            }
            else
            {
                FileBuffer* buffer = assetManager->assetBuffer(resource, extension, false);
                if (buffer)
                {
                    image = images->loadImageFromBuffer(buffer->getData(), buffer->getLength());
                }
                else
                {
                    image = images->createImageFromColor(glm::vec3(1.f), defaultSize);
                }
            }
            
            GLTextureRef* textureRef = new GLTextureRef;
            textureRef->ref = new GLTexture;
            *textureRef->ref = draw->compileTexture(image.size, image.pixels.data(), image.pixels.size());
            
            return textureRef;
        }
        void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef)
        {
        }
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLTextureRef>(CoreLogicManager* manager)
    {
        return manager->request<GLTextureLoader>();
    }
    
    class GLShaderVertLoader : public AssetManagerLoader
    {
    public:
        GLShaderVertLoader(CoreLogicManager* manager, const char* name) : AssetManagerLoader(manager, name) { }
        const char* getExtension(AssetManager* assetManager) { return "vert"; }
        Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension)
        {
            GLShaderVertRef* shaderVertRef = new GLShaderVertRef;
            shaderVertRef->ref = new GLShaderVert;
            
            FileBuffer* buffer = assetManager->assetBuffer(resource, extension, true);
            if (!buffer)
            {
                shaderVertRef->ref->content =
                "// required for opengl es 2.0\n"
                "#ifdef GL_ES\n"
                "precision mediump float;\n"
                "#endif\n"
                "\n"
                "// variable length inputs\n"
                "attribute vec4 attrVertex;\n"
                "attribute vec2 attrTexCoord0;\n"
                "attribute vec4 attrColors;\n"
                "\n"
                "// single unit inputs\n"
                "uniform mat4 uniProjection;\n"
                "uniform mat4 uniTransform;\n"
                "\n"
                "// variable length outputs\n"
                "varying vec2 varTexCoord0;\n"
                "varying vec4 varColor;\n"
                "\n"
                "void main()\n"
                "{\n"
                "\n"
                "	// take vertex and multiply against projection matrix\n"
                "	gl_Position = uniProjection * uniTransform * attrVertex;\n"
                "\n"
                "	// simply copy texture coord\n"
                "	varTexCoord0 = attrTexCoord0;\n"
                "\n"
                "	// simply copy color\n"
                "	varColor = attrColors;\n"
                "\n"
                "}\n";
            }
            else
            {
                shaderVertRef->ref->content = buffer->getText();
                delete buffer;
            }
            
            return shaderVertRef;
        }
        void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef)
        {
        }
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLShaderVertRef>(CoreLogicManager* manager)
    {
        return manager->request<GLShaderVertLoader>();
    }
    
    class GLShaderFragLoader : public AssetManagerLoader
    {
    public:
        GLShaderFragLoader(CoreLogicManager* manager, const char* name) : AssetManagerLoader(manager, name) { }
        const char* getExtension(AssetManager* assetManager) { return "frag"; }
        Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension)
        {
            GLShaderFragRef* shaderFragRef = new GLShaderFragRef;
            shaderFragRef->ref = new GLShaderFrag;
            
            FileBuffer* buffer = assetManager->assetBuffer(resource, extension, true);
            if (!buffer)
            {
                shaderFragRef->ref->content =
                "// required for opengl es 2.0\n"
                "#ifdef GL_ES\n"
                "precision mediump float;\n"
                "#endif\n"
                "\n"
                "// single unit inputs\n"
                "uniform sampler2D uniTex0;\n"
                "\n"
                "// variable length outputs from vert shader\n"
                "varying vec2 varTexCoord0;\n"
                "varying vec4 varColor;\n"
                "\n"
                "void main()\n"
                "{\n"
                "\n"
                "	// result color is pulled from texture and multiplied with the given color\n"
                "	gl_FragColor = texture2D(uniTex0, varTexCoord0) * varColor;\n"
                "\n"
                "}\n";
            }
            else
            {
                shaderFragRef->ref->content = buffer->getText();
                delete buffer;
            }    
            
            return shaderFragRef;
        }
        void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef)
        {
        }
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLShaderFragRef>(CoreLogicManager* manager)
    {
        return manager->request<GLShaderFragLoader>();
    }
    
    class GLShaderLoader : public AssetManagerLoader
    {
    public:
        GLShaderLoader(CoreLogicManager* manager, const char* name) : AssetManagerLoader(manager, name) { }
        const char* getExtension(AssetManager* assetManager) { return "shader"; }
        Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension)
        {
            std::vector<std::string> files = strSplit(resource, ",");
            if (files.size() != 2) return NULL;
            
            GLShaderRef* shaderRef = new GLShaderRef;
            
            shaderRef->ref = new GLShader;
            shaderRef->vert = assetManager->request<GLShaderVertRef>(files[0].c_str());
            shaderRef->frag = assetManager->request<GLShaderFragRef>(files[1].c_str());
            
            assetManager->watch<GLShaderRef, GLShaderVertRef>(resource, files[0].c_str());
            assetManager->watch<GLShaderRef, GLShaderFragRef>(resource, files[1].c_str());
            
            return shaderRef;
        }
        void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef)
        {
            //need to add watchers for both vert & frag files
            GLDraw* draw = manager->request<GLDraw>();
            
            GLShaderRef* shaderRef = (GLShaderRef*)watchingRef;
            
            if (shaderRef->vert->ref && shaderRef->vert->ref->content.length() > 1 &&
                shaderRef->frag->ref && shaderRef->frag->ref->content.length() > 1)
            {
                *shaderRef->ref = draw->compileShader(shaderRef->vert->ref->content.c_str(),
                                                      shaderRef->frag->ref->content.c_str());
            }
        }
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLShaderRef>(CoreLogicManager* manager)
    {
        return manager->request<GLShaderLoader>();
    }
    
}

using namespace copilot;

GLDraw::GLDraw(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
}

GLDraw::~GLDraw() { }

void GLDraw::config(glm::vec2 size)
{
	glViewport(0, 0, size.x, size.y);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
}

void GLDraw::clear(glm::vec3 color)
{
    glClearColor(color.x, color.y, color.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GLDraw::clippingOn(glm::rect rect)
{
	glEnable(GL_SCISSOR_TEST);
    glScissor(rect.getLeft(), rect.getTop(), rect.getWidth(), rect.getHeight());
}
void GLDraw::clippingOff()
{
    glDisable(GL_SCISSOR_TEST);    
}

int GLDraw::nextPowerOfTwo(int size)
{
	size = size - 1;
	size = size | (size >>  1);
	size = size | (size >>  2);
	size = size | (size >>  4);
	size = size | (size >>  8);
	size = size | (size >> 16);
	return size + 1;
}

GLRenderTriIndices GLDraw::compileTriIndices()
{
    GLRenderTriIndices tri;
    for (int i=0; i<GLRenderMaxTri; ++i)
    {
        int index = i * 3;
        int value = i * 3;
        tri.indices[index+0] = value + 0;
        tri.indices[index+1] = value + 1;
        tri.indices[index+2] = value + 2;
    }
    return tri;
}
GLRenderQuadIndices GLDraw::compileQuadIndices()
{
    GLRenderQuadIndices quad;
    for (int i=0; i<GLRenderMaxQuad; ++i)
    {
        int index = i * 6;
        int value = i * 4;
        quad.indices[index+0] = value + 0;
        quad.indices[index+1] = value + 1;
        quad.indices[index+2] = value + 2;
        quad.indices[index+3] = value + 2;
        quad.indices[index+4] = value + 1;
        quad.indices[index+5] = value + 3;
    }
    return quad;
}
GLTexture GLDraw::compileTexture(glm::vec2 size, const unsigned char* buffer, size_t bufferSize)
{
    GLTexture texture;
    
    texture.handle = 0;
    texture.size = size;
    texture.linear = false;
    texture.repeatX = false;
    texture.repeatY = false;
    texture.uv = glm::vec2(1.f);
    
	glGenTextures(1, &texture.handle);
    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.handle);
    
	if (texture.linear)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
	}
    
	if (texture.repeatX && texture.repeatY)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
    else if (texture.repeatX)
    {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    else if (texture.repeatY)
    {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
    
    if (size.x * size.y * 4 == bufferSize)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture.size.x, texture.size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    }

    return texture;
}
GLShader GLDraw::compileShader(const char* vertex, const char* fragment)
{
    GLShader shader;
    shader.handle = 0;
    
    if (strlen(vertex) == 0 || strlen(fragment) == 0) return shader;
    
    GLint status;
    GLint log_len;
    GLuint vsh, fsh;
    const GLchar* vsh_ptr = (GLchar*)vertex;
    const GLchar* fsh_ptr = (GLchar*)fragment;
    
    shader.handle = glCreateProgram();
    
    vsh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vsh, 1, &vsh_ptr, NULL);
    glCompileShader(vsh);
    
    glGetShaderiv(vsh, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        glGetShaderiv(vsh, GL_INFO_LOG_LENGTH, &log_len);
        if (log_len > 0)
        {
            char* log = new char[log_len];
            glGetShaderInfoLog(vsh, log_len, &log_len, log);
            printf("shader vertex:\n%s\n", log);
            delete[] log;
        }
        glDeleteShader(vsh);
        glDeleteProgram(shader.handle);
        shader.handle = 0;
        return shader;
    }
    
    fsh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fsh, 1, &fsh_ptr, NULL);
    glCompileShader(fsh);
    
    glGetShaderiv(fsh, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        glGetShaderiv(fsh, GL_INFO_LOG_LENGTH, &log_len);
        if (log_len > 0)
        {
            char* log = new char[log_len];
            glGetShaderInfoLog(fsh, log_len, &log_len, log);
            printf("shader fragment:\n%s\n", log);
            delete[] log;
        }
        glDeleteShader(vsh);
        glDeleteShader(fsh);
        glDeleteProgram(shader.handle);
        shader.handle = 0;
        return shader;
    }
    
    glAttachShader(shader.handle, vsh);
    glAttachShader(shader.handle, fsh);
    
    glBindAttribLocation(shader.handle, 0, "attrVertex");
    glBindAttribLocation(shader.handle, 1, "attrTexCoord0");
    glBindAttribLocation(shader.handle, 2, "attrColors");
    
    glLinkProgram(shader.handle);
    
    glGetProgramiv(shader.handle, GL_LINK_STATUS, &status);
    if (status == 0)
    {
        glGetProgramiv(shader.handle, GL_INFO_LOG_LENGTH, &log_len);
        if (log_len > 0)
        {
            char* log = new char[log_len];
            glGetProgramInfoLog(shader.handle, log_len, &log_len, log);
            printf("shader program:\n%s\n", log);
            delete[] log;
        }
        glDeleteProgram(shader.handle);
        shader.handle = 0;
        return shader;
    }
    
    shader.attrVertex = glGetAttribLocation(shader.handle, "attrVertex");
    shader.attrTexCoord0 = glGetAttribLocation(shader.handle, "attrTexCoord0");
    shader.attrColors = glGetAttribLocation(shader.handle, "attrColors");
    
    char texName[32];
    strncpy(texName, "uniTex0", 32);
    for (int i=0; i<GLShaderMaxTextures; ++i)
    {
        texName[6] = '0' + i;
        shader.uniTex[i] = glGetUniformLocation(shader.handle, texName);
    }
    
    shader.uniProjection = glGetUniformLocation(shader.handle, "uniProjection");
    shader.uniTransform = glGetUniformLocation(shader.handle, "uniTransform");
    
    return shader;
}

// quick draw texture with custom uv coords
// assume we're passing in 4 colors (bl, tl, tr, br)
GLRenderQuad GLDraw::compileQuadTiled(GLTexture texture, glm::vec2 center, glm::vec2 size,
                                      glm::vec2* uvOffset, glm::vec2 *anchor, glm::vec4** colors, glm::mat4* transform)
{
    glm::vec2 uv1(0.f);
    glm::vec2 uv2(size.x / texture.size.x, size.y / texture.size.y);
    
    if (uvOffset)
    {
        glm::vec2 uvo(uvOffset->x / texture.size.x, uvOffset->y / texture.size.y);
        uv1 += uvo; uv2 += uvo;
    }
    
    return compileQuad(texture, center, size, anchor, colors, transform, &uv1, &uv2);
}
GLRenderQuad GLDraw::compileQuad(GLTexture texture, glm::vec2 center, glm::vec2 size,
                                 glm::vec2 *anchor, glm::vec4** colors, glm::mat4* transform, glm::vec2* uv1, glm::vec2* uv2)
{
    GLRenderQuad quad;

    static glm::vec2 _uv1(0.f);
    
    if (uv1 == NULL)
    {
        uv1 = &_uv1;
        uv2 = &texture.uv;
    }
    
    if (colors == NULL)
    {
        static glm::vec4 c(1.f, 1.f, 1.f, 1.f);
        static glm::vec4* cs[] = { &c, &c, &c, &c };
        colors = cs;
    }
    
    if (anchor == NULL)
    {
        static glm::vec2 a(0.5f, 0.5f);
        anchor = &a;
    }
    glm::vec2 ia(1.f - anchor->x, 1.f - anchor->y);
    
    glm::vec2 tl, tr, bl, br;
    
    tr.x = br.x = size.x * ia.x;
    tl.x = bl.x = size.x * (ia.x - 1.f);
    
    bl.y = br.y = size.y * ia.y;
    tl.y = tr.y = size.y * (ia.y - 1.f);
    
    if (transform)
    {
        // could probably make this more efficient
        tl = (*transform * glm::vec4(glm::vec3(tl, 0.f), 0.f)).swizzle(glm::X, glm::Y);
        tr = (*transform * glm::vec4(glm::vec3(tr, 0.f), 0.f)).swizzle(glm::X, glm::Y);
        bl = (*transform * glm::vec4(glm::vec3(bl, 0.f), 0.f)).swizzle(glm::X, glm::Y);
        br = (*transform * glm::vec4(glm::vec3(br, 0.f), 0.f)).swizzle(glm::X, glm::Y);
    }
    
    tl += center;
    tr += center;
    bl += center;
    br += center;
    
    quad.tl.vert.z = quad.tr.vert.z =
    quad.bl.vert.z = quad.br.vert.z = 0.f;
    
    quad.tl.vert.x = tl.x;
    quad.tl.vert.y = tl.y;
    quad.tl.tex.u = uv1->x;
    quad.tl.tex.v = uv1->y;
    quad.tl.color.r = colors[1]->x;
    quad.tl.color.g = colors[1]->y;
    quad.tl.color.b = colors[1]->z;
    quad.tl.color.a = colors[1]->w;
    
    quad.tr.vert.x = tr.x;
    quad.tr.vert.y = tr.y;
    quad.tr.tex.u = uv2->x;
    quad.tr.tex.v = uv1->y;
    quad.tr.color.r = colors[2]->x;
    quad.tr.color.g = colors[2]->y;
    quad.tr.color.b = colors[2]->z;
    quad.tr.color.a = colors[2]->w;
    
    quad.bl.vert.x = bl.x;
    quad.bl.vert.y = bl.y;
    quad.bl.tex.u = uv1->x;
    quad.bl.tex.v = uv2->y;
    quad.bl.color.r = colors[0]->x;
    quad.bl.color.g = colors[0]->y;
    quad.bl.color.b = colors[0]->z;
    quad.bl.color.a = colors[0]->w;
    
    quad.br.vert.x = br.x;
    quad.br.vert.y = br.y;
    quad.br.tex.u = uv2->x;
    quad.br.tex.v = uv2->y;
    quad.br.color.r = colors[3]->x;
    quad.br.color.g = colors[3]->y;
    quad.br.color.b = colors[3]->z;
    quad.br.color.a = colors[3]->w;
    
    return quad;
}

void GLDraw::use(GLShader shader,
                 glm::mat4 projection, glm::mat4 transform,
                 GLTexture* textures, uint16_t totalTextures,
                 long offset, int32_t stride)
{
    glUseProgram(shader.handle);
    
    glUniformMatrix4fv(shader.uniProjection, 1, 0, glm::value_ptr(projection));
    glUniformMatrix4fv(shader.uniTransform, 1, 0, glm::value_ptr(transform));
    
    for (int i=0; i<totalTextures; ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, textures[i].handle);
        glUniform1i(shader.uniTex[i], i);
        glActiveTexture(GL_TEXTURE0);
    }
    
    glVertexAttribPointer(shader.attrVertex, 3, GL_FLOAT, 0, stride, (void*)(offset + offsetof(GLRenderStruct,vert)));
    glEnableVertexAttribArray(shader.attrVertex);
    
    glVertexAttribPointer(shader.attrTexCoord0, 2, GL_FLOAT, 0, stride, (void*)(offset + offsetof(GLRenderStruct,tex)));
    glEnableVertexAttribArray(shader.attrTexCoord0);
    
    glVertexAttribPointer(shader.attrColors, 4, GL_FLOAT, 0, stride, (void*)(offset + offsetof(GLRenderStruct,color)));
    glEnableVertexAttribArray(shader.attrColors);
}

void GLDraw::render(GLShader shader,
            glm::mat4 projection, glm::mat4 transform,
            GLTexture* textures, uint16_t totalTextures,
            GLRenderTri* triangles, GLRenderTriIndices* triangleIndices, uint16_t totalTriangles)
{
    use(shader, projection, transform, textures, totalTextures, (long)triangles, sizeof(triangles[0].p1));
    glDrawElements(GL_TRIANGLES, totalTriangles * 3, GL_UNSIGNED_SHORT, triangleIndices->indices);
}

void GLDraw::render(GLShader shader,
            glm::mat4 projection, glm::mat4 transform,
            GLTexture* textures, uint16_t totalTextures,
            GLRenderQuad* quads, GLRenderQuadIndices* quadIndices, uint16_t totalQuads)
{
    use(shader, projection, transform, textures, totalTextures, (long)quads, sizeof(quads[0].tl));
    glDrawElements(GL_TRIANGLES, totalQuads * 6, GL_UNSIGNED_SHORT, quadIndices->indices);
}


