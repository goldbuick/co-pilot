
#include <bundles/render/GLFont.h>
#include <bundles/files/FileAccess.h>
#include <bundles/files/assetTypes/Images.h>
#include <core/common/PropertyCollection.h>

#define STB_FONT_GLYPH_FIRST			32
#define STB_FONT_GLYPH_COUNT			96
#define STB_FONT_GLYPH_MAX_TEXTURE_SIZE	1024
#define STB_TRUETYPE_IMPLEMENTATION

#include "ttf_monaco.h"
#include "stb_truetype.h"

namespace copilot
{
    
    template<> void PropertyCollectionInit(GLFontTexture& value) { value.texture.handle = 0; }
    
    class GLFontTextureLoader : public AssetManagerLoader
    {
    public:
        GLFontTextureLoader(CoreLogicManager* manager, const char* name) : AssetManagerLoader(manager, name) { }
        const char* getExtension(AssetManager* assetManager) { return "ttf"; }
        Asset* getAsset(AssetManager* assetManager, const char* resource, const char* extension)
        {
            GLFont* font = manager->request<GLFont>();
            
            std::vector<std::string> components = strSplit(resource, ",");
            
            int fontHeight = 16;
            if (components.size() > 1)
                fontHeight = atoi(components[1].c_str());
            
            const char* _resource = components[0].c_str();
            
            GLFontTextureRef* fontTextureRef = new GLFontTextureRef;
            fontTextureRef->ref = new GLFontTexture;
            
            // default
            if (strcmp(_resource, "default") == 0)
            {
                *fontTextureRef->ref = font->compileTexture(fontHeight, monacoTTF, monacoLength);
                return fontTextureRef;
            }
            
            FileBuffer* buffer = assetManager->assetBuffer(_resource, extension, false);
            if (!buffer)
            {
                *fontTextureRef->ref = font->compileTexture(fontHeight, monacoTTF, monacoLength);
                return fontTextureRef;
            }
            
            *fontTextureRef->ref = font->compileTexture(fontHeight, buffer->getData(), buffer->getLength());
            return fontTextureRef;
        }
        void updateAsset(AssetManager* assetManager, const char* resource, const char* extension, Asset* loadedRef, Asset* watchingRef)
        {
            // attempt to update asset ?
        }
    };
    
    template<> AssetManagerLoader* AssetManagerLoaderFactory<GLFontTextureRef>(CoreLogicManager* manager)
    {
        return manager->request<GLFontTextureLoader>();
    }
    
}

using namespace copilot;

GLFont::GLFont(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name), currentHandle(1)
{
}

GLFontTexture GLFont::compileTexture(int fontHeight, const unsigned char* buffer, size_t bufferSize)
{
    GLFontTexture fontTexture;
    
    GLDraw* draw = manager->request<GLDraw>();
    Images* images = manager->request<Images>();
    
    // store target height
    fontTexture.fontHeight = fontHeight;
    
    // invalid tex
    fontTexture.texture.handle = 0;
    
    // generate alpha channel texture
    int textureSize = 64;
    unsigned char* alphaImage = new unsigned char[textureSize * textureSize];
    
	// attempt to generate texture & data
	stbtt_bakedchar glyphData[STB_FONT_GLYPH_COUNT];
	int result = stbtt_BakeFontBitmap(buffer, 0, fontHeight, alphaImage, textureSize, textureSize, STB_FONT_GLYPH_FIRST, STB_FONT_GLYPH_COUNT, glyphData);
    
	// keep trying until we succeed
	while (result < 1)
	{
		// move up to next power of two
		textureSize = draw->nextPowerOfTwo(textureSize+1);
		
		// free old alpha image
		delete[] alphaImage;
		
		// we really don't want a font texture larger than STB_FONT_GLYPH_MAX_TEXTURE_SIZE
		if (textureSize > STB_FONT_GLYPH_MAX_TEXTURE_SIZE) return fontTexture;
		
		// create a larger alpha image
		alphaImage = new unsigned char[textureSize * textureSize];
		
		// attempt to generate texture & data
		result = stbtt_BakeFontBitmap(buffer, 0, fontHeight, alphaImage, textureSize, textureSize, STB_FONT_GLYPH_FIRST, STB_FONT_GLYPH_COUNT, glyphData);
	}
    
    // compile texture
    glm::vec2 size(textureSize);
    Image image = images->createImageFromAlpha(alphaImage, size);
    fontTexture.texture = draw->compileTexture(size, image.pixels.data(), image.pixels.size());
    
    // delete alpha buffer
    delete[] alphaImage;
    
    // setup glyphs
    glm::rect rect;
	stbtt_bakedchar* b;
	for (int i=0; i<STB_FONT_GLYPH_COUNT; ++i)
	{
		// going to copy baked data into our own structure
		b = &glyphData[i];
        
        GLGlyph glyph;
        glyph.step = b->xadvance;
        glyph.offset.x = b->xoff;
        glyph.offset.y = b->yoff;
        
        // sub-rect & size
        glm::rect rect = glm::rect::withLTRB(b->x0, b->y0, b->x1, b->y1);
        
        glyph.size = rect.getSize();
        glyph.halfSize = glyph.size * 0.5f;
        
        // get uv coords
        glyph.uv1 = (rect.getMin() / fontTexture.texture.size) * fontTexture.texture.uv;
        glyph.uv2 = (rect.getMax() / fontTexture.texture.size) * fontTexture.texture.uv;
        
        // char lookup map
        fontTexture.glyphs[STB_FONT_GLYPH_FIRST + i] = glyph;		
	}
    
    return fontTexture;
}

void GLFont::drawText(GLRenderQuadPool* quadPool, GLFontTexture* font, float x, float y, const char* text, glm::vec4** colors)
{
    GLDraw* draw = manager->request<GLDraw>();
    
    // default colors
    static glm::vec4 _color(1.f);
    static glm::vec4* _colors[4] = {&_color, &_color, &_color, &_color};
    if (!colors) colors = _colors;
	
	// prevent wiggles (tm)
	x = (int)x; y = (int)y;
    
    GLGlyph* g;
    glm::vec2 center;
    std::map<int, GLGlyph>::iterator gi;
	for (int i=0; i<(int)strlen(text); ++i)
	{
        gi = font->glyphs.find((int)text[i]);
		
		// unknown chars rendered as space
		if (gi == font->glyphs.end())
            gi = font->glyphs.find((int)' ');
        
        g = &gi->second;
        
        center.x = STBTT_ifloor(x + g->offset.x + g->halfSize.x) + 0.5f;
        center.y = STBTT_ifloor(y + g->offset.y + g->halfSize.y) + 0.5f;
        quadPool->quads[quadPool->index++] = draw->compileQuad(font->texture, center, g->size, NULL, colors, NULL, &g->uv1, &g->uv2);

        x += g->step;
	}
}
float GLFont::getTextHeight(GLFontTexture* font, const char* text)
{
	float y = 0, top = 0, nextTop = 0;
    
    GLGlyph* g;
    glm::vec2 center;
    std::map<int, GLGlyph>::iterator gi;
	for (int i=0; i<(int)strlen(text); ++i)
	{
        gi = font->glyphs.find((int)text[i]);
		
		// unknown chars rendered as space
		if (gi == font->glyphs.end())
            gi = font->glyphs.find((int)' ');
        
        g = &gi->second;
        
		nextTop = (STBTT_ifloor(y + g->offset.y + g->halfSize.y) + 0.5f) - (g->size.y * 0.5f);
		if (nextTop < top) top = nextTop;
	}
    
    return -top;
}
float GLFont::getTextWidth(GLFontTexture* font, const char* text)
{
	float x = 0, right = 0;
    
    GLGlyph* g;
    glm::vec2 center;
    std::map<int, GLGlyph>::iterator gi;
	for (int i=0; i<(int)strlen(text); ++i)
	{
        gi = font->glyphs.find((int)text[i]);
		
		// unknown chars rendered as space
		if (gi == font->glyphs.end())
            gi = font->glyphs.find((int)' ');
        
        g = &gi->second;
        
        right = STBTT_ifloor(x + g->offset.x) + g->size.x + 0.5f;
        
        x += g->step;
	}
    
    return right;
}
float GLFont::getTextStep(GLFontTexture* font, const char glyph)
{
    std::map<int, GLGlyph>::iterator gi = font->glyphs.find((int)glyph);
		
    // unknown chars rendered as space
    if (gi == font->glyphs.end())
        gi = font->glyphs.find((int)' ');
    
    return gi->second.step;
}
glm::vec2 GLFont::getTextSize(GLFontTexture* font, const char* text)
{
	float x = 0, right = 0;
	float y = 0, top = 0, nextTop = 0;
    
    GLGlyph* g;
    glm::vec2 center;
    std::map<int, GLGlyph>::iterator gi;
	for (int i=0; i<(int)strlen(text); ++i)
	{
        gi = font->glyphs.find((int)text[i]);
		
		// unknown chars rendered as space
		if (gi == font->glyphs.end())
            gi = font->glyphs.find((int)' ');
        
        g = &gi->second;
        
        right = STBTT_ifloor(x + g->offset.x) + g->size.x + 0.5f;
		nextTop = (STBTT_ifloor(y + g->offset.y + g->halfSize.y) + 0.5f) - (g->size.y * 0.5f);
        
        x += g->step;
		if (nextTop < top) top = nextTop;
	}
    
    return glm::vec2(right, -top);
}

