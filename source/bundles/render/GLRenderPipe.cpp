
#include <bundles/sdl/LibSDL.h>
#include <bundles/render/GLRenderPipe.h>

using namespace copilot;

GLRenderPipeBatch::GLRenderPipeBatch(GLRenderPipeTask* inTask)
: triIndex(0), quadIndex(0), task(inTask)
{
    task->triPool->index = 0;
    task->quadPool->index = 0;
}

GLRenderTriPool* GLRenderPipeBatch::getTriPool()
{
    return task->triPool;
}

GLRenderQuadPool* GLRenderPipeBatch::getQuadPool()
{
    return task->quadPool;
}

void GLRenderPipeBatch::render(GLShader shader,
            glm::mat4 projection, glm::mat4 transform,
            GLTexture* textures, uint16_t totalTextures,
            GLRenderTriPool* triPool)
{
    uint16_t total = triPool->index - triIndex;
    if (total)
    {
        task->tris.push({
            shader,
            projection,
            transform,
            textures,
            totalTextures,
            &triPool->triangles[triIndex],
            total
        });
        triIndex = triPool->index;
    }
}

void GLRenderPipeBatch::render(GLShader shader,
            glm::mat4 projection, glm::mat4 transform,
            GLTexture* textures, uint16_t totalTextures,
            GLRenderQuadPool* quadPool)
{
    uint16_t total = quadPool->index - quadIndex;
    if (total)
    {
        task->quads.push({
            shader,
            projection,
            transform,
            textures,
            totalTextures,
            &quadPool->quads[quadIndex],
            total
        });
        quadIndex = quadPool->index;
    }
}

GLRenderPipe::GLRenderPipe(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
    GLDraw* draw = manager->request<GLDraw>();
    tri.read = tri.write = 0;
    quad.read = quad.read = 0;
    triIndices = draw->compileTriIndices();
    quadIndices = draw->compileQuadIndices();
}

GLRenderPipe::~GLRenderPipe()
{
}

size_t GLRenderPipe::addCamera2D(int rank)
{
    GLRenderCamera2D camera;
    camera.rank = rank;
    camera.angle = 0.f;
    camera.scale = glm::vec2(1.f);
    camera.position = glm::vec2(0.f);
    
    camera.viewport.autosize = true;
    camera.viewport.rect = glm::rect::withXYWH(0.f, 0.f, 1.f, 1.f);
    
    size_t handle = camera2Ds.add(camera);
    return handle;
}
GLRenderCamera2D* GLRenderPipe::getCamera2D(size_t handle)
{
    return camera2Ds.get(handle);
}
void GLRenderPipe::removeCamera2D(size_t handle)
{
    camera2Ds.remove(handle);
}
size_t GLRenderPipe::camera2DHandle(size_t index)
{
    return camera2Ds.handle(index);
}
size_t GLRenderPipe::totalCamera2Ds()
{
    return camera2Ds.total();
}

void GLRenderPipe::setUnitForWindow(size_t windowHandle, GLRenderScreenUnits unit)
{
    unitsForWindows[windowHandle] = unit;
}

glm::vec2 GLRenderPipe::getUnitForWindow(size_t windowHandle)
{
    std::map<size_t,GLRenderScreenUnits>::iterator u=unitsForWindows.find(windowHandle);
    if (u != unitsForWindows.end()) return u->second.calculated;
    
    LibSDL* sdl = manager->request<LibSDL>();
    LibSDLWindow* window = sdl->getWindow(windowHandle);
    if (!window) return glm::vec2(0.f);
    
    int width, height;
    SDL_GetWindowSize(window->state, &width, &height);
    
    return glm::vec2(width, height);
}


glm::vec2 GLRenderPipe::unitVecToScreen(GLRenderScreenUnits* unit, glm::vec2 vec)
{
    return vec * unit->unitToScreen;
}
glm::rect GLRenderPipe::unitRectToScreen(GLRenderScreenUnits* unit, glm::rect rect)
{
    return glm::rect::withMinMax(rect.getMin() * unit->unitToScreen,
                                 rect.getMax() * unit->unitToScreen);
}
glm::vec2 GLRenderPipe::screenVecToUnit(GLRenderScreenUnits* unit, glm::vec2 vec)
{
    return vec * unit->unitToScreen;
}
glm::rect GLRenderPipe::screenRectToUnit(GLRenderScreenUnits* unit, glm::rect rect)
{
    return glm::rect::withMinMax(rect.getMin() * unit->unitToScreen,
                                 rect.getMax() * unit->unitToScreen);
}

glm::mat4 GLRenderPipe::unitProjectionOrtho(GLRenderScreenUnits* unit)
{
    return glm::ortho(0.f, unit->calculated.x, unit->calculated.y, 0.f, -512.f, 512.f);
}
glm::mat4 GLRenderPipe::camera2DTransform(GLRenderScreenUnits* unit, GLRenderCamera2D* camera2D, glm::vec2 parallax)
{
    glm::vec3 halfUnit = glm::vec3(unit->calculated.x * 0.5f, unit->calculated.y * 0.5f, 0.f);
    
    glm::vec3 position = glm::vec3(
       camera2D->viewport.rect.getLeft() - camera2D->position.x * parallax.x,
       camera2D->viewport.rect.getTop() - camera2D->position.y * parallax.x, 0.f);
    
    glm::mat4 transform = glm::translate(glm::mat4(1.f), halfUnit);
    glm::mat4 rotateTransform = glm::rotate(transform, camera2D->angle, glm::vec3(0.f, 0.f, 1.f));
    glm::mat4 scaleTransform = rotateTransform * glm::scale(glm::mat4(1.f), glm::vec3(camera2D->scale, 1.f));
    
    return glm::translate(scaleTransform, position);
}

size_t GLRenderPipe::addLayer(EventedSelectorFunc selector, GLRenderLayerFunc render, bool parallel)
{
    return layers.add(new GLRenderPipeLayerData(parallel, PropertyCollection(), selector, render));
}
size_t GLRenderPipe::addLayer(PropertyCollection meta, EventedSelectorFunc selector, GLRenderLayerFunc render, bool parallel)
{
    return layers.add(new GLRenderPipeLayerData(parallel, meta, selector, render));
}
void GLRenderPipe::removeLayer(size_t handle)
{
    layers.remove(handle);
}
size_t GLRenderPipe::layerHandle(size_t index)
{
    return layers.handle(index);
}
size_t GLRenderPipe::totalLayers()
{
    return layers.total();
}

void GLRenderPipe::addLayerToCamera2D(size_t layerHandle, size_t cameraHandle)
{
    GLRenderCamera2D* camera2D = camera2Ds.get(cameraHandle);
    if (camera2D)
    {
        camera2D->layers.push_back(layerHandle);
        camera2D->layers.unique();
    }
}
void GLRenderPipe::removeLayerFromCamera2D(size_t layerHandle, size_t cameraHandle)
{
    GLRenderCamera2D* camera2D = camera2Ds.get(cameraHandle);
    if (camera2D) camera2D->layers.remove(layerHandle);
}
std::list<size_t> GLRenderPipe::layersForCamera2D(size_t cameraHandle)
{
    GLRenderCamera2D* camera2D = camera2Ds.get(cameraHandle);
    if (!camera2D) return {};
    return camera2D->layers;
}

void GLRenderPipe::addCamera2DToWindow(size_t cameraHandle, size_t windowHandle)
{
    camerasForWindows[windowHandle].push_back(cameraHandle);
    camerasForWindows[windowHandle].unique();
}
void GLRenderPipe::removeCamera2DFromWindow(size_t cameraHandle, size_t windowHandle)
{
    camerasForWindows[windowHandle].remove(cameraHandle);
}
std::list<size_t> GLRenderPipe::camera2DsForWindow(size_t windowHandle)
{
    return camerasForWindows[windowHandle];
}

GLRenderTriPool* GLRenderPipe::readTriPool()
{
    GLRenderTriPool* pool = &tri.pool[tri.read];
    tri.read = (tri.read + 1) % GLRenderPipeDepth;
    return pool;
}
GLRenderQuadPool* GLRenderPipe::readQuadPool()
{
    GLRenderQuadPool* pool = &quad.pool[quad.read];
    quad.read = (quad.read + 1) % GLRenderPipeDepth;
    return pool;
}
GLRenderTriPool* GLRenderPipe::writeTriPool()
{
    GLRenderTriPool* pool = &tri.pool[tri.write];
    tri.write = (tri.write + 1) % GLRenderPipeDepth;
    return pool;
}
GLRenderQuadPool* GLRenderPipe::writeQuadPool()
{
    GLRenderQuadPool* pool = &quad.pool[quad.write];
    quad.write = (quad.write + 1) % GLRenderPipeDepth;
    return pool;
}

// compare funcs
bool compare_by_rank(GLRenderCamera2D* first, GLRenderCamera2D* second)
{
    return (first->rank < second->rank);
}

// task for rendering a layer
TaskRunnerLogic(GLRenderPipeTaskExec)
{
    GLRenderPipeTask* task = data->toUnsafePtr<GLRenderPipeTask>();
    
    // perform selection
    task->selection = task->collection->select(task->layer->selector, task->layer->meta);
    
    // if a parallel layer perform render
    if (task->layer->parallel)
    {
        GLRenderPipeBatch batch(task);
        task->layer->render(manager, task->collection, task->render, NULL, &batch,
                            task->units, task->camera, task->layer->meta, task->selection);
    }
}

// the big daddy
void GLRenderPipe::dispatch(EventedCollection* entities)
{
    LibSDL* sdl = manager->request<LibSDL>();
    GLDraw* draw = manager->request<GLDraw>();
    TaskRunner* tasks = manager->request<TaskRunner>();
    
    int width, height;
    glm::vec2 drawSize;
    glm::vec2 windowSize;
    glm::mat4 projection;
    std::list<GLRenderCamera2D*> windowCameras;
    
    for (std::map<size_t,std::list<size_t> >::iterator w=camerasForWindows.begin(); w!=camerasForWindows.end(); ++w)
    {
        LibSDLWindow* window = sdl->getWindow(w->first);
        if (window)
        {
            sdl->makeCurrentWindow(w->first);
            
            SDL_GetWindowSize(window->state, &width, &height);
            windowSize = glm::vec2(width, height);
            
            SDL_GL_GetDrawableSize(window->state, &width, &height);
            drawSize = glm::vec2(width, height);
            
            GLRenderScreenUnits units;
            std::map<size_t,GLRenderScreenUnits>::iterator u=unitsForWindows.find(w->first);
            if (u == unitsForWindows.end())
            {
                units.autosize = true;
            }
            else
            {
                units = u->second;
            }
            
            // config units
            if (units.autosize)
            {
                units.calculated = windowSize;
            }
            else
            {
                // pick smaller axis
                if (windowSize.y < windowSize.x)
                {
                    units.calculated.y = units.target.y;
                    units.calculated.x = windowSize.x * (units.target.y / windowSize.y);
                }
                else
                {
                    units.calculated.x = units.target.x;
                    units.calculated.y = windowSize.y * (units.target.x / windowSize.x);
                }
            }
            
            // calc ratios
            units.screenToUnit = units.calculated / windowSize;
            units.unitToScreen = windowSize / units.calculated;
            
            // update
            if (u != unitsForWindows.end()) u->second = units;
            
            draw->config(drawSize);
            
            windowCameras.clear();
            for (std::list<size_t>::iterator c=w->second.begin(); c!=w->second.end(); ++c)
            {
                GLRenderCamera2D* camera = camera2Ds.get(*c);
                if (camera) windowCameras.push_back(camera);
            }
            windowCameras.sort(compare_by_rank);
            
            for (std::list<GLRenderCamera2D*>::iterator c=windowCameras.begin(); c!=windowCameras.end(); ++c)
            {
                GLRenderCamera2D* camera = (*c);
                
                if (camera->viewport.autosize)
                    camera->viewport.rect = glm::rect::withMinMax(glm::vec2(0.f), units.calculated);
                
                // cache viewable area
                glm::vec2 inverse = glm::vec2(1.f / camera->scale.x, 1.f / camera->scale.y);
                glm::vec2 dist = glm::vec2(glm::min(units.calculated.x, units.calculated.y)) * inverse;
                camera->cachedViewable = glm::rect::withMinMax(camera->position - dist, camera->position + dist);
                
                // build tasks
                Any* data;
                for (std::list<size_t>::iterator l=camera->layers.begin(); l!=camera->layers.end(); ++l)
                {
                    GLRenderPipeLayerData* layer = layers.get(*l);
                    if (layer)
                    {
                        GLRenderPipeTask task;
                        task.render = this;
                        task.layer = layer;
                        task.units = &units;
                        task.camera = camera;
                        task.collection = entities;
                        task.triPool = writeTriPool();
                        task.quadPool = writeQuadPool();
                        
                        data = tasks->write();
                        *data = task;
                    }
                }
                
                // kick off processing
                tasks->run(GLRenderPipeTaskExec);
                
                // handle results
                data = tasks->result();
                while (data)
                {
                    GLRenderPipeTask* task = data->toUnsafePtr<GLRenderPipeTask>();
                    if (!task->layer->parallel)
                    {
                        task->layer->render(manager, task->collection, task->render, draw, NULL,
                                            task->units, task->camera, task->layer->meta, task->selection);
                    }
                    else
                    {
                        while (task->tris.size())
                        {
                            GLRenderPipeTaskTri* triResult = &task->tris.front();
                            draw->render(triResult->shader, triResult->projection, triResult->transform,
                                         triResult->textures, triResult->totalTextures,
                                         triResult->triangles, &triIndices, triResult->totalTriangles);
                            task->tris.pop();
                        }
                        while (task->quads.size())
                        {
                            GLRenderPipeTaskQuad* quadResult = &task->quads.front();
                            draw->render(quadResult->shader, quadResult->projection, quadResult->transform,
                                         quadResult->textures, quadResult->totalTextures,
                                         quadResult->quads, &quadIndices, quadResult->totalQuads);
                            task->quads.pop();
                        }
                    }
                    data = tasks->result();
                }
            }
            
            sdl->swapBuffersWindow(w->first);
        }
    }
}


