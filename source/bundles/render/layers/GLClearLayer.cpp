
#include <bundles/render/layers/GLClearLayer.h>

using namespace copilot;

GLRenderPipeLayer(GLClearLayer::render)
{
    glm::vec3 color = meta.get<glm::vec3>("color");
    draw->clear(color);
}

size_t GLClearLayer::addLayer(CoreLogicManager* manager, glm::vec3 color)
{
    copilot::PropertyCollection meta;
    meta.set("color", color);
    return manager->request<GLRenderPipe>()->addLayer(meta, EventedCollection::selEmpty, GLClearLayer::render, false);
}
