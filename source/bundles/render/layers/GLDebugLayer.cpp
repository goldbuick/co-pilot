
#include <bundles/render/GLDraw.h>
#include <bundles/files/AssetManager.h>
#include <bundles/render/layers/GLDebugLayer.h>

using namespace copilot;

// render
GLRenderPipeLayer(GLDebugLayer::render)
{
    Props(GLDebugLayer);
    NamedProps(GLDebugLayer, Meta);
    
    glm::mat4 projection = render->unitProjectionOrtho(units);
    
    Meta.with(meta);
    glm::vec2 parallax = Meta.hasparallax() ? Meta.parallax() : glm::vec2(1.f);
    glm::mat4 transform = render->camera2DTransform(units, camera, parallax);
    
    glm::rect rect;
    glm::mat4 _transform;
    
    GLShaderRef* shader = NULL;
    GLShaderRef* lastShader = NULL;
    GLTextureRef* tex = NULL;
    GLTextureRef* lastTex = NULL;
    
    GLRenderQuadPool* quadPool = batch->getQuadPool();
    for (std::list<ReadEventedEntity>::iterator i=selection.begin(); i!=selection.end(); ++i)
    {
        Props.with(*i);
        if (!Props.hasglTexture() || !camera->cachedViewable.isInside(Props.position())) continue;
        
        shader = Props.glShader();
        tex = Props.glTexture();
        
        _transform = glm::rotate(Props.glAngle(), 0.f, 0.f, 1.f);

        if (lastShader != NULL && lastTex != NULL && (lastShader != shader || lastTex != tex))
            batch->render(*lastShader->ref, projection, transform, lastTex->ref, 1, quadPool);
        
        if (shader && shader->ref && tex && tex->ref)
        {
            quadPool->quads[quadPool->index++] = draw->compileQuad(*tex->ref, Props.position(), Props.glSize(), NULL, NULL, &_transform);
            lastShader = shader;
            lastTex = tex;
        }
    }
    
    if (shader && shader->ref && tex && tex->ref) batch->render(*shader->ref, projection, transform, tex->ref, 1, quadPool);
}

// mix-ins
GLShaderRef* GLDebugLayer::shader(CoreLogicManager* manager)
{
    AssetManager* assets = manager->request<AssetManager>();
    return assets->request<GLShaderRef>("default,default");
}
GLTextureRef* GLDebugLayer::texture(CoreLogicManager* manager, glm::vec3 color)
{
    AssetManager* assets = manager->request<AssetManager>();
    return assets->request<GLTextureRef>(GLTextureColorStr(color).c_str());
}
size_t GLDebugLayer::addLayer(CoreLogicManager* manager, EventedSelectorFunc selector)
{
    return manager->request<GLRenderPipe>()->addLayer(selector, GLDebugLayer::render);
}
size_t GLDebugLayer::addLayer(CoreLogicManager* manager, PropertyCollection& meta, EventedSelectorFunc selector)
{
    return manager->request<GLRenderPipe>()->addLayer(meta, selector, GLDebugLayer::render);
}

