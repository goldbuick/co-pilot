
#include <bundles/sdl/InputMap.h>
#include <bundles/sdl/LibSDL.h>

using namespace copilot;

InputMap::InputMap(CoreLogicManager* manager, const char* name)
: CoreLogic(manager, name)
{
    eventId = manager->request<LibSDL>()->getNewPollId();
}

InputMap::~InputMap() { }

uint16_t InputMap::getNewPollId()
{
    return polling.getNewPollId();
}

void InputMap::updateTriggers(SDL_Event event, InputMapTriggers& triggers)
{
    LibSDL* sdl = manager->request<LibSDL>();
    switch (event.type)
    {
        case SDL_KEYUP:
            for (std::list<InputMapKeyTrigger>::iterator k=triggers.keys.begin(); k!=triggers.keys.end(); ++k)
            if (k->key == event.key.keysym.sym)
            {
                k->pressed = 0.f;
            }
            break;
        case SDL_KEYDOWN:
            for (std::list<InputMapKeyTrigger>::iterator k=triggers.keys.begin(); k!=triggers.keys.end(); ++k)
            if (k->key == event.key.keysym.sym)
            {
                k->pressed = 32767.f;
            }
            break;
        case SDL_JOYBUTTONUP:
            for (std::list<InputMapJoystickButtonTrigger>::iterator j=triggers.joystickButtons.begin(); j!=triggers.joystickButtons.end(); ++j)
            if (sdl->getJoystickID(j->index) == event.jbutton.which && j->button == event.jbutton.button)
            {
                j->pressed = 0.f;
            }
            break;
        case SDL_JOYBUTTONDOWN:
            //printf("SDL_JOYBUTTONDOWN(%i) %i\n", (int)event.jbutton.which, (int)event.jbutton.button);
            for (std::list<InputMapJoystickButtonTrigger>::iterator j=triggers.joystickButtons.begin(); j!=triggers.joystickButtons.end(); ++j)
            if (sdl->getJoystickID(j->index) == event.jbutton.which && j->button == event.jbutton.button)
            {
                j->pressed = 32767.f;
            }
            break;
        case SDL_JOYHATMOTION:
            //printf("SDL_JOYHATMOTION(%i) %i\n", (int)event.jhat.which, (int)event.jhat.hat);
            // TODO
            break;
        case SDL_JOYAXISMOTION:
            //printf("SDL_JOYAXISMOTION(%i) %i\n", (int)event.jaxis.which, (int)event.jaxis.axis);
            for (std::list<InputMapJoystickAxisTrigger>::iterator a=triggers.joystickAxises.begin(); a!=triggers.joystickAxises.end(); ++a)
            if (sdl->getJoystickID(a->index) == event.jaxis.which && a->axis == event.jaxis.axis)
            {
                if (a->direction < 0 && event.jaxis.value < 0)
                {
                    a->pressed = fabs(event.jaxis.value);
                }
                else if (a->direction > 0 && event.jaxis.value > 0)
                {
                    a->pressed = fabs(event.jaxis.value);
                }
                else
                {
                    a->pressed = 0.f;
                }
            }
            break;
        case SDL_MOUSEBUTTONUP:
            for (std::list<InputMapMouseButtonTrigger>::iterator m=triggers.mouseButtons.begin(); m!=triggers.mouseButtons.end(); ++m)
            if (m->button == event.button.button)
            {
                m->pressed = 0.f;
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
            for (std::list<InputMapMouseButtonTrigger>::iterator m=triggers.mouseButtons.begin(); m!=triggers.mouseButtons.end(); ++m)
            if (m->button == event.button.button)
            {
                m->pressed = 32767.f;
            }
            break;
    }
    
    triggers.pressed = 0.f;
    
    for (std::list<InputMapKeyTrigger>::iterator k=triggers.keys.begin(); k!=triggers.keys.end(); ++k)
    if (k->pressed) triggers.pressed = k->pressed;
    
    for (std::list<InputMapMouseButtonTrigger>::iterator m=triggers.mouseButtons.begin(); m!=triggers.mouseButtons.end(); ++m)
    if (m->pressed) triggers.pressed = m->pressed;
    
    for (std::list<InputMapJoystickButtonTrigger>::iterator j=triggers.joystickButtons.begin(); j!=triggers.joystickButtons.end(); ++j)
    if (j->pressed) triggers.pressed = j->pressed;
    
    for (std::list<InputMapJoystickAxisTrigger>::iterator a=triggers.joystickAxises.begin(); a!=triggers.joystickAxises.end(); ++a)
    if (a->pressed) triggers.pressed = a->pressed;
}

bool InputMap::getEvents(uint16_t pollId, InputMapEvent& event)
{
    SDL_Event _event;
    InputMapPair* pair;
    InputMapEvent trigger;
    InputMapButton* button;
    InputMapVector* vector;
    
    while (manager->request<LibSDL>()->getEvents(eventId, _event))
    {
        for (int i=0; i<handles.total(); ++i)
        {
            trigger.mapId = handles.handle(i);
            pair = handles.get(trigger.mapId);
            if (pair)
            {
                button = buttons.get(pair->buttonId);
                if (button)
                {
                    trigger.isButton = true;
                    updateTriggers(_event, button->triggers);
                    if (button->triggers.lastPressed != button->triggers.pressed)
                    {
                        button->triggers.lastPressed = button->triggers.pressed;
                        trigger.value.pressed = fabs(button->triggers.pressed) / 32767.f;
                        polling.addEntry(trigger);
                    }
                }
                
                vector = vectors.get(pair->vectorsId);
                if (vector)
                {
                    trigger.isButton = false;
                    updateTriggers(_event, vector->up);
                    updateTriggers(_event, vector->down);
                    updateTriggers(_event, vector->left);
                    updateTriggers(_event, vector->right);

                    if (vector->up.lastPressed != vector->up.pressed ||
                        vector->down.lastPressed != vector->down.pressed ||
                        vector->left.lastPressed != vector->left.pressed ||
                        vector->right.lastPressed != vector->right.pressed)
                    {
                        vector->up.lastPressed = vector->up.pressed;
                        vector->down.lastPressed = vector->down.pressed;
                        vector->left.lastPressed = vector->left.pressed;
                        vector->right.lastPressed = vector->right.pressed;
                        
                        // threshold filter
                        if (vector->up.pressed < vector->threshold &&
                            vector->down.pressed < vector->threshold &&
                            vector->left.pressed < vector->threshold &&
                            vector->right.pressed < vector->threshold)
                        {
                            vector->x = vector->y = 0.f;
                        }
                        else
                        {
                            if ((vector->up.pressed && vector->down.pressed) || (!vector->up.pressed && !vector->down.pressed))
                            {
                                vector->y = 0.f;
                            }
                            else if (vector->up.pressed)
                            {
                                vector->y = -vector->up.pressed / 32767.f;
                            }
                            else if (vector->down.pressed)
                            {
                                vector->y = vector->down.pressed / 32767.f;
                            }
                            
                            if ((vector->left.pressed && vector->right.pressed) || (!vector->left.pressed && !vector->right.pressed))
                            {
                                vector->x = 0.f;
                            }
                            else if (vector->left.pressed)
                            {
                                vector->x = -vector->left.pressed / 32767.f;
                            }
                            else if (vector->right.pressed)
                            {
                                vector->x = vector->right.pressed / 32767.f;
                            }
                        }
                        
                        trigger.value.vector.x = vector->x;
                        trigger.value.vector.y = vector->y;
                        polling.addEntry(trigger);
                    }
                }
            }
        }
    }
    
    return polling.getEntry(pollId, event);
}

size_t InputMap::addButton(InputMapButton button)
{
    button.triggers.lastPressed = button.triggers.pressed = 0.f;
    return handles.add({ buttons.add(button), 0 });
}
void InputMap::clearButton(size_t mapId)
{
    InputMapPair* pair = handles.get(mapId);
    if (pair && pair->buttonId) buttons.remove(pair->buttonId);
}

size_t InputMap::addVector(InputMapVector vector)
{
    vector.x = vector.y = 0.f;
    vector.up.lastPressed = vector.up.pressed = 0.f;
    vector.down.lastPressed = vector.down.pressed = 0.f;
    vector.left.lastPressed = vector.left.pressed = 0.f;
    vector.right.lastPressed = vector.right.pressed = 0.f;
    return handles.add({ 0, vectors.add(vector)});
}
void InputMap::clearVector(size_t mapId)
{
    InputMapPair* pair = handles.get(mapId);
    if (pair && pair->vectorsId) vectors.remove(pair->vectorsId);
}
