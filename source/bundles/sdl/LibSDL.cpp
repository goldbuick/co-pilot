
#include <bundles/sdl/LibSDL.h>

using namespace copilot;

LibSDL::LibSDL(CoreLogicManager* inManager, const char* inName)
: CoreLogic(inManager, inName), context(NULL), escToQuit(true), shouldQuit(false), currentWindow(0)
{

#if defined(WIN32) || defined(_WINDOWS)
    ULONG_PTR affinity_mask;
    ULONG_PTR process_affinity_mask;
    ULONG_PTR system_affinity_mask;
    
    if (!GetProcessAffinityMask(GetCurrentProcess(), &process_affinity_mask, &system_affinity_mask))
        return;
    
    // run on the first core
    affinity_mask = (ULONG_PTR)1 << 0;
    if (affinity_mask & process_affinity_mask)
        SetThreadAffinityMask(GetCurrentThread(), affinity_mask);
#endif
    
    SDL_Init(SDL_INIT_EVERYTHING);
    
    printf("SDL_NumJoysticks(%i)\n", SDL_NumJoysticks());

    joysticks.resize(SDL_NumJoysticks());
    for (int i=0; i<SDL_NumJoysticks(); ++i)
    {
        joysticks[i] = SDL_JoystickOpen(i);
        SDL_JoystickUpdate();
        if (joysticks[i])
        {
            printf("SDL_JoystickOpen(%i) %s\n", i, SDL_JoystickNameForIndex(i));
            printf("SDL_JoystickOpen(%i) axes: %i\n", i, SDL_JoystickNumAxes(joysticks[i]));
            printf("SDL_JoystickOpen(%i) buttons: %i\n", i, SDL_JoystickNumButtons(joysticks[i]));
            printf("SDL_JoystickOpen(%i) balls: %i\n", i, SDL_JoystickNumBalls(joysticks[i]));
            printf("SDL_JoystickOpen(%i) hats: %i\n", i, SDL_JoystickNumHats(joysticks[i]));
        }
        else
        {
            printf("SDL_JoystickOpen(%i) %s\n", i, SDL_GetError());
        }
    }
    
    SDL_JoystickEventState(SDL_ENABLE);
}
LibSDL::~LibSDL()
{
    for (int i=0; i<SDL_NumJoysticks(); ++i)
        SDL_JoystickClose(joysticks[i]);
    
    for (int i=0; i<windows.total(); ++i)
        SDL_DestroyWindow(windows.get(windows.handle(i))->state);
    
    if (context)
    {
        SDL_GL_DeleteContext(context);
        context = NULL;
    }
    
    SDL_Quit();
}

bool LibSDL::active()
{
    return !shouldQuit && windows.total();
}

void LibSDL::setEscToQuit(bool enable)
{
    escToQuit = enable;
}

uint16_t LibSDL::getNewPollId()
{
    return eventQueue.getNewPollId();
}

bool LibSDL::getEvents(uint16_t pollId, SDL_Event& event)
{
    SDL_Event _event;
    while (SDL_PollEvent(&_event))
    {
        switch (_event.type)
        {
            case SDL_QUIT:
                shouldQuit = true;
                break;
                
            case SDL_KEYDOWN:
                switch(_event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        if (escToQuit) shouldQuit = true;
                        break;
                }
                break;
                
            case SDL_JOYDEVICEADDED:
                joysticks.resize(SDL_NumJoysticks());
                joysticks[_event.jdevice.which] = SDL_JoystickOpen(_event.jdevice.which);
                SDL_JoystickEventState(SDL_ENABLE);
                break;
            case SDL_JOYDEVICEREMOVED:
                for (std::vector<SDL_Joystick*>::iterator i=joysticks.begin(); i!=joysticks.end(); ++i)
                    if (SDL_JoystickInstanceID(*i) == _event.jdevice.which)
                    {
                        SDL_JoystickClose(*i);
                        joysticks.erase(i);
                        break;
                    }
                break;
        }
        eventQueue.addEntry(_event);
    }
    
    uint64_t now = getTime();
    for (std::list<LibSDLTimer>::iterator i=timers.begin(); i!=timers.end(); ++i)
    {
        if (now > i->lastTime)
        {
            uint64_t acc = now - i->lastTime;
            uint64_t delta = i->delta * SDL_GetPerformanceFrequency();
            if (acc >= delta)
            {
                i->lastTime = now;
                SDL_zero(_event);
                _event.type = i->eventId;
                _event.user.code = (uint32_t)(1000.f * i->delta);
                eventQueue.addEntry(_event);
            }
        }
    }
    
    return eventQueue.getEntry(pollId, event);
}

uint32_t LibSDL::addTimer(float ticksPerSecond)
{
    float delta = 1.f / ticksPerSecond;
    
    for (std::list<LibSDLTimer>::iterator i=timers.begin(); i!=timers.end(); ++i)
        if (i->delta == delta) return i->eventId;

    LibSDLTimer timer = {
        delta,
        SDL_RegisterEvents(1),
        getTime()
    };
    
    timers.push_back(timer);
    return timer.eventId;
}

uint32_t LibSDL::addCustomEventId(const char* name)
{
    std::map<std::string, uint32_t>::iterator i=customEvents.find(name);
    if (i != customEvents.end()) return i->second;
    
    uint32_t eventId = SDL_RegisterEvents(1);;
    customEvents[name] = eventId;
    
    return eventId;
}

uint64_t LibSDL::getTime()
{
    return SDL_GetPerformanceCounter();
}
float LibSDL::getDeltaTime(uint64_t now, uint64_t lastTime)
{
    return (now - lastTime) / (float)SDL_GetPerformanceFrequency();
}

size_t LibSDL::addWindow(const char* title, glm::vec2 size)
{
    LibSDLWindow window;
    
    window.focus = true;
    
    SDL_GL_SetSwapInterval(0);    
    #ifdef MOBILE_RENDER
        SDL_GL_SetAttribute(SDL_GL_RETAINED_BACKING, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        window.state = SDL_CreateWindow(title, 0, 0, size.x, size.y,
                                        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_BORDERLESS);
    #else    
        window.state = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size.x, size.y,
                                        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    #endif
    
    if (!context) context = SDL_GL_CreateContext(window.state);
    SDL_GL_MakeCurrent(window.state, context);
    
    currentWindow = windows.add(window);
    return currentWindow;
}
void LibSDL::removeWindow(size_t handle)
{
    LibSDLWindow* window = windows.get(handle);
    if (window)
    {
        SDL_DestroyWindow(window->state);
        windows.remove(handle);
    }
}
LibSDLWindow* LibSDL::getWindow(size_t handle)
{
    return windows.get(handle);
}
size_t LibSDL::getWindowHandle(size_t index)
{
    return windows.handle(index);
}
size_t LibSDL::totalWindows()
{
    return windows.total();
}
void LibSDL::makeCurrentWindow(size_t handle)
{
    LibSDLWindow* window = windows.get(handle);
    if (window && handle != currentWindow)
    {
        currentWindow = handle;
        SDL_GL_MakeCurrent(window->state, context);
    }
}
void LibSDL::swapBuffersWindow(size_t handle)
{
    LibSDLWindow* window = windows.get(handle);
    if (window) SDL_GL_SwapWindow(window->state);
}

const char* LibSDL::getJoystickName(size_t index)
{
    if (index >= joysticks.size()) return "";
    return SDL_JoystickName(joysticks[index]);
}

SDL_JoystickID LibSDL::getJoystickID(size_t index)
{
    if (index >= joysticks.size()) return -1;
    return SDL_JoystickInstanceID(joysticks[index]);
}

