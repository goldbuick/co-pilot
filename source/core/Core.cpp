
#include <core/Core.h>

namespace copilot
{
    #define BUFF_SIZE 4096
    std::string format(const char* format, ... )
    {
        char text[BUFF_SIZE] = {0,};
        
        va_list ap;
        va_start(ap, format);
        vsnprintf(text, BUFF_SIZE, format, ap);
        va_end(ap);
        
        return std::string(text);
    }
}

using namespace copilot;

CoreLogic::CoreLogic(CoreLogicManager* inManager, const char* inName)
: manager(inManager), name(inName) { }
CoreLogic::~CoreLogic() { }

CoreLogicManager* CoreLogic::getManager()
{
    return manager;
}
const char* CoreLogic::getName()
{
    return name.c_str();
}

CoreLogicManager::CoreLogicManager(CoreLogicManager* inChain)
: chain(inChain) { }

CoreLogicManager::~CoreLogicManager()
{
    for (std::map<std::string, CoreLogic*>::iterator i=logistics.begin(); i!=logistics.end(); ++i)
        delete i->second;
    logistics.clear();
}

CoreLogic* CoreLogicManager::requestLogic(const char* name)
{
    std::map<std::string, CoreLogic*>::iterator entry = logistics.find(name);
    
    // found local instance
    if (entry != logistics.end()) return entry->second;
    
    // lookup core logic from parent
    if (chain) return chain->requestLogic(name);
    
    return NULL;
}
void CoreLogicManager::addLogic(CoreLogic* entry, const char* name)
{
    logistics[name] = entry;
}
