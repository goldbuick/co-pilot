
#include <core/common/Common.h>

namespace copilot
{
    std::wstring strToWide(std::string& str)
    {
        return std::wstring(str.begin(), str.end());
    }

    std::string strPrint(const char* format, ...)
    {
        const size_t MaxChars = 2048;
        static char buffer[MaxChars];
        
        va_list args;
        va_start(args, format);
        vsnprintf(buffer, MaxChars, format, args);
        va_end(args);
        
        return std::string(buffer);
    }
    
    std::string strPrint(const char* format, va_list args)
    {
        const size_t MaxChars = 2048;
        static char buffer[MaxChars];
        
        vsnprintf(buffer, MaxChars, format, args);
        va_end(args);
        
        return std::string(buffer);
    }

    std::vector<std::string> strSplit(const char* source, const char* delim)
    {
        size_t len = strlen(source)+1;
        char* str = new char[len];
        snprintf(str, len, "%s", source);
        
        // split string by space
        std::vector<std::string> parts;
        for (char *cursor=strtok(str, delim); cursor!=NULL; cursor=strtok(NULL, delim))
            parts.push_back(cursor);
        
        delete[] str;
        
        return parts;
    }
    
}
