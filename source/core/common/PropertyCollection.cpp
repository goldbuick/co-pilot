
#include <core/common/Maths.h>
#include <core/common/PropertyCollection.h>

namespace copilot
{
    
    // built-in inits
    template<> void PropertyCollectionInit(int& value) { value = 0; }
    template<> void PropertyCollectionInit(bool& value) { value = false; }
    template<> void PropertyCollectionInit(float& value) { value = 0.f; }
    template<> void PropertyCollectionInit(size_t& value) { value = 0; }
    template<> void PropertyCollectionInit(glm::rect& value) { value = glm::rect(); }
    template<> void PropertyCollectionInit(glm::vec2& value) { value = glm::vec2(0.f); }
    template<> void PropertyCollectionInit(glm::vec3& value) { value = glm::vec3(0.f); }
    template<> void PropertyCollectionInit(glm::vec4& value) { value = glm::vec4(0.f); }
    template<> void PropertyCollectionInit(glm::mat4& value) { value = glm::mat4(1.f); }
    
}

using namespace copilot;

// copy constructor
PropertyCollection::PropertyCollection(PropertyCollection& other)
{
    properties = other.properties;
}
PropertyCollection::PropertyCollection(const PropertyCollection& other)
{
    properties = other.properties;
}

std::list<std::string> PropertyCollection::keys()
{
    std::list<std::string> props;
    for (std::map<std::string, Property>::iterator i=properties.begin(); i!=properties.end(); ++i)
        props.push_back(i->first);
    return props;
}

std::list<std::string> PropertyCollection::dirty()
{
    std::list<std::string> props;
    for (std::map<std::string, Property>::iterator i=properties.begin(); i!=properties.end(); ++i)
        if (i->second.dirty)
        {
            i->second.dirty = false;
            props.push_back(i->first);
        }
    return props;
}

bool PropertyCollection::has(const char* name)
{
    Property* property = request(name);
    return (property != NULL);
}

void PropertyCollection::copyTo(const char* name, PropertyCollection* dest, bool markDirty)
{
    Property* sourceProperty = request(name);
    if (sourceProperty)
    {
        Property* destProperty = dest->request(name);
        if (!destProperty)
        {
            destProperty = dest->add(name);
            if (markDirty) destProperty->dirty = true;
            destProperty->value = sourceProperty->value;
        }
        else if (sourceProperty->value.type_info() != destProperty->value.type_info() ||
                 sourceProperty->value != destProperty->value)
        {
            if (markDirty) destProperty->dirty = true;
            destProperty->value = sourceProperty->value;
        }
    }
}

void PropertyCollection::copyAllTo(PropertyCollection* dest, bool markDirty)
{
    copyPropsTo(keys(), dest, markDirty);
}

std::list<std::string> PropertyCollection::copyDirtyTo(PropertyCollection* dest)
{
    std::list<std::string> props = dirty();
    copyPropsTo(props, dest, false);
    
    return props;
}

void PropertyCollection::copyPropsTo(std::list<std::string> props, PropertyCollection* dest, bool markDirty)
{
    for (std::list<std::string>::iterator i=props.begin(); i!=props.end(); ++i)
        copyTo(i->c_str(), dest, markDirty);
}

Property* PropertyCollection::request(const char* name)
{
    std::map<std::string, Property>::iterator i=properties.find(name);
    if (i == properties.end()) return NULL;
    return &i->second;
}

Property* PropertyCollection::add(const char* name)
{
    Property prop;
    properties[name] = prop;
    return request(name);
}

// debug
void PropertyCollection::dump()
{
    printf("Props:\n");
    std::list<std::string> props = keys();
    for (std::list<std::string>::iterator i=props.begin(); i!=props.end(); ++i)
        printf("\t%s\n", i->c_str());
    printf("------\n");
}
